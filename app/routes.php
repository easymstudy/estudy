<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/****验证码(注册)****/
Route::get('code/{rand?}', 'UserController@createcode');
/****忘记密码(找回密码)****/
Route::get('getimg/{rand?}', 'UserController@getimg');
/******发送邮件*****/
Route::post('sendmail', 'UserController@send_mail');
Route::get('sendok', 'UserController@sendok');
Route::get('modifypwd', 'UserController@modifypwd');
Route::get('checklink', 'UserController@deadline_link');
Route::get('success', 'UserController@modify_success');
/******重置密码*****/
Route::get('resetpwd', 'UserController@resetpwd');
Route::post('setpwd', 'UserController@setpwd');

//忘记密码模块
Route::get('findpsw','UserController@find_pass');
//激活帐号页面
Route::get('activation/{type?}','UserController@account_activation');
//激活发送邮件
Route::post('activationMail','UserController@activation_mail');
Route::get('mailok','UserController@activate_email_ok');

//激活链接
Route::get('activate','UserController@activate');
Route::get('activateSuccess','UserController@activateSuccess');

Route::get('sharelink','UserController@user_share');

/******* 微信  ********/
//交互
//Route::any('wx/index','WxController@wx_check');
Route::any('wx/index','WxController@responseMsg');
//自定义菜单
Route::any('wx/menu' ,'WxController@wx_init_menu');

//授权验证
Route::any('wx/authorize/{type}/{cid?}/{qid?}/{eid?}' ,'WxController@wx_authorize');

//作业吧测试地址
Route::get('wx/test' ,'WxController@test');

//获取token
Route::post('wx/token' ,'WxController@get_token');
//获取SignPackage
Route::get('wx/sign','WxController@getSignPackage');

/****用户登录模块 存在cookie直接跳转到后台首页****/
Route::group(array('before' => 'checklogin'), function(){

	//首页登录注册
	Route::get('/', function(){       
		return View::make('user.login');
	});
	Route::get('login', function(){
		return View::make('user.login');
	});
	Route::get('register', function(){
		return View::make('user.register');
	});
	
	//登录操作
	Route::post('login', 'UserController@_login');
	//注册操作
	Route::post('register', 'UserController@_register');
});


/******后台管理首页*******/
//需要验证登录的路由
Route::group(array('before' => 'isLogin'), function(){
    
	//退出
	Route::get('logout', 'AdminController@logout');
	
	//后台首页
    Route::get('index/{p?}', 'AdminController@index');
	
	/********** 班级**********/
	//班级列表
	Route::get('classList/{p?}/{wd?}', 'ClassController@index');
	//创建班级-view
	Route::get('classCreate', function(){
		return View::make('admin.class.create');
	});
	//删除班级
	Route::post('deleteclass', 'ClassController@deleteClass');
	
	//创建班级-action
	//Route::post('classCreate', 'ClassController@create');
	Route::post('classCreate', 'ClassController@create');
	//创建班级结果
	Route::get('classSuccess/{cid}', 'ClassController@success');
	//老师二维码创建
	Route::get('makecode', 'ClassController@teacherCode');
	//解除绑定
	Route::get('removebind', 'ClassController@removebind');
	//获取班级下老师二维码
	Route::get('teachercode', 'ClassController@getcode');
	//修改班级-view
	Route::get('modifyclass/{cid}', 'ClassController@modify');
	//修改班级-action
	Route::post('classModify', 'ClassController@_modify');
	//班级动态
	Route::get('classDetail/{cid}/{p?}', 'ClassController@detail');
	//班级学院详情
	Route::post('getuserinfo', 'ClassController@getUserInfo');
	//学生详情
	Route::get('studentInfo/{cid}/{uid}', 'ClassController@studentInfo');
	//学生详情-统计
	Route::post('getClassStatistics', 'ClassController@getclasstatistics');
	//班级-作业记录
	Route::get('workList/{cid}/{p?}', 'ClassController@worklist');
	//班级-作业详情
	Route::get('workstat/{cid}/{eid}', 'ClassController@workstat');
	//班级-作业记录详情统计
	Route::post('examrank', 'ClassController@stat_rank');
	//设置备注名
	Route::post('setremark', 'ClassController@setremark');
	
	
	/*************系统设置****************/    
	//系统设置，绑定微信url
	Route::get('bindWx', 'SystemController@bindWx');
 	
	//系统设置, 修改密码-view
	Route::get('changePass', function(){
		//用户帐号
		$user = Cookie::get('user');
		return View::make('admin.system.changepass')->with('res',$user);
	});
	Route::post('changePass', 'SystemController@changePass');
	
	//系统设置, 绑定微信-action 
	Route::post('bindWx', 'WxController@wx_login');
	
	//微信验证码
	Route::get('wxCode/{u}/{s}', 'WxController@wx_code');

	/*********录入试卷**********/
	Route::get('test', 'PaperController@test');
	
	Route::get('createPaper/{pid?}', function($pid = ''){
		
		$res = '';		
		if (!empty($pid)) {
			//取出题目
			$paper = new Paper();
			$res = $paper->getAllPaper($pid);
		}
				
		return View::make('admin.paper.create')->with('res',$res);				
	});
	
	//创建试卷-action
	Route::post('createPaper', 'PaperController@create');
	//更新试卷-action
	Route::post('updatePaper', 'PaperController@update_title');
	
	//录入试题-view
	Route::get('enterPaper/{pid}', 'PaperController@enter');
	
	//交换试卷题目
	Route::post('savestructs', 'PaperController@savestructs');
	
	/*
	 * 删除试卷的题目
	 */
    Route::post('deletequestion', 'PaperController@deleteQuestion'); 
	
	/*
	 * 录入试题-保存题目
	 */
    Route::post('savetitle', 'PaperController@saveQuestion'); 
	 
	/*
	 * 录入试题-保存选项
	 */
    Route::post('savechoice', 'PaperController@saveQuestion'); 
	
	/*
	 * 录入试题-保存正确答案
	 */
    Route::post('savecorrect', 'PaperController@saveQuestion');	

	/*
	 * 录入试题-保存分数
	 */
    Route::post('savescore', 'PaperController@saveQuestion'); 
	
	/*
	 * 录入试题-保存属性
	 */
    Route::post('saveattr', 'PaperController@saveQuestion'); 

	/*
	 * 录入试题-保存材料
	 */
    Route::post('savematerial', 'PaperController@saveMaterial'); 
	/*
	 * 录入试题-浮动编辑框
	 */
    Route::get('floatedit', function(){
		return View::make('admin.paper.floatedit');		
	}); 

	/*
	 * 录入试题-浮动编辑框
	 */
    Route::post('savedoc', 'PaperController@savedoc'); 
	
	/*
	 * 试卷预览
	 */
    Route::get('paperpreview/{pid}/{status?}', 'PaperController@paperPreview'); 
	/*
	 * 提交审核
	 */
    Route::get('submitcheck/{pid}/{score}/{count}', 'PaperController@submitCheck');
	
	/********试卷列表**********/
    Route::get('paperlist/{p?}/{wd?}', 'PaperController@paperList');
	
	// 试卷审核	
    Route::post('papercheck', 'PaperController@paperCheck');
	//删除
    Route::post('paperdelete', 'PaperController@paperdelete');
	
	
	/********设置作业 view********/
	Route::get('work', 'WorkController@homework');
	//提交作业
	Route::post('setwork', 'WorkController@setwork');
	//作业列表
	Route::get('worklist/{p?}','WorkController@worklist');	
	//删除作业
	Route::post('workdelete','WorkController@workChange');	
	//选择试卷名
	Route::post('selectpaper','WorkController@selectpaper');		
	//选择班级名
	Route::post('selectcname','WorkController@selectcname');
	//查看作业详情
	Route::get('workdetail/{eid}','WorkController@workdetail');	
	//测试
	Route::get('work/test','WorkController@test');	
	
	
	  
});