<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 */
class UserController extends BaseController {
    
	//验证规则
	private $rule = array(
		'username' => array('required','regex:/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i'),
		'password' => array('required','min:6'),    
	);
	
	//自定义error
	private $message = array(
		'username.required' => '帐号不能为空!',
		'username.regex' => '帐号不合法!',
		'password.required' => '密码不能为空!',
		'password.min' => '密码不合法!',	
	);
	
	//生成验证码
	public function createcode(){
		
		require Config::get('app.ThirdClass').'seccode.class.php';

		@header("Expires: -1");
		@header("Cache-Control: no-store, private, post-check=0, pre-check=0, max-age=0", FALSE);
		@header("Pragma: no-cache");
		$code = new seccode();
		$code->code = mt_rand();
		$code->type = 0;
		$code->background = 0;
		$code->adulterate = 1;
		$code->ttf = 1;
		$code->angle = 0;
		$code->color = 1;
		$code->size = 0;
		$code->shadow = 1;
		$code->animator = 0;		
		$code->datapath = $_SERVER['DOCUMENT_ROOT'].'/admin/';
		$code->fontpath = $_SERVER['DOCUMENT_ROOT'].'/admin/fonts/';
		$code->width = 125;
		$code->height = 73;			
		$code->display();
	}
	
	//(忘记密码)获取图片验证码
	public function getimg(){
		
		require Config::get('app.ThirdClass').'seccode.class.php';

		@header("Expires: -1");
		@header("Cache-Control: no-store, private, post-check=0, pre-check=0, max-age=0", FALSE);
		@header("Pragma: no-cache");

		$code = new seccode('findpsw');
		//找回密码场景
		$code->code = mt_rand();
		$code->type = 0;
		$code->background = 0;
		$code->adulterate = 1;
		$code->ttf = 1;
		$code->angle = 0;
		$code->color = 1;
		$code->size = 0;
		$code->shadow = 1;
		$code->animator = 0;		
		$code->datapath = $_SERVER['DOCUMENT_ROOT'].'/admin/';
		$code->fontpath = $_SERVER['DOCUMENT_ROOT'].'/admin/fonts/';
		$code->width = 125;
		$code->height = 73;			
		$code->display();
	
	}
	
	public function xxx(){
		$sql = 'Insert into paper(title,introduce,structs,status,is_delete,word,update_time,tid) 
				select title,introduce,structs,status,is_delete,word,'.$_SERVER['REQUEST_TIME'].',2 from paper where pid = 1';				
		echo $sql;
		exit;
		DB::insert($sql);	
	}
	
	
    //登录
    public function _login() {
        
        if (Request::ajax()) {
			//接收表单
            $data = Input::all();
			
			//验证码比对
			if ( strtolower($data['code']) != Session::get('code') ) {
				return Response::json(array('status'=>0,'info'=>'验证码错误'));
			}
			
			//验证数据合法
            $input = array(
                'username' => $data['user'],
                'password' => $data['pass'],
            );			
            $validator = Validator::make($input, $this->rule, $this->message);
			//验证失败信息
            if ( $validator->fails() ) {
            	$message = current($validator->messages()->toArray());
				return Response::json(array('status'=>0,'info'=>$message[0]));
            }
		
			//登录
			$user = new User();

			if ( $res = $user->login($data) ) {

				$uid = $res['tid'];
				//tid存入session
				Session::put('tid', $uid );
				
				if ($res['status'] == 0) {
					//没有激活
					return Response::json(array('status'=>2,'info'=>'没有激活'));					
				}
				
				$uid = Helpers\Helper::authcode($uid,'ENCODE');
				//cookie存储用户信息
				$useInfo = array(
					'user' => $data['user'],
					'uid' => $uid,
				);
				
				if ($data['remember']) {
					//永久有效
					$cookies = Cookie::forever('user', $useInfo);									
				} else {
					$cookies = Cookie::make('user', $useInfo);
				}
				return Response::json(array('status'=>1,'info'=>'登陆成功'))->withCookie($cookies);					
			} else {
				return Response::json(array('status'=>0,'info'=>'帐号密码错误!'));	
			}
        } else {
			return Response::json(array('status'=>0,'info'=>'非法操作'));
		}
    }
	
    //注册
    public function _register() {

        if (Request::ajax()) {
			
            $data = Input::all();
            //验证表单数据合法
            $input = array(
                'username' => $data['user'],
                'password' => $data['pass'],
				'password_confirmation' => $data['repass'],
            );
			
			//从基础规则上增加自由规则
			$this->rule['username'][] = 'unique:teacher';
			$this->rule['password'][] = 'confirmed';
			
			//同上
			$this->message['username.unique'] = '此帐号已被注册过!';
			$this->message['confirmed'] = '两次输入的密码不一致!';
			
            $validator = Validator::make($input, $this->rule, $this->message);
			//验证失败信息
            if ( $validator->fails() ) {
            	$message = current($validator->messages()->toArray());
				return Response::json(array('status'=>0,'info'=>$message[0]));
            } 
			
			//注册
			$user = new User();
			if ( $uid = $user->register($data) ) {

					//tid存入session
					Session::put('tid', $uid );
					/*
					$uid = Helpers\Helper::authcode($uid,'ENCODE');
					//cookie存储用户信息
					$useInfo = array(
						'user' => $data['user'],
						'uid' => $uid,
					);
					*/
					
					//跳转到激活页面
					return Response::json(array('status'=>1,'info'=>'注册成功'));	
					
					//$cookies = Cookie::make('user', $useInfo);
					//return Response::json(array('status'=>1,'info'=>'注册成功'))->withCookie($cookies);	
    				
			} else {
				return Response::json(array('status'=>0,'info'=>'注册失败'));	
			}
            
        } else {
        	return Response::json(array('status'=>0,'info'=>'非法操作'));
        }
    }
	
	//注册激活
	public function account_activation($type = ''){
		
		//如果不存在session ,跳转到404页面
		//if ( $tid =  Session::get('tid') ) {
			$tid =  Session::get('tid');
			//查询此人的邮箱
			$User = new User();
			if ( $res = $User->get_admin_info($tid) ) {
				return View::make('user.activation')->with('res', $res)->with('type',$type);
			}
						
		//}
				
	}
	
	//激活发送邮件
	public function activation_mail(){
		
		$tid = Session::get('tid');
		//通过tid查询username
		$res = array();
		
		if ($tid = Session::get('tid')) {
			$User = new User();
			$res = $User->get_admin_info($tid);
		} else {
			
			$data = Input::all();			
			if (!empty($data['sid'])){
				$sid = Helpers\Helper::authcode(base64_decode($data['sid']));
				$email = new Email();
				if ($res = $email->get_account($sid)) {
					$res['username'] = Crypt::decrypt($res['account_code']);	
				}
			}
		}
		
		if ($res) {

			$_data = array(	
				'account_code'=>Crypt::encrypt($res['username']),	
				'status'=>0,
				'deadline'=>$_SERVER["REQUEST_TIME"] + 1800 //有效期30分钟内
			);
			
			$email = new Email();
			//往邮件表添加一条记录
			if ( $id = $email->user_send_mail($_data) ) {
	
				$sid = base64_encode(Helpers\Helper::authcode($id, 'ENCODE'));
				
				//成功则发出邮件
				require Config::get('app.ThirdClass').'phpmail/class.phpmailer.php';
				require Config::get('app.ThirdClass').'phpmail/class.smtp.php';
							
				//邮件配置
				$mail = new PHPMailer(); //实例化 
				$mail->IsSMTP(); // 启用SMTP 
				$mail->Host = "smtp.163.com"; //SMTP服务器 以163邮箱为例子 
				$mail->Port = 25;  //邮件发送端口 
				$mail->SMTPAuth   = true;  //启用SMTP认证 
				 
				$mail->CharSet  = "UTF-8"; //字符集 
				$mail->Encoding = "base64"; //编码方式 
				 
				$mail->Username = "13554390742@163.com";  //你的邮箱 
				$mail->Password = "loveyouyou616";  //你的密码 
				$mail->Subject = "麦学习帐号激活"; //邮件标题 
				 
				$mail->From = "13554390742@163.com";  //发件人地址（也就是你的邮箱） 
				$mail->FromName = "麦学习团队";  //发件人姓名 
				 
				$address = $res['username'];//收件人email 
				$mail->AddAddress($address, "您好");//添加收件人（地址，昵称） 
				 
				//$mail->AddAttachment('xx.xls','我的附件.xls'); // 添加附件,并指定名称 
				$mail->IsHTML(true); //支持html格式内容 
				//$mail->AddEmbeddedImage("logo.jpg", "my-attach", "logo.jpg"); //设置邮件中的图片 
				$mail->Body = '亲爱的麦学习用户，您好：'; //邮件主体内容 
				$mail->Body .= '<br/><br/><br/><br/>请您点击下面链接来激活麦学习后台登录帐号:<br > <a href="http://admin.mstudy.me/activate?mailType=activation&sid='.$sid.'">http://admin.mstudy.me/resetpwd?mailType==activation&sid='.$sid.'</a>'; //邮件主体内容 
				$mail->Body .= '<br/><br/>为了确保您的帐号安全，该链接仅<font style="color:red;">30分钟</font>内访问有效。';
				$mail->Body .= '<br/><br/>如果点击链接不工作...';
				$mail->Body .= '<br/><br/>请您选择并复制整个链接，打开浏览器窗口并将其粘贴到地址栏中。然后单击"转到"按钮或按键盘上的 Enter 键。';
				$mail->Body .= '<br/><br/>链接仅<font style="color:red;">请勿直接回复该邮件</font>，有关 麦学习 的更多帮助信息，请访问：<a href="http://help.mstudy.me">http://help.mstudy.me</a>';
				
				//发送 
				if(!$mail->Send()) { 
					//echo "Mailer Error: ".$mail->ErrorInfo; 
					return Response::json(array('status'=>0,'info'=>'发送失败'));				
				} else { 
					$cookies = Cookie::make('username',$_data['account_code']);
					return Response::json(array('status'=>1,'info'=>'发送成功'))->withCookie($cookies);
				} 		
			} else {
				//echo "Mailer fail"; 
				return Response::json(array('status'=>0,'info'=>'发送失败'));
			}
		} else {
			//操作失败
			return Response::json(array('status'=>0,'info'=>'发送失败'));
		}
	}
	
	//发送邮件成功页面
	public function activate_email_ok(){
		
		if ($username = Cookie::get('username') ) {
			$username =  Crypt::decrypt($username);
			$address = strrchr($username, '@'); 

			$name = strstr($username,'@',true);
			if (strlen($name) <3 ) {
				$username = '*'.$address;
			} else {
				//保留最后一位
				$username = $name{0}.str_repeat('*',strlen($name)-2).$name{strlen($name)-1}.$address;	
			}
			return View::make('user.activate_email_ok')->with('username', $username);
		}
		return Redirect::to('login');
		
	}
	
	//激活帐号
	public function activate(){

		$data = Input::all();
		if ($data['mailType'] == 'activation') {
			
			$id = Helpers\Helper::authcode(base64_decode($data['sid']));
			$email = new Email();
			if ($res = $email->get_user_mail($id)) {
				
				$username = Crypt::decrypt($res['account_code']);
				
				//激活
				$User = new User();
				
				//查询状态
				$res = $User->get_admin($username);
				if ($res['status'] == 1) {
					//已经激活过
					return Redirect::to('activateSuccess');
				}
				
				if ( $User->update_status($username) ) {
					//激活成功, 修改连接
					$email->update_status($id);
					
					//复制一套试卷
					/*
					$sql = 'Insert into paper(title,introduce,structs,status,is_delete,word,update_time,tid) 
							select title,introduce,structs,status,is_delete,word,'.$_SERVER['REQUEST_TIME'].','.$res['tid'].' from paper where pid = 1';				
					*/
					
					$Paper = new Paper();
					if ( $_data = $Paper->get_paper_detail(1,array('title','introduce','structs','status','is_delete','word')) ) {
						$_data['tid'] = $res['tid'];
						$Paper->insert_paper($_data);
					}
					
					//跳转到状态页面, 成功
					return Redirect::to('activateSuccess');
				} else {
					//激活失败
					exit('激活失败,请联系管理员');
				}
			} 
		}
		
		//链接无效
		return View::make('user.activation_failure')->with('sid',$data['sid']);
	}
	
	//激活成功
	public function activateSuccess(){
		return View::make('user.activate_success');
	}
	
	
	//找回密码
	public function find_pass(){
		return View::make('user.findpsw');
	}
	
	//发送邮件找回密码
	public function send_mail(){
		
		$data = Input::all();

		//验证码错误
		if ($data['code'] != Session::get('findpsw')) {
			return Response::json(array('status'=>-1,'info'=>'验证码错误'));
		}
		
		//账号不存在
		$User = new User();
		$res = $User->get_admin($data['username']);
		if (empty($res)) {
			return Response::json(array('status'=>-2,'info'=>'邮箱错误'));
		}
		
		$_data = array(
			'account_code'=>Crypt::encrypt($data['username']),		
			'status'=>0,
			'deadline'=>$_SERVER["REQUEST_TIME"] + 259200 //有效期3天
		);
		
		$email = new Email();
		//往邮件表添加一条记录
		if ( $id = $email->send_mail($_data) ) {

			$sid = base64_encode(Helpers\Helper::authcode($id, 'ENCODE'));
			
			//成功则发出邮件
			require Config::get('app.ThirdClass').'phpmail/class.phpmailer.php';
			require Config::get('app.ThirdClass').'phpmail/class.smtp.php';
						
			//邮件配置
			$mail = new PHPMailer(); //实例化 
			$mail->IsSMTP(); // 启用SMTP 
			$mail->Host = "smtp.163.com"; //SMTP服务器 以163邮箱为例子 
			$mail->Port = 25;  //邮件发送端口 
			$mail->SMTPAuth   = true;  //启用SMTP认证 
			 
			$mail->CharSet  = "UTF-8"; //字符集 
			$mail->Encoding = "base64"; //编码方式 
			 
			$mail->Username = "13554390742@163.com";  //你的邮箱 
			$mail->Password = "loveyouyou616";  //你的密码 
			$mail->Subject = "麦学习后台密码修复"; //邮件标题 
			 
			$mail->From = "13554390742@163.com";  //发件人地址（也就是你的邮箱） 
			$mail->FromName = "麦学习团队";  //发件人姓名 
			 
			$address = $data['username'];//收件人email 
			$mail->AddAddress($address, "您好");//添加收件人（地址，昵称） 
			 
			//$mail->AddAttachment('xx.xls','我的附件.xls'); // 添加附件,并指定名称 
			$mail->IsHTML(true); //支持html格式内容 
			//$mail->AddEmbeddedImage("logo.jpg", "my-attach", "logo.jpg"); //设置邮件中的图片 
			$mail->Body = '亲爱的麦学习用户，您好：'; //邮件主体内容 
			$mail->Body .= '<br/><br/><br/><br/>请您点击下面链接来修复麦学习后台登录密码:<br > <a href="http://admin.mstudy.me/resetpwd?mailType=regemail&sid='.$sid.'">http://admin.mstudy.me/resetpwd?mailType=regemail&sid='.$sid.'</a>'; //邮件主体内容 
			$mail->Body .= '<br/><br/>为了确保您的帐号安全，该链接仅<font style="color:red;">3天</font>内访问有效。';
			$mail->Body .= '<br/><br/>如果点击链接不工作...';
			$mail->Body .= '<br/><br/>请您选择并复制整个链接，打开浏览器窗口并将其粘贴到地址栏中。然后单击"转到"按钮或按键盘上的 Enter 键。';
			$mail->Body .= '<br/><br/>链接仅<font style="color:red;">请勿直接回复该邮件</font>，有关 麦学习 的更多帮助信息，请访问：<a href="http://help.mstudy.me">http://help.mstudy.me</a>';
			
			//发送 
			if(!$mail->Send()) { 
				//echo "Mailer Error: ".$mail->ErrorInfo; 
				return Response::json(array('status'=>0,'info'=>'发送失败'));				
			} else { 
				//跳转到第2个页面
				$cookies = Cookie::make('username', $_data['account_code']);
				return Response::json(array('status'=>1,'info'=>'发送成功'))->withCookie($cookies);
			} 		
		} else {
			//echo "Mailer fail"; 
			return Response::json(array('status'=>0,'info'=>'发送失败'));
		}
	}
	
	//邮箱链接检测
	public function resetpwd(){

		$data = Input::all();
		
		//验证连接来源
		$sid = Helpers\Helper::authcode(base64_decode($data['sid']));
		
		//通过sid查询是否合法有效
		$email = new Email();
		if ( $res = $email->get_account($sid) ) {
			return View::make('user.modifypwd')->with('data', $data);
		} else {
			//跳转到第二个页面(失效)
			return View::make('user.findpsw_ok')->with('status',1);				
		}
	}
	
	//发送邮件成功
	public function sendok(){

		if ($username = Cookie::get('username') ) {
			$username =  Crypt::decrypt($username);
			$address = strrchr($username, '@'); 

			$name = strstr($username,'@',true);
			if (strlen($name) <3 ) {
				$username = '*'.$address;
			} else {
				//保留最后一位
				$username = $name{0}.str_repeat('*',strlen($name)-2).$name{strlen($name)-1}.$address;	
			}
			return View::make('user.findpsw_ok')->with('username', $username);
		}		
		return View::make('user.findpsw_ok');
	}
	
	//修改密码
	public function modifypwd(){
		return View::make('user.modifypwd');
	}
	
	//链接失效
	public function deadline_link(){		
		return View::make('user.findpsw_ok');
	}
	
	//修改密码成功
	public function modify_success(){
		return View::make('user.success');
	}
	
	//设置新密码
	public function setpwd(){
		
		$data = Input::all();
		
		//1.检测type
		if ($data['mailType'] == 'regemail') {

			$sid = Helpers\Helper::authcode(base64_decode($data['sid']));
			//2.通过sid 获取 用户信息
			$email = new Email();
	
			if ($res = $email->get_account($sid)) {
				$username = Crypt::decrypt($res['account_code']);	
				
				//新旧密码不能一致
				$User = new User();

				if ($res = $User->get_admin($username)) {

					if ($res['password'] == md5($data['newpass'].Config::get('app.passkey'))) {
						//跳转到第2个页面, 提示新密码和旧密码不能一样
						return View::make('user.findpsw_ok')->with('status',2);
					}

					//更新
					$data = array(
						'tid'=>$res['tid'],
						'password'=>md5($data['newpass'].Config::get('app.passkey'))
					);

					if ($User->update_admin($data)) {
						//跳转到成功页面,提示用新密码重新登录
						$email->update_email($sid);
						return Redirect::to('success');
					} else {
						//操作失败，重新操作
						return View::make('user.findpsw_ok')->with('status',3);
					}
				}
			}
		}		

		//跳转到链接失效页面
		return View::make('user.findpsw_ok')->with('status',1);
	}
	
	/**********用户分享，客户端************/
	public function user_share(){
		//跳转到链接失效页面
		return View::make('share.index');
	}
	
	
}
