<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 */

class AdminController extends BaseController {
	
	//后台首页
	public function index($p = 1) {
		
		$tid = Session::get('tid');

		//基础数据统计
		$req_url = Config::get('app.api_url')."admin/stat/base/{$tid}";
		$stat_base = json_decode(Helpers\Helper::curl($req_url), true);
		
		//最近考试情况列表
		$time = $_SERVER["REQUEST_TIME"];
		
		$Stat = new Stat();
		$count = $Stat->get_examstat_count($time);

		//初始化
		$res = array();
		$show = '';
		$pagesize = Config::get('app.pagesize');
		
		if ($count) {
			
			require Config::get('app.ThirdClass').'Page.class.php';
			
			$start = ($p-1) * $pagesize;
			$res = $Stat->get_exam_detail($time, $start, $pagesize);

			//分页
			$Pape = new Page($count, $pagesize, $p);
			$show = $Pape->show();
		}
		
		//检测此老师是否布置了作业
		$Exam = new Exam();
		$exam_total = $Exam->is_make_exam($tid);
				
		return View::make('admin.index')->with('statbase', $stat_base['statbase'])
										->with('res', $res)
										->with('show', $show)
										->with('count', $count)
										->with('pagesize', $pagesize)
										->with('exam_total', $exam_total);
	}   
	
	//后台登出
	public function logout(){
		
		//销毁cookie，跳转到登录页面。
		$cookie = Cookie::forget('user');
		
		//销毁session
		Session::forget('tid');
		
		return  Redirect::to('login')->withCookie($cookie);
			
	} 
    
}
