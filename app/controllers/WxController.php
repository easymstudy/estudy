<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WxController
 *
 * @author zhaochong
 * 模拟登录以及模拟绑定，后台相关wx的操作
 */
class WxController extends BaseController {
	
	const appid = 'wxff3590451925b1f1';
	const secret = '0393758d45a6bd1df24a64abc99c1310';

    //绑定的url
    private $url = 'http://www.easymstudy.com/wx/index';
    //绑定的token
    private $token = 'weixin';
    
    //需要请求的urls
    private $r_urls = array(
        'login'=>'https://mp.weixin.qq.com/cgi-bin/login',
        'bind'=>'https://mp.weixin.qq.com/advanced/callbackprofile?t=ajax-response&lang=zh_CN&token=',
        'getInfo'=>'https://mp.weixin.qq.com/advanced/advanced?action=dev&t=advanced/dev&lang=zh_CN&f=json&token=',
    	'getCode'=>'https://mp.weixin.qq.com/cgi-bin/verifycode?username=',
		'menu'=>'https://api.weixin.qq.com/cgi-bin/menu/create?access_token=',
		'get_access_token'=>'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=#APPID#&secret=#APPSECRET#',
	);
    
    //请求header
    private $headers = array(
        'User-Agent:Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36',
		'Referer:https://mp.weixin.qq.com/',
    );
    
    //获取数据正则
    private $pattern = array(
        'cookie'=>'/set-cookie:([^\r\n]*)/i',
        'res'=>'/\{.*\}/i',
    );
	
	public function test(){
		
		$weixin = array(
			'uid'=>0,
			'cid'=>0,
			'tid'=>3,
			'openid'=>'oYItKs9eh57E-1qY_-T3NVCqebSY',
			'type'=>7,
		);
		
		return View::make('admin.weixin.index')->with('_weixin', json_encode($weixin));
	}
	
	
	/*********后台操作**********/	
	//微信登录验证码
	public function wx_code($u){

		if ( !empty($u) ) {
			
			$url = $this->r_urls['getCode'].$u.'&r='.$_SERVER["REQUEST_TIME"];
			//验证码cookie
			$file = Config::get('app.myPublic').'data/cookie/cookie_'.$u.'.txt';
			
			$curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0 );
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2 );
            curl_setopt($curl, CURLOPT_HEADER, 0 );            
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);	
			curl_setopt($curl, CURLOPT_COOKIEJAR, $file);	
            $result = curl_exec ($curl);
            curl_close ( $curl );
			header('Content-Type:image/jpeg');
 			return $result;
		}
	}

    //微信模拟登录
    public function wx_login() {

        if (Request::ajax()) {
            
            $_data = Input::all();			

            //绑定的公众帐号
            $data['post'] = array(
                'username' => $_data['wx_account'],
                'pwd' => md5($_data['wx_pass']),
            );
			
			//cookie文件
			$file = '';
			
			//验证码的cookie
			if (isset($_data['wx_code'])){
				$data['post']['imgcode'] = $_data['wx_code'];
				$file = Config::get('app.myPublic').'data/cookie/cookie_'.$_data['wx_account'].'.txt';
			}
			
			//模拟登录
            $res = Helpers\Helper::curl($this->r_urls['login'], $data, '', $this->headers, 1, $file);
			if ($res){
            	
				list($header, $body) = explode("\r\n\r\n",$res);
				//获取返回信息
				preg_match_all( $this->pattern['res'], $body, $matches );
				$body = json_decode( $matches[0][0] );
				//file_put_contents('1.php',var_export($body,true));
				if ($body->base_resp->ret == 0) {
	
					$url_info = parse_url( $body->redirect_url );
					parse_str( $url_info['query'], $getParams );
		
					//获取cookie
					preg_match_all( $this->pattern['cookie'], $header, $matches);
					$cookie = implode( ';', $matches[1] );
									
					//模拟绑定
					$res = $this->wx_bind($cookie, $getParams['token']);				
	
					if ( $res->base_resp->ret == 0 ){
						
						//获取微信帐号信息
						$data = $this->wx_getInfo($cookie, $getParams['token']);
						$data['account'] =  $_data['wx_account'];
						$data['password'] = $_data['wx_pass'];
						$data['app_key'] = $_data['wx_secret'];
						//file_put_contents('1.php',var_export($data, true));
						//把填充微信公众帐号信息
						$Wx = new Wx();
						$Wx->updateWxInfo($data);
											
						return Response::json(array('status'=>1,'info'=>'绑定成功','data'=>$data['user_info'])); 
					} else {
						return Response::json(array('status'=>0,'info'=>'绑定失败'));
					}
					
				} elseif( $body->base_resp->ret == '-8' ) {
					//验证码
					return Response::json(array('status'=>'-8','info'=>'需要验证码'));
				} else {
					return Response::json(array('status'=>0,'info'=>'绑定失败'));
				}			
			}

			return Response::json(array('status'=>0,'info'=>'绑定失败'));
        } else {
        		return Response::json(array('status'=>0,'info'=>'非法操作'));
        }
    }
    
    //微信模拟绑定
    private function wx_bind($cookie, $token) {	
		
        $data['post'] = array(
            'url'=>$this->url,
            'callback_token'=> $this->token,
			'encoding_aeskey'=>Helpers\Helper::randomkeys(),
			'callback_encrypt_mode'=>0,
        );
		
		$headers = array(
			'User-Agent:Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36',
			'Referer:https://mp.weixin.qq.com/advanced/advanced?action=interface&t=advanced/interface&token='.$token.'&lang=zh_CN'
		);
		
		//$cookie = 'ptui_loginuin=422896102; qqB_short=1; pgv_si=s5644323840; pgv_info=ssid=s0; pgv_pvid=1880956730; o_cookie=422896102; qm_username=422896102; qm_sid=5862206ca6e57e2e6417e27adadb44c8,qMWo1ekdXNHJIRGtBM1BCbVA4TEtZLUlXWUZxWDJHdVkxQjJic2lYZ3dFSV8.; pt2gguin=o0422896102; uin=o0422896102; skey=@lYv9OabXM; ptisp=ctc; RK=XFkK2SRQOa; qzone_check=422896102_1415065661; ptcz=1af860da1a43342620977576b15766307d124bd50c4517e51e3f1a1bd64e5dd1; data_bizuin=2392756394; data_ticket=AgUYDPYRve/aXYDqHLQoYKYQAwEPKlDyEv5MSUU+LqU=; slave_user=gh_fb97a07dec15; slave_sid=NDJhV1pjMFBNQzdKbUVNTlFTUFR5ak8xUldRM0VvU1FicGdXcnNCOTFYb3dQaUo5SE1UaGxVQU5xbnllZ051V0U1SHhQaGVYME1mODBuOVJ3YU5YQjh0TTNMXzVWYUxUd3BVZmxCeWVUMzZoelNpbkxha3FGOFBJeUE1N25Cank=; bizuin=2390838154';
		
        $res = Helpers\Helper::curl($this->r_urls['bind'].$token, $data, $cookie, $headers);

		return json_decode($res);
        
    }
    
    //获取已绑定的微信帐号信息
    private function wx_getInfo($cookie, $token) {
        
        $res = Helpers\Helper::curl($this->r_urls['getInfo'].$token, '', $cookie);
        // ?暂时不判断获取失败
        $res = json_decode($res,true);
        if ( isset($res['user_info']) && isset($res['advanced_info']) ){
            return array('user_info'=>$res['user_info'],'advanced_info'=>$res['advanced_info']['dev_info']);
        }
    }
	
	/************* 微信操作等自定义菜单 **************/
	//验证
	public function wx_check(){

		$echoStr = Input::get('echostr');
        //valid signature , option
        if($this->checkSignature()){
        	echo $echoStr;
        	exit;
        }	
	}
	
	//验证
	private function checkSignature()
	{
        // you must define TOKEN by yourself
        if (!$this->token) {
            throw new Exception('TOKEN is not defined!');
        }

        $signature = Input::get('signature');
        $timestamp = Input::get('timestamp');
        $nonce = Input::get('nonce');
        		
		$token = $this->token;
		$tmpArr = array($token, $timestamp, $nonce);
        // use SORT_STRING rule
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		
		if( $tmpStr == $signature ){
			return true;
		}else{
			return false;
		}
	}

	//授权验证
	public function wx_authorize($type ,$cid = '', $qid = '', $eid = ''){

		if ( Input::has('code') ){
			
			//接口url
			$apiurl = Config::get('app.api_url');
			
			$_code = Input::get('code');
			
			//初始化模版渲染值
			$weixin = array(
				'cid'=>'',
				'type'=>'',
				'openid'=>'',
				'is_subscribe'=>0,				
			);
						
			$Wx = new Wx();
			if ( $res = $Wx->get_app_info() ){
				$url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=wxf305ea0876e9955e&secret=a39cb813682c13d004633ac46c473bd7&code='.$_code.'&grant_type=authorization_code';
				$appid = 'wxf305ea0876e9955e';
				$res = json_decode(Helpers\Helper::curl($url),true);				
				
				//错误
				if (isset($res['errcode'])) {
					
					switch ($res['errcode']) {
						case 40029 :
							$url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$appid."&redirect_uri=".urlencode("http://www.easymstudy.com/wx/authorize/{$type}")."&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
							return Redirect::to($url);
							break;
					}
				}
								
				if (!empty($res['access_token'])) {

					//$url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$res['access_token'].'&openid='.$res['openid'].'&lang=zh_CN';		
					//$result = json_decode(Helpers\Helper::curl($url),true);										
					$result = $res;
										
					if (!empty($result['openid'])) {

						//检测是否关注
						$uid = $User->isSubscribe($result['openid']);
						if (!$uid) {
							$r_url = 'http://mp.weixin.qq.com/s?__biz=MzAwODAwNzAwOA==&mid=205224348&idx=1&sn=075cff42168d0a9fe62c2562bde3ff37#rd';
							return Redirect::to($r_url);
						}	
						$weixin['uid'] = $uid['uid'];																						
						$weixin['is_subscribe'] = 1;
						$weixin['openid'] = $result['openid'];

						if ($type == 4) {
							
							//1.后台扫老师二维码 是否已经被绑定过
							$Code = new ClassCode();
							$status = $Code->check_bind($qid);
							if ($status['status'] == 1) {
								//已经绑定过 前台通过判断type值 跳转到 绑定失败页面
								$weixin['type'] = 5; 
								return View::make('admin.weixin.index')->with('_weixin',json_encode($weixin));
							} else {
								
								$weixin['type'] = 6;
								$weixin['qid'] = $qid;
								$weixin['cid'] = $cid;
								
								return View::make('admin.weixin.index')->with('_weixin', json_encode($weixin));	

							} //if ($status == 1) {
							
						} else {
							
							//判断是不是老师
							$url = $apiurl.'/teacher/study/role';
							$data['post'] = array(
									'oid'=>$res['openid'],
							);
							$res = json_decode(Helpers\Helper::curl($url, $data), true);
							
							if ($res['status'] == 1) {

								if ($res['data']['role'] == 2) {
									//是老师
									switch ($type) {
										case 1:
											//不能加
											$weixin['type'] = 9;
											return View::make('admin.weixin.index')->with('_weixin', json_encode($weixin));
											break;
										case 2:
											//已经是老师， 直接跳转班级列表
											$weixin['type'] = 8;
											//$weixin['tid'] = $res['data']['tid'];
											return View::make('admin.weixin.index')->with('_weixin', json_encode($weixin));
											break;
										case 3:
											//已经是老师，扫班级二维码 错误
											$weixin['type'] = 9;
											return View::make('admin.weixin.index')->with('_weixin', json_encode($weixin));
											break;
										case 7:
											//可能出现此情况
											$weixin['type'] = 11;
											return View::make('admin.weixin.index')->with('_weixin', json_encode($weixin));
											break;
									}
									
								} else {
									
									//学生获取新的作业
									if ($type == 7) {
										$weixin['type'] = 7;
										$weixin['cid'] = $cid;
										$weixin['eid'] = $eid;
										return View::make('admin.weixin.index')->with('_weixin', json_encode($weixin));							
									}
									
									switch ((int)$type) {
										case 1:
										case 2:
											$weixin['type'] = $type;
											$weixin['cid'] = $cid;
											return View::make('admin.weixin.index')->with('_weixin', json_encode($weixin));			
											break;
										case 3:
											$url = $apiurl."/classes/is_exists_class/{$weixin['uid']}/{$cid}";
											$_res = json_decode(Helpers\Helper::curl($url), true);
											
											if ($_res['status'] == 1) {
												//已经在此班级
												$type = 10;
											}										
											$weixin['type'] = $type;
											$weixin['cid'] = $cid;			
											return View::make('admin.weixin.index')->with('_weixin', json_encode($weixin));
											break;
									}
										
								}//$res['data']['role'] == 2
								
							}//$res['status'] == 1
								
						}//$type == 4
							
					}//!empty($result['openid']
												
				}//!empty($res['access_token'])
					
			} //$res = $Wx->get_app_info() 
			
		} // Input::has('code')
	}
	
	//自定义菜单
	public function wx_init_menu(){

		$res = $this->get_token(true);
		//从缓存中获取access_token
		$token = $res['token'];		
		$url = $this->r_urls['menu'].$token;

		$arr = array( 
            'button' =>array(
                array(
					'type'=>'view',
                    'name'=>urlencode("加入班级"),										
					"url"=>"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf305ea0876e9955e&redirect_uri=".urlencode('http://admin.mstudy.me/wx/authorize/1/2')."&response_type=code&scope=snsapi_base&state=1#wechat_redirect"
                ),
                array(
					'type'=>'view',
                    'name'=>urlencode("我的班级"),
					"url"=>"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf305ea0876e9955e&redirect_uri=".urlencode('http://admin.mstudy.me/wx/authorize/2/2')."&response_type=code&scope=snsapi_base&state=1#wechat_redirect"
                ),				
                array(
					'name'=>urlencode('更多'),
					'sub_button'=>array(
						array(
							'type'=>'view',
							'name'=>urlencode('麦学习'),
							'url'=>'http://www.easymstudy.com/sharelink'
						),
						array(
							'type'=>'view',
							'name'=>urlencode('帮助'),
							'url'=>'http://help.mstudy.me/m_index.html'
						),
					),
                ),				
            )
        );
        
		$jsondata = urldecode(json_encode($arr));
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2 );
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$jsondata);
        $res = curl_exec($ch);
        curl_close($ch);
		var_dump($res);
	}		
	
	//交互
    public function responseMsg()
    {
		//get post data, May be due to the different environments
		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
      	//extract post data
		if (!empty($postStr)){
                /* libxml_disable_entity_loader is to prevent XML eXternal Entity Injection,
                   the best way is to check the validity of xml by yourself */
                libxml_disable_entity_loader(true);
              	$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
				$fromUsername = $postObj->FromUserName;
                $toUsername = $postObj->ToUserName;
                $keyword = trim($postObj->Content);
                $time = time();	
				
				//暂时不封装, 写入用户名信息
				$User = new User();
				$data = array(
					'openid'=>$fromUsername,
				);
				switch (trim($postObj->Event)) {

					case 'subscribe':
						//关注
						//获取token
						$token = $this->get_token(true);
						
						if (!empty($token['token'])) {
							
							//获取用户信息
							$url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={$token['token']}&openid={$fromUsername}&lang=zh_CN";
							$res = json_decode(Helpers\Helper::curl($url), true);					
						
							if (!empty($res['subscribe'])) {
								//file_put_contents('1.php',1);
								//如果已经存在更新即可
								if ($User->findUser($fromUsername)) {
									$User->update_subscribe($fromUsername,1);
								} else {
									$data['nickname'] = $res['nickname'];
									$data['headimgurl'] = $res['headimgurl'];
									$data['status'] = 1;
									
									$User->subscribe($data); 
								}
							}
						}
						break;
					case 'unsubscribe':
						//取消关注
						$data['status'] = 0;
						$User->unsubscribe($data);
						break;
				}
																	
				$textTpl = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
							</xml>";             
				
				if(!empty( $keyword ))
                {
              		$msgType = "text";
                	$contentStr = "Welcome to wechat world!";
                	$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
                	echo $resultStr;
                }else{
                	echo "Input something...";
                }

        }else {
        	echo "";
        	exit;
        }
    }
	
	/*******获取access_token统一接口(后台客户端)********/
	public function get_token($flag=false) {

		$redis = Illuminate\Support\Facades\Redis::connection();

		$wx_token = $redis->get('wx_token_'.self::appid);

		if (!$wx_token){
			//访问接口获取token
			$init = array("#APPID#", '#APPSECRET#');
			$replace = array(self::appid, self::secret);
			$url= str_replace($init, $replace, $this->r_urls['get_access_token']);
			$res = json_decode(Helpers\Helper::curl($url),true);
			
			$wx_token = '';
			if (isset($res['access_token'])) {
				$wx_token = $res['access_token'];
				$redis->setex('wx_token_'.self::appid, 7000, $res['access_token']);
			}						
		}
		
		$data = array(
			'token'=>$wx_token
		);
		
		if ($flag) {
			return $data;
		} else {
			return Response::json(array('status'=>1, 'token'=>$data['token']));
		}

	}	
	
	//前台获取SignPackage
	public function getSignPackage(){
		
		//这里不检测是否为 ajax请求，Request::ajax()可能会报跨域问题
		//if (Request::ajax()) {
			
			require Config::get('app.ThirdClass').'/weixin/jssdk.php';
			$jssdk = new JSSDK(self::appid, self::secret);
			$res = json_encode($jssdk->GetSignPackage());

			return Response::json(array('status'=>1, 'res'=>json_decode($res,true)))
							->header('Access-Control-Allow-Origin','*');

		//}

	}
	
	
}
