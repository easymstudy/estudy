<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaocong
 */

class WorkController extends BaseController {
	
	public function test(){
	}
	
	
	//查看作业详情
	public function workdetail($eid){
		
		$res = $students = array();
		
		$work = new Work();
		if ($res = $work->get_exam_info($eid)) {
			
			//格式化用时
			$res['duration'] = Helpers\Helper::formatduration($res['duration']);
			
			$cids = array();
			if (!empty($res['uids'])) {
				$res['uids'] = json_decode(gzuncompress(base64_decode($res['uids'])), true);
				foreach ($res['uids'] as $key=>$value) {
					$cids[] = $key;
				}
				
				$classes = new Classes();
				if (!empty($cids)) {
					$students = $classes->get_class_detail($cids);			
				}
			}
		}
		
		//print_r($res);
		//print_r($students);
		
		return View::make('admin.work.detail')
					->with('res', $res)
					->with('students', $students);
	}
	
	//改变作业状态
	public function workChange(){
		
		if (Request::ajax()) {

			$work = new Work();
			$data = Input::all();
			$eid = (int)$data['eid'];
						
			//删除考试
			if ( $res = $work->changeStatus($data)){
				
				//先查询 这次作业布置给了那些班级
				if ( $res = $work->get_exam_uids($eid) ){
					$uids = json_decode(gzuncompress(base64_decode($res['uids'])), true);
					if (!empty($uids)) {
						
						$cids = array();
						
						foreach ($uids as $k=>$v) {
							$cids[] = $k;
						}
						
						if (!empty($cids)) {
							//查询符合条件班级的作业
							$Classes = new Classes();
							
							if ($res = $Classes->get_examids($cids) ) {
								
								foreach ($res as $key=>&$val) {
									$val['examids'] =  json_decode(gzuncompress(base64_decode($val['examids'])), true);
									if (isset($val['examids'][$eid])) {
										unset($val['examids'][$eid]);	
									}
									
									$data = array();
									if (empty($val['examids'])){
										$data['examids'] = '';
									} else {
										$data['examids'] = base64_encode(gzcompress(json_encode($val['examids']),9));
									}
									$Classes->update_class($val['cid'], $data);
								}
							}
						}
					}				
				}
												
				return Response::json(array('status'=>1, 'info'=>'操作成功'));
			} else {
				return Response::json(array('status'=>0, 'info'=>'操作失败'));				
			}
		}
				
	}
	
	//作业队列
	public function worklist($p = 1){
		
		$work = new Work();
		
		//获取可用试卷的总条数
		$count = $work->work_count();
		$pagesize = Config::get('app.pagesize');
		$start = ($p - 1) * $pagesize;
		$total = ceil( $count / $pagesize );
		$limit = array('start'=>$start, 'pagesize'=>$pagesize);
		$res = $work->worklist($limit);

		return View::make('admin.work.list')
					->with('p', $p)
					->with('total', $total)
					->with('res', $res);
	}
	
	//设置作业 - view
	public function homework(){
				
		//作业数据
		$paper = new Paper();
		$papers = $paper->getAllPaper();
		
		//获取班级和学生
		$class = new Classes();
		$classes = $class->getAllClassesDetail();
		
		return View::make('admin.work.homework')
					->with('papers', $papers)
					->with('classes', $classes);
	}

	//设置作业-action
	public function setwork(){
		
		if (Request::ajax()) {

			$data = Input::all();
			
			$paper = new Paper();
			$class = new Classes();
			$work = new Work();
			
			//获取paper structs字段 和 title字段
			if ($res = $paper->getPaperInfo($data['paperid'], array('title','structs')) ){
				
				//获取老师id
				$user = Cookie::get('user');
				$tid = Helpers\Helper::authcode($user['uid']);

				//用户(不参加) ids
				$uids = $data['uids'];
				//用户(参加)
				$joins = json_decode($data['joins'],true);
				//班级集合
				$cnames = json_decode($data['classes'],true);
								
				$res['structs'] = @gzdecode(base64_decode($res['structs']));
				
				$data = array(
					'tid' => (int)$tid,
					'pid' => (int)$data['paperid'],
					'title'=>$res['title'],
					'structs' => base64_encode( gzcompress(($res['structs']) ,9)),
					'duration'=>(int)$data['duration'], //作业时长
					'startime'=> (int)strtotime($data['startime']), //组卷开始时间
					'endtime'=> (int)strtotime($data['endtime']), //组卷结束时间
					'is_see'=> (int)$data['is_see'], 
					'uids'=> base64_encode(  gzcompress($data['uids'], 9)),
					'creatime'=>$_SERVER["REQUEST_TIME"]					
				);
				
				//发答案时间
				$data['finishtime'] = $data['endtime'] + $data['duration'];
	
				if ( $eid = $work->setWork($data) ) {
					
					//取出 class表里面的examids 字段
					$cids =  array_keys(json_decode($uids,true));
					
					//插入统计表
					$_stat = array();
					$Stat = new Stat();
					$tid = Session::get('tid');
					foreach ($cids as $k=>$v) {
						$_stat[] = array(
							'eid'=>$eid,
							'exam_name'=>$data['title'],
							'cid'=>$v,
							'finishtime'=>$data['finishtime'],
							'tid'=>$tid,
						);
					} 
					$Stat->insert_exam_stat($_stat);

					if ($classInfo = $class->get_classes_info($cids, array('cid','examids'))){
						
						$_data = array(
							'eid' => $eid,
							'title' =>$res['title'],
							'startime'=> $data['startime'], //组卷开始时间
							'endtime' => $data['endtime'],
							'duration'=>$data['duration'],
							'finishtime'=> $data['finishtime'],
							'creatime'=>$data['creatime']
						);
												
						foreach ($classInfo as $key=>$value) {
							$examids = array();	
							if (!empty($value['examids'])){
								$value['examids'] = json_decode(gzuncompress(base64_decode($value['examids'])), true);
								$examids = $value['examids'];
							}
							$examids[$eid] = $_data;
							$examids = base64_encode(gzcompress(json_encode($examids),9));
							
							//修改某个班级信息
							$class->update_class($value['cid'], array('examids'=>$examids));							
						}
						
						/***消息通知放入队列***/
						//1. 重组消息
						if (!empty($joins)) {
							
							$message = array();
							$duration = Helpers\Helper::formatduration($_data['duration']);

							foreach ($joins as $cid=>$val) {
								
								foreach ($val as $key=>$oid) {
									
									$message[] = array(
										'key1'=>date("Y/m/d H:i",$_data['startime']).'-'.date("Y/m/d H:i",$_data['endtime']),//取卷时间
										'key2'=>isset($cnames[$cid]) ? trim($cnames[$cid]) : '',
										'key3'=>'《'.$_data['title'].'》',
										'key4'=>'规定用时:'.$duration,
										'remark'=>empty($data['is_see']) ? "公布成绩时间: ".date("Y/m/d H:i",$_data['finishtime']) : '完成答题立即出成绩' ,
										'openid'=>$oid,
										'cid'=>$cid,
										'eid'=>$eid,
									);
								}
							}

							//2. 放入队列
							$this->pushNote($message);
 						}

						return Response::json(array('status'=>1,'info'=>'设置成功'));					

					} else {
						return Response::json(array('status'=>0,'info'=>'设置失败'));	
					}
				} else {
					return Response::json(array('status'=>0,'info'=>'设置失败'));	
				}
			} else {
				return Response::json(array('status'=>0,'info'=>'设置失败'));	
			}
		}		
	}
	
	//发送消息(内部调用)
	private function pushNote($message) {
		
		if (!empty($message)) {

			$Wx = new WxController();
			$token = $Wx->get_token(true);
						
			foreach ($message as $key=>$val) {
				
				$data = array(
					'message'=>$val,
					'token'=>$token['token']
				);
				//28229 php 进程
				Queue::push("Push@send", array('data'=>$data));
			}
		}
		
	}
	
	
}
