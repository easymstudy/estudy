<?php
/**
 * Description of UserController
 *
 * @author zhaochong
 */

class PaperController extends BaseController {
	
	//test
	public function test(){
		$Paper = new Paper();		
		$Paper->insertPaper(array('pid'=>1,'mid'=>1));
		exit;	
	}
	
	//保存试卷导入的word
	public function savedoc(){
		
		if (Request::ajax()){
			
			$paper = new Paper();
			if ( $paper->save_doc() ) {
				return Response::json(array('status'=>1,'info'=>'操作成功'));		
			} else {
				return Response::json(array('status'=>0,'info'=>'操作失败'));		
			}
		}		
	}
	
	//试卷列表
	public function paperlist($p = 1, $wd = ''){
		
		//取出试卷列表
		$paper = new Paper();

		//获取可用试卷的总条数
		$count = $paper->paper_count();
		
		//计算 开始数
		$pagesize = Config::get('app.pagesize');
		$start = ($p - 1) * $pagesize;
		
		$total = ceil( $count / $pagesize ); 
		if (empty($total)){
			$total = 1;
		}

		$limit = array('start'=>$start, 'pagesize'=>$pagesize);

		$res = array();


		//查询条件
		if (!empty($wd)){
			
			$wd = trim(urldecode($wd));
			
			$count = $paper->search_paper_total($wd);

			if ($count) {
				$total = ceil( $count / $pagesize );
				$res = $paper->search_paper_list($wd, $limit);
			}
			
			/*		
			require Config::get('app.ThirdClass').'sphinxapi.php';

			$sp = new SphinxClient();
			$sp->setServer('112.124.50.14',9312);
			$sp->setMatchMode(SPH_MATCH_ANY);//SPH_MATCH_ALL默认
			//$sp->setLimits($start,$pagesize);
			$res = $sp->query("$wd","paper,deltapaper");
			print_r($res);
			
			if (!empty($res['total'])){
				
				$ids = array_keys($res['matches']);
				$count = count($ids);
				$total = ceil( $count / $pagesize );
				$res = $paper->search_paper_list($ids, $limit);
			} else {
				$res = array();
			}
			*/
					
		} else {
			$res = $paper->paper_list($limit);
		}
		
		return View::make('admin.paper.list')
					->with('p', $p)
					->with('total', $total)
					->with('res', $res);
	}
	
	//拖动后更新试卷 题目顺序
	public function savestructs(){
			
		if (Request::ajax()) {
			
			$data = Input::all();
			$structs = $data['structs'];
			$pid = $data['pid'];
			
			$paper = new Paper();
			if ( $paper->update_structs($pid, $structs) ) {
				return Response::json(array('status'=>1,'info'=>'操作成功'));							
			} else {
				return Response::json(array('status'=>0,'info'=>'操作失败'));		
			}				
		}			
	}
	
	//更新试卷
	public function update_title(){
		
		if (Request::ajax()) {
					
			$Paper = new Paper();
			if ( $Paper->update_paper() ) {
				return Response::json(array('status'=>1,'info'=>'操作成功'));			
			} else {
				return Response::json(array('status'=>0,'info'=>'操作失败'));	
			}	
		}
	}
	
	//创建试卷
	public function create(){
		
		$Paper = new Paper();

		if ( $pid = $Paper->create_paper() ) {
			return Response::json(array('status'=>1,'info'=>'创建成功','pid'=>$pid));			
		} else{
			return Response::json(array('status'=>0,'info'=>'创建失败'));	
		}
	}
		
	public function enter($pid){
		
		//获取已经有的题目-及时保存机制
		if ( empty($pid) ){
			exit('非法操作');
		}
		
		//获取此套试卷的题目
		$paper = new Paper();
		//1.获取的structs
		$structs = $paper->getStructs($pid);

		if ($structs === false) {
			exit('试卷不存在');
		}
	
		//获取word
		$paperInfo =  $paper->getPaperInfo($pid, array('word','title'));	
		
		if (empty($structs)){
			return View::make('admin.paper.enter')
						->with('paperInfo', $paperInfo)
						->with('pid', $pid);	
		}

		//初始化 普通题和复合题ids
		$mc = $gc = array();
		//整理下， 得出复合题id和普通题id 集合
		//print_r($structs);
		foreach ($structs as $k=>$v){
			switch((int)$v['type']){
				case 0:
					$mc[] = (int)$v['id'];
					break ;
				case 1:
					$gc[] = (int)$v['id'];
					break ;
			}			
		}
		
		//实例化普通题model
		$Multiple = new Multiple();

		if (empty($gc)){
			
			//只有普通题目	
			if ($commonQuestions = $Multiple->selectQuestion($mc) ){
				
				//处理字符串
				foreach ( $commonQuestions as $key=>&$value ){
					
					//Helpers\Helper::replaceImg
					//1.处理标题
					$value['title'] = @gzdecode(base64_decode($value['title']));
					//2.处理选项
					$value['choices_list'] = @gzdecode(base64_decode($value['choices_list']));
					//3.处理属性
					$value['attribute_list'] = @gzdecode(base64_decode($value['attribute_list']));									
					
					$arr = array(
						'title'=>$value['title'],
						'choices_list'=>$value['choices_list'],
						'attribute_list'=>$value['attribute_list'],
					);
					
					$str = Helpers\Helper::replaceImg(json_encode($arr));
					$arr = json_decode($str,true);

					$value['title'] = $arr['title'];
					$value['choices_list'] = json_decode($arr['choices_list'], true);
					$value['attribute_list'] = json_decode($arr['attribute_list'], true);
					$value['type'] = 0;										
					//4.处理正确选项
					$value['correct'] = json_decode($value['correct'], true);
				}

				return View::make('admin.paper.edit')
						->with('data', $commonQuestions)
						->with('pid', $pid)
						->with('paperInfo', $paperInfo);
							
			} else {
				exit('数据异常');
			}

		} else {

			//实例化复合题model
			$Group = new Group();
			//取出所有复合题
			if ( $compositeQuestions = $Group->getMids($gc) ){

				//取出子题id
				$ids = $material = array();
				
				foreach ($compositeQuestions as $key=>$value){
					
					$multiple_choice_ids = json_decode($value['multiple_choice_ids'], true);
					if (!empty($multiple_choice_ids)) {
						foreach ($multiple_choice_ids as $k=>$v) {
							$mc[] = $v;		
						} 
					}
					
					//材料集合
					$material[$value['gid']] = @gzdecode(base64_decode($value['material']));
				}
				
				//如果没有子题，只有材料
				if (empty($mc)){
					$compositeQuestions = $compositeQuestions[0];
					$data = array(
						array(
							'type'=>1,
							'gid'=>$compositeQuestions['gid'],
							'material'=>$material[$compositeQuestions['gid']]
						)	
					);
					return View::make('admin.paper.edit')
							->with('pid', $pid)
							->with('data', $data)
							->with('paperInfo', $paperInfo);																																																														
				}
				
				//通过所有子题id 获取
				if ($commonQuestions = $Multiple->selectQuestion($mc) ){
					$data = array();
					//print_r($structs);
					//print_r($commonQuestions);
					foreach ($structs as $key=>$value) {
						foreach ( $commonQuestions as $k=>&$v ) {
							
							//判断类型	
							switch ((int)$value['type']) {
								case 0:
									if ( $v['mid'] == $value['id'] ){
										$v['title'] =  @gzdecode(base64_decode($v['title']));
										$v['choices_list'] =  @gzdecode(base64_decode($v['choices_list']));
										$v['attribute_list'] =  @gzdecode(base64_decode($v['attribute_list']));										
										$data[$key] = $v;
										$data[$key]['type'] = 0;
									}
									break;
								case 1:
									if ( $v['group_choice_id'] == $value['id'] ){
										$data[$key]['type']	= 1;
										$data[$key]['gid'] = $v['group_choice_id'];
										$data[$key]['material'] = $material[$v['group_choice_id']];
										$v['title'] = @gzdecode(base64_decode($v['title']));
										$v['choices_list'] = @gzdecode(base64_decode($v['choices_list']));
										$v['attribute_list'] = @gzdecode(base64_decode($v['attribute_list']));
										$data[$key]['sublist'][] = $v;
										continue;
									}
									
									//如果找到最后都没找到子题 ，说明此题只有材料没有子题
									if (($k+1) == count($commonQuestions)){
										//echo $v['group_choice_id'].'------'.$value['id'];
										$gid = $structs[$key]['id'];
										$data[$key]['gid'] = $gid;
										$data[$key]['type'] = 1;
										$data[$key]['material'] = $material[$gid];

									}
									break;							
							}			
						}
					}

					$data = Helpers\Helper::replaceImg(json_encode($data));
					$data = json_decode($data, true);
				
					foreach ($data as $_key=>&$_value) {
						
						switch ($_value['type']){
							case 0:
								$_value['choices_list'] = json_decode($_value['choices_list'],true);
								$_value['attribute_list'] = json_decode($_value['attribute_list'],true);
								$_value['correct'] = json_decode($_value['correct'],true);
								break;
							case 1:
								if (!empty($_value['sublist'])){
									foreach ($_value['sublist'] as $_k=>&$_v ){
										$_v['choices_list'] = json_decode($_v['choices_list'],true);
										$_v['attribute_list'] = json_decode($_v['attribute_list'],true);
										$_v['correct'] = json_decode($_v['correct'],true);
									}
								}								
								break;
						}
					}

					return View::make('admin.paper.edit')
							->with('data', $data)
							->with('pid', $pid)
							->with('paperInfo', $paperInfo);
							
				} else {
					exit('数据异常');				
				}
							
			} else {
				exit('数据异常');
			}
		}
	}
	
	//预览试卷
	public function paperPreview($pid, $status = ''){

		if ( empty($pid) ){
			exit('非法操作');
		}
		
		//获取此套试卷的题目
		$paper = new Paper();
		
		//1.获取的structs
		$structs = $paper->getStructs($pid);
		
		if ($structs === false) {
			exit('试卷不存在');
		}
	
		//试卷信息
		$paperInfo =  $paper->getPaperInfo($pid, array('title','word'));	
		
		$serial_number = 0;
		$total_points = 0; 
		$data = array();	
					
		if (!empty($structs)) {
			
			//题目数量
			$serial_number = count($structs);
			
			$mc = $gc = array();
			//整理下， 得出复合题id和普通题id 集合

			foreach ($structs as $k=>$v){
				switch($v['type']){
					case 0:
						$mc[] = (int)$v['id'];
						break ;
					case 1:
						$gc[] = (int)$v['id'];
						break ;
				}			
			}
			
			//实例化普通题model
			$Multiple = new Multiple();
			
			if (!empty($gc)){
				
				$Group = new Group();
				
				//取出所有复合题
				if ( $compositeQuestions = $Group->getMids($gc) ){
					//取出子题id
					$ids = $material = array();					
					foreach ($compositeQuestions as $key=>$value){
					
						$multiple_choice_ids = json_decode($value['multiple_choice_ids'], true);
						if (!empty($multiple_choice_ids)) {
							foreach ($multiple_choice_ids as $k=>$v) {
								$mc[] = $v;		
							} 
						}
						//材料集合
						$material[$value['gid']] = @gzdecode(base64_decode($value['material']));
					}
					
					//如果没有子题，只有材料
					if (empty($mc)){
						$compositeQuestions = $compositeQuestions[0];
						$data = array(
							array(
								'type'=>1,
								'gid'=>$compositeQuestions['gid'],
								'material'=>$material[$compositeQuestions['gid']]
							)	
						);
						return View::make('admin.paper.preview')
								->with('pid', $pid)
								->with('data', $data)
								->with('total_points', '')
								->with('serial_number',$serial_number)
								->with('status', $status)
								->with('paperInfo', $paperInfo);																																																														
					}					

					//通过所有子题id 获取
					if ($commonQuestions = $Multiple->selectQuestion($mc) ){
						
						$data = array();
		
						foreach ($structs as $key=>$value) {
							foreach ( $commonQuestions as $k=>&$v ) {
								
								//判断类型	
								switch ((int)$value['type']) {
									case 0:
										if ( $v['mid'] == $value['id'] ){
											$v['title'] =  @gzdecode(base64_decode($v['title']));
											$v['choices_list'] =  @gzdecode(base64_decode($v['choices_list']));
											$v['attribute_list'] =  @gzdecode(base64_decode($v['attribute_list']));										
											$data[$key] = $v;
											$data[$key]['type'] = 0;
										}
										break;
									case 1:
										if ( $v['group_choice_id'] == $value['id'] ){
											$data[$key]['type']	= 1;
											$data[$key]['gid'] = $v['group_choice_id'];
											$data[$key]['material'] = $material[$v['group_choice_id']];
											$v['title'] = @gzdecode(base64_decode($v['title']));
											$v['choices_list'] = @gzdecode(base64_decode($v['choices_list']));
											$v['attribute_list'] = @gzdecode(base64_decode($v['attribute_list']));
											$data[$key]['sublist'][] = $v;
											continue;
										}
											
										//如果找到最后都没找到 子题 ，说明此题只有材料没有子题
										if (($k+1) == count($commonQuestions)){
											$gid = $structs[$key]['id'];
											$data[$key]['gid'] = $gid;
											$data[$key]['type'] = 1;
											$data[$key]['material'] = $material[$gid];
											//$data[$key]['sublist'] = '';
										}										
										break;							
								}
							}
						}
						
						$data = Helpers\Helper::replaceImg(json_encode($data));
						$data = json_decode($data, true);
						
						foreach ($data as $_key=>&$_value) {

							switch ($_value['type']){
								case 0:
									$_value['choices_list'] = json_decode($_value['choices_list'],true);
									$_value['attribute_list'] = json_decode($_value['attribute_list'],true);
									$_value['correct'] = json_decode($_value['correct'],true);

									//计算分数
									$total_points += (int)$_value['score'];									
									
									break;
								case 1:
									if (!empty($_value['sublist'])){
										foreach ($_value['sublist'] as $_k=>&$_v ){
											$_v['choices_list'] = json_decode($_v['choices_list'],true);
											$_v['attribute_list'] = json_decode($_v['attribute_list'],true);
											$_v['correct'] = json_decode($_v['correct'],true);

											//计算分数
											$total_points += (int)$_v['score'];												
										}
									}								
									break;
							}
						}
					} else {
						exit('数据异常');				
					}
				} else {
					exit('数据异常');				
				}
			} else {// end $gc
				
				//普通题
				if ($commonQuestions = $Multiple->selectQuestion($mc) ){

					//处理字符串
					foreach ( $commonQuestions as $key=>&$value ){
						
						//Helpers\Helper::replaceImg
						//1.处理标题
						$value['title'] = @gzdecode(base64_decode($value['title']));
						//2.处理选项
						$value['choices_list'] = @gzdecode(base64_decode($value['choices_list']));
						//3.处理属性
						$value['attribute_list'] = @gzdecode(base64_decode($value['attribute_list']));									
						
						$arr = array(
							'title'=>$value['title'],
							'choices_list'=>$value['choices_list'],
							'attribute_list'=>$value['attribute_list'],
						);
						
						$str = Helpers\Helper::replaceImg(json_encode($arr));
						$arr = json_decode($str,true);
	
						$value['title'] = $arr['title'];
						$value['choices_list'] = json_decode($arr['choices_list'], true);
						$value['attribute_list'] = json_decode($arr['attribute_list'], true);
						$value['type'] = 0;										
						//4.处理正确选项
						$value['correct'] = json_decode($value['correct'], true);
						
						//计算总分
						$total_points += (int)$value['score'];
					}
					$data = $commonQuestions;
				} else {
					exit('数据异常');
				}
			}
		}
		//print_r($data);
		return View::make('admin.paper.preview')
					->with('data', $data)
					->with('serial_number',$serial_number)// 题序号
					->with('total_points', $total_points)//计算分数
					->with('status', $status)
					->with('paperInfo', $paperInfo)
					->with('pid', $pid);
	}	
	
	/*******保存接口********/
	/*
	 * 分别为 标题 选项 属性 
	 */
	//保存标题
	public function saveQuestion(){
		
		if (Request::ajax()) {

			$data = Input::all();
			//通过题目类型不同操作 0普通单题 1复合题
			/*
			 * 如果是普通题，只需要操作multiple_choice和paper。
			 * 如果是复合题，需要操作group_choice和paper 及multiple_choice。
			 */
			$paper = new Paper();
			$multiple = new Multiple();
			
			switch((int)$data['type']) {
				
				case 0:
				
					if ($data['mid']) {
							
						//已经存在普通题id, 直接更新multiple_choice表 title字段
						if ( $multiple->updateQuestion($data) ) {
							//成功
							return Response::json(array('status'=>1, 'info'=>'保存成功'));						
						} else {
							//失败
							return Response::json(array('status'=>0, 'info'=>'保存失败'));
						}
						
					} else{

						//不存在普通题id, 先插入 multiple_choice表获取mid, 再往paper 表structs字段插入mid等
						if ( $mid = $multiple->insertQuestion($data) ){
							
							$arr = array(
								'pid'=>$data['pid'],
								'mid'=>(int)$mid,
								'type'=>0,
							);
							
							if ( $paper->updateStructs($arr) ) {
								return Response::json(array('status'=>1, 'info'=>'保存成功' , 'mid'=>$mid));														
							} else {
								return Response::json(array('status'=>0, 'info'=>'保存失败'));
							}	
						} else {
							return Response::json(array('status'=>0, 'info'=>'保存失败'));
						}
					}
					break;
				
				case 1:
					
					$group = new Group();
					
					//已经有子题id，必然有复合题id, 直接更新
					if ($data['qid']) {
						$data['mid'] = $data['qid'];
						
						if ( $multiple->updateQuestion($data) ) {
							return Response::json(array('status'=>1, 'info'=>'保存成功'));
						} else {
							return Response::json(array('status'=>0, 'info'=>'保存失败'));
						}
					} else {
						
						//没有子题id ， 判断是否有复合题id
						//没有子题id ,先添加子题
						if ( $mid = $multiple->insertQuestion($data) ) {
							
							if ($data['mid']) {
								
								//有复合题id
								$arr = array(
									'qid'=>$mid, //子题id 
									'mid'=>$data['mid'], //复合id
									'field'=>'multiple_choice_ids'
								);
								
								//更新下group_choice表multiple_choice_ids
								if ( $group->updateGroup($arr) ) {
									return Response::json(array('status'=>1, 'info'=>'保存成功', 'qid'=>$mid));					
								} else {
									return Response::json(array('status'=>0, 'info'=>'保存失败'));									
								}								
									
							} else {
								
								$arr = array(
									'mid'=> $mid,					
									'field'=>'multiple_choice_ids',
								);
																
								//没有复合题id, 新增复合题
								if ( $gid = $group->insertGroup($arr) ){
									
									$arr = array(
										'pid'=>$data['pid'],
										'mid'=>(int)$gid,
										'type'=>1,
									);
									
									//更新paper表和multiple_choice表
									if ( $paper->updateStructs($arr) ){
										
										//更新multiple_choice表
										$multiple = new Multiple();
										
										$arr = array(
											'mid'=>$mid,
											'field'=>'group_choice_id',
											'gid'=>$gid,
										);
										
										$multiple->updateQuestion($arr);
																		
										return Response::json(array('status'=>1, 'info'=>'保存成功', 'mid'=>$gid, 'qid'=>$mid));										
									} else {
										return Response::json(array('status'=>0, 'info'=>'保存失败'));
									}									
								} else {
									return Response::json(array('status'=>0, 'info'=>'保存失败'));									
								}
							}
						} else {
							return Response::json(array('status'=>0, 'info'=>'保存失败'));
						}
					}					 
					break;
			}
		}
	}
	
	//保存材料
	public function saveMaterial(){
		
		if (Request::ajax()) {
		
			$data = Input::all();
			
			$group = new Group();
			
			//此位置 mid为复合题id		
			if ( $data['mid'] ) {
				//直接更新材料  
				if ( $group->updateGroup($data)) {
					return Response::json(array('status'=>1, 'info'=>'保存成功'));
				} else {
					return Response::json(array('status'=>0, 'info'=>'保存失败'));					
				}
												
			} else{
				
				//添加材料，并且更改structs字段
				if ($gid = $group->insertGroup($data) ) {
					
					$arr = array(
						'pid'=>$data['pid'],
						'mid'=>$gid,
						'type'=>$data['type'],
					);
					
					$paper = new Paper();
					
					if ( $paper->updateStructs($arr) ) {
						return Response::json(array('status'=>1, 'info'=>'保存成功', 'mid'=>$gid));
					} else {
						return Response::json(array('status'=>0, 'info'=>'保存失败'));
					}
							
				} else {
					return Response::json(array('status'=>0, 'info'=>'保存失败'));
				}
			}
		}
	}
	
	//提交审核
	public function submitcheck($pid, $score, $count){
		
		if ( empty($pid) ){
			exit('非法操作');
		}
		
		//试卷信息
		$paper = new Paper();
		$paperInfo =  $paper->getPaperInfo($pid, array('title'));	
		
		//显示 题目数 分数
		return View::make('admin.paper.check')
					->with('score', $score)
					->with('count', $count)
					->with('paperInfo', $paperInfo)
					->with('pid', $pid);
	}
	
	//审核试卷， 根据传过来的参数 来做不同操作
	public function paperCheck(){
		
		if (Request::ajax()) {
			
			$paper = new Paper();
			
			$data = Input::all();
			
			if ( $res = $paper->changeStatus($data)){
				return Response::json(array('status'=>1, 'info'=>'操作成功'));
			} else {
				return Response::json(array('status'=>0, 'info'=>'操作失败'));				
			}
		}
	}
	
	//删除试卷
	public function paperdelete(){
		
		if (Request::ajax()) {
			
			$paper = new Paper();
			
			//找到所有的试题id ，删除
			$data = Input::all();
			
			$ids = json_decode($data['ids'], true);
			
			//查找structs字段
			if (!empty($ids)) {
				if ($structs = $paper->getMoreStructs($ids) ){
					
					$gc = $mc =array();
					
					foreach ($structs as $k=>&$v) {
						$v['structs'] = @json_decode(gzdecode(base64_decode($v['structs'])), true); 						
						
						if (!empty($v['structs'])) {
							
							foreach ($v['structs'] as $_k=>$_v) {
								switch ((int)$_v['type']) {
									case 0:
										$mc[] = $_v['id'];
										break;
									case 1:
										$gc[] = $_v['id'];
										break;
								}		
							}
						}
					}
					
					$Multiple = new Multiple();
					//2种不同的删除
					if (!empty($mc)) {
						$Multiple->deleteQ($mc);		
					}
					
					if (!empty($gc)) {
						$Multiple->deleteGq($gc);
					}
					
					if ( $res = $paper->changeStatus($data)){
						return Response::json(array('status'=>1, 'info'=>'操作成功'));					
					}

				}
			}
			return Response::json(array('status'=>0, 'info'=>'操作失败'));
			//file_put_contents("1.log", var_export($ids,true));
		}
					
	}
	
	
	//删除
	public function deleteQuestion(){
		
		if (Request::ajax()) {

			$data = Input::all();
	
			$paper = new Paper();
					
			//删除题目-简单操作 直接从试卷 里面 删除
			if ($structs = $paper->getStructs($data['pid'])){
				
				foreach ($structs as $k=>$v) {
					if ( $v['id'] == $data['id'] ){
						unset($structs[$k]);
						break;
					}				
				}
				//更新$structs
				if ( $paper->update_structs($data['pid'], $structs) ) {
						return Response::json(array('status'=>1, 'info'=>'操作成功'));		
				} else {
						return Response::json(array('status'=>0, 'info'=>'操作失败'));
				}
											
			} else {
				return Response::json(array('status'=>0, 'info'=>'操作失败'));
			}
		}
	}
	
}