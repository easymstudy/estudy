<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 */
class ClassController extends BaseController {
	
	//删除班级
	public function deleteClass(){
		
		if (Request::ajax()) {
			
			$Class = new Classes();
			$data = Input::all();
			
			if ( $res = $Class->delete_class($data) ) {
				return Response::json(array('status'=>1, 'info'=>'操作成功'));
			} else {
				return Response::json(array('status'=>0, 'info'=>'操作失败'));		
			}
		}
	}
	
	//班级列表	
	public function index($p = 1,$wd = '') {

		$Class = new Classes();
		//得到班级总条数 ,相对于老师
		$count = $Class->class_count();
		$pagesize = Config::get('app.pagesize');
		$start = ($p - 1) * $pagesize;
		$total = ceil( $count / $pagesize );
		$limit = array('start'=>$start, 'pagesize'=>$pagesize);																														
		if (empty($total)){
			$total = 1;
		}

		//初始化
		$res = array();
		
		//查询条件
		if (!empty($wd)){
			
			$wd = trim(urldecode($wd));
			
			$count = $Class->search_class_total($wd);
			
			if ($count) {
				$total = ceil( $count / $pagesize );
				$res = $Class->search_class_list($wd, $limit);
			}
			/*
			require Config::get('app.ThirdClass').'sphinxapi.php';

			$sp = new SphinxClient();
			$sp->setServer('112.124.50.14',9312);
			$sp->setMatchMode(SPH_MATCH_ANY);//SPH_MATCH_ALL默认
			//$sp->setLimits($start,$pagesize);
			$res = $sp->query("$wd","classes,deltaclass");
		
			if (!empty($res['total'])){
				
				$ids = array_keys($res['matches']);
				$count = count($ids);
				$total = ceil( $count / $pagesize );
				$res = $Class->search_class_list($ids, $limit);
				
			} else {
				$res = array();
			}
			*/
			
		} else {
			$res = $Class->get_class_list($limit);
		}
		
		return View::make('admin.class.list')->with('res', $res)
											 ->with('p', $p)
											 ->with('total', $total);
	}
	
		
	//班级动态-基础信息
	public function detail($cid, $p=1){
		
		$Class = new Classes();
		$res = $Class->get_class_defail($cid);
		
		//获取此班级的学生列表
		$Class_user =  new ClassUser();
		$student = $Class_user->get_class_userinfo($cid);
		$student = @json_decode( gzuncompress(base64_decode($student['uids'])),true);

		$jtime = array();
		//初始化 total等
		$total = $count = ''; 
		
		if ( !empty($student) ) {
			
			$count = count($student);
			$pagesize = Config::get('app.pagesize');
			$start = ($p - 1) * $pagesize;
			$total = ceil( $count / $pagesize ); 
			//通过第几页获取 学院信息
			$student = array_slice($student, $start, $pagesize);
			
		}
		
		if ( $res ) {
			return View::make('admin.class.detail')->with('res', $res)
												   ->with('student', $student)
												   ->with('cid', $cid)
												   ->with('count', $count)
												   ->with('total', $total)
												   ->with('p', $p);
		} else {
			exit('班级不存在');
		}
	}
	
	//班级动态-某个学生记录
	public function studentInfo($cid, $uid){
		
		//获取此班级信息
		$Class = new Classes();
		$class_res = $Class->get_class_defail($cid);
		
		//初始化数据
		if (!$class_res)
			exit('班级不存在');				
		
		//通过uid 获取 用户名和头像
		$userInfo = array(
			'nickname'=>'',
			'remark'=>'备注名',
			'headimgurl'=>'',		
		);
		
		$User = new User();
		$userInfo = $User->get_user_info($uid);
		
		//获取此用户的基础统计 正常交作业次数  总答题数 答对
		$req_url = Config::get('app.api_url')."/user/exam/totalstat/{$cid}/{$uid}";
		$totalstat = json_decode(Helpers\Helper::curl($req_url), true);
		
				
		//获取此学生所有交作业的时间
		$UserExam = new UserExam();
		$exams = $UserExam->user_exam_info($cid,$uid, array('ueid', 'eid', 'endtime'));
		
		//获取此学生最近几次作业
		$req_url = Config::get('app.api_url')."/user/exam/chart/{$cid}/{$uid}/5";
		$statimg = json_decode(Helpers\Helper::curl($req_url), true);

		$imgdata = array(
			'time'=>'',
			'user'=>'',
			'class'=>''
		);
		if (!empty($statimg['data'])){
			$statimg = array_reverse($statimg['data']);
			foreach ($statimg as $k=>$v) {
				$imgdata['time'][] = $v['submit_time'];
				if (empty($v['join_num']) || empty($v['full_points'])){
					$imgdata['user'][] = 0;
					$imgdata['class'][] = 0;
				} else {
					$imgdata['user'][] = round($v['score']/$v['full_points'], 2)*100;
					$imgdata['class'][] = round(($v['total_points']/$v['join_num'])/$v['full_points'], 2)*100;
				}
			}	
		}

		return View::make('admin.class.studenInfo')->with('class_res', $class_res)
												   ->with('totalstat', $totalstat['stat'])
												   ->with('userInfo', $userInfo)
												   ->with('uid', $uid)
												   ->with('imgdata', json_encode($imgdata))
												   ->with('exams', $exams);
	}
	
	//班级动态-某个学生记录-获取某次作业统计数据
	public function getclasstatistics(){
		
		if (Request::ajax()){
			
			$data = Input::all();
			
			//先查询试卷名和作业时间
			$Exam = new Exam();
			$examinfo = $Exam->select_exam_info($data['eid']);
			if (!empty($examinfo['startime'])) {
				$examinfo['startime'] = date('Y年m月d日 H:i', $examinfo['startime']);	
			}
			if (!empty($examinfo['endtime'])) {
				$examinfo['endtime'] = date('Y年m月d日 H:i', $examinfo['endtime']);	
			}
			//统计
			$req_url = Config::get('app.api_url')."user/exam/stat/{$data['cid']}/{$data['uid']}/{$data['eid']}/{$data['ueid']}";
			$examstat = json_decode(Helpers\Helper::curl($req_url), true);
			
			//题目正确
			$UserExam = new UserExam();
			$examdetail = array();
			$res = $UserExam->user_submit_detail($data['ueid']);
			if (!empty($res['exam_submit'])) {
				$examdetail = json_decode(gzuncompress(base64_decode($res['exam_submit'])), true);				
			}
			
			return Response::json(array('status'=>1, 'info'=>'操作成功','examstat'=>$examstat['stat'], 'examinfo'=>$examinfo, 'examdetail'=>$examdetail));
			
		}
	}
	
	//班级-作业列表
	public function worklist($cid, $p=1){
		
		//获取此班级信息
		$Class = new Classes();
		$class_res = $Class->get_class_defail($cid);		

		
		if (!$class_res)
			exit('班级不存在');
		
		//初始化
		$examlist = array();
		$total = 1;
		
		if (!empty($class_res['examids'])) {
			
			$_examlist = json_decode(gzuncompress(base64_decode($class_res['examids'])),true);
		
			$data = $ids = array();
			foreach ($_examlist as $key=>$val) {
				$data[$val['eid']] = $val;
				$ids[] = $val['eid'];				
			}
			
			//数据库查询到的及时数据			
			if (!empty($ids)) {
				$Exam = new Exam();
				
				if ( $examlist = $Exam->select_class_exam($cid, $ids) ) {

					//分页
					$count = count($examlist);
					$pagesize = Config::get('app.pagesize');
					$start = ($p - 1) * $pagesize;
					$total = ceil( $count / $pagesize );
					
					//通过第几页获取 学院信息
					$examlist = array_slice($examlist, $start, $pagesize);	
				}
			}
		}	
		return View::make('admin.class.worklist')->with('class_res', $class_res)
												 ->with('examlist', $examlist)
												 ->with('total', $total)
												 ->with('p', $p);		
	}
	
	//班级下某次作业详情
	public function workstat($cid, $eid){

		//获取此班级信息
		$Class = new Classes();
		$class_res = $Class->get_class_defail($cid);	

		if (!$class_res)
			exit('班级不存在');
		
		//统计
		$req_url = Config::get('app.api_url')."admin/class/exam/{$cid}/{$eid}";
		$examstat = json_decode(Helpers\Helper::curl($req_url), true);
		
		//作业每题对错
		$Stat = new Stat();
		$res = $Stat->get_item_detail($cid,$eid);
		$item = array();
		if (!empty($res['item_detail'])) {
			
			$item_detail = json_decode(gzuncompress(base64_decode($res['item_detail'])), true);			
			$i = 1;
			
			foreach ($item_detail as $key=>$val) {
				$item['key'][] = $i;
				$i++;
				$item['right'][] = $val['right'];
				$item['error'][] = $val['error'];			
			}
		}
		$item = json_encode($item);
									
		return View::make('admin.class.workstat')->with('class_res', $class_res)
												 ->with('examstat', $examstat['stat'])
												 ->with('item', $item)
												 ->with('eid', $eid);
	}
	
	//班级下某次作业详情-排名
	public function stat_rank(){
		
		if (Request::ajax()){
			
			$data = Input::all();
			$model = 'DESC';
			switch ((int)$data['type']) {
				case 1:
					$order = 'score';
					break;			
				case 2:
					$order = 'answer_time';
					$model = 'ASC';
					break;			
				case 3:
					$order = 'right_num';
					break;
				default :
					$order = 'score';			
			}
			
			//排名
			$UserStat = new UserStat();
			$ranking = $UserStat->exam_rank($data['cid'], $data['eid'], $order, $model);
			return Response::json(array('status'=>1, 'info'=>'操作成功','ranking'=>$ranking));
		}
	}
		
	//修改班级-view
	public function modify($cid){
		
		//如果没有二维码则获取
		$Class = new Classes();
		if ($res = $Class->get_class_info($cid)) {
			return View::make('admin.class.modify')->with('res', $res);
		} else {
			exit('班级不存在');					
		}
	}
	
	//修改班级-action
	public function _modify(){
		
		if (Request::ajax()) {
			
			$Class = new Classes();
			
			$_data = Input::all();
			
			//验证班级是否已经存在
			if ($Class->checkClass($_data['cid'])) {
				return Response::json(array('status'=>-2,'info'=>'此班级已存在'));				
			}			
			
			if ( $cid = $Class->update_class_info()) {
				return Response::json(array('status'=>1,'info'=>'修改成功', 'cid'=>$cid));	
			} else{
				return Response::json(array('status'=>-1,'info'=>'修改失败'));				
			}
		}
	}
	
	//创建班级
	public function create() {
		
		if (Request::ajax()) {
			
			//先检测是否绑定公众帐号
			$Wx = new Wx();
			$res = $Wx->get_app_info();
			
			if (!$res){
				return Response::json(array('status'=>-1,'info'=>'请先绑定微信公众帐号'));				
			} else {
				
				$Class = new Classes();
				
				//班级-验证是否重名
				if ($Class->checkClass()) {
					return Response::json(array('status'=>-2,'info'=>'此班级已存在'));				
				}
								
				if ( $cid = $Class->createClass() ) {
					
					//生成班级二维码
					require Config::get('app.ThirdClass').'phpqrcode/phpqrcode.php';
					$errorCorrectionLevel = 'L';//容错级别   
					$matrixPointSize = 6;//生成图片大小
					$value = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$res['appId']."&redirect_uri=".urlencode("http://admin.mstudy.me/wx/authorize/3/{$cid}")."&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
					$name = md5("class{$cid}");
					$code = Config::get('app.Public').'/data/qcode/'.md5("class{$cid}").'.png';
					QRcode::png($value, $code, $errorCorrectionLevel, $matrixPointSize, 2);				
					//更新二维码地址
					$Class->update_code($name,$cid);
					
					return Response::json(array('status'=>1,'info'=>'创建成功', 'cid'=>$cid));		
				} else {
					return Response::json(array('status'=>0,'info'=>'创建失败'));				
				}			
			}
		}
	}
	
	//创建班级结果
	public function success($cid) {

		//如果没有二维码则获取
		$Class = new Classes();
		$res = $Class->get_class_info($cid);

		if ($res){

			return View::make('admin.class.success')->with('res',$res);		

		}else{
			//班级不存在
			exit('班级不存在');
		}
	}
	
	//获取老师二维码
	public function getcode(){
		
		if (Request::ajax()) {
			
			$data = Input::all();
			$Code = new ClassCode();
			if ( $res = $Code->getCode($data['cid'])) {
				return Response::json(array('status'=>1,'info'=>'获取成功', 'codes'=>$res));		
			} else {
				return Response::json(array('status'=>0,'info'=>'获取失败'));		
			}			
		}
	}
	

	//生成老师二维码
	public function teacherCode() {
		
		if (Request::ajax()) {

			$data = Input::all();
			//先看此班级下是否已经有了6位老师
			$Code = new ClassCode();
			$count = $Code->count_teacher($data['cid']);
			
			if ($count == 6) {
				return Response::json(array('status'=>0,'info'=>'最多生成6个绑定二维码'));		
			} 
			
			//获取公众帐号信息
			$Wx = new Wx();
			$res = $Wx->get_app_info();
			
			//先新增一条记录
			$data = array(
				'cid'=>$data['cid'],
				'status'=>0, //未绑定
			);			
			
			if ( $qid = $Code->insert_code($data) ) {
				
				//生成二维码
				require Config::get('app.ThirdClass').'phpqrcode/phpqrcode.php';
				$errorCorrectionLevel = 'L';//容错级别   
				$matrixPointSize = 6;//生成图片大小
				$value = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$res['appId']."&redirect_uri=".urlencode("http://admin.mstudy.me/wx/authorize/4/{$data['cid']}/{$qid}")."&response_type=code&scope=snsapi_base&state=1#wechat_redirect";
				$name = md5($_SERVER["REQUEST_TIME"]);
				$_code = Config::get('app.Public').'/data/tcode/'.$name.'.png';
				QRcode::png($value, $_code, $errorCorrectionLevel, $matrixPointSize, 2);				
				
				$data = array(
					'qid'=>$qid,
					'code_name'=>$name,
				);	
				
				//更新二维码地址
				$Code->update_code($data);
				return Response::json(array('status'=>1,'info'=>'新增成功', 'tcodename'=>$name));					
								
			} else {
				return Response::json(array('status'=>0,'info'=>'新增失败'));					
			}
			
		}
	}
	
	//解除绑定
	public function removebind(){
		
		if (Request::ajax()) {
			
			$apiurl = Config::get('app.api_url').'/teacher/quit/coach';
			$data = Input::all();			
			
			$_data['post'] = array(
				'qid'=>$data['qid'],
				'tid'=>$data['tid'],
				'cid'=>$data['cid']
			);
						
			$res = json_decode(Helpers\Helper::curl($apiurl, $_data), true);
			
			if ($res['status'] == 1) {
				return Response::json(array('status'=>1,'info'=>'退出成功'));							
			} else {
				return Response::json(array('status'=>0,'info'=>'操作失败'));			
			}
		}
		
	}
	
	//设置备注名
	public function setremark(){
		
		if (Request::ajax()) {
				
			$_data = Input::all();
			$_data['remark'] = 	strip_tags(trim($_data['remark']));
				
			$User = new User();
			//如果一样直接返回
			$res = $User->get_user_info($_data['uid']);
			if ($res['remark'] == $_data['remark']) {
				return Response::json(array('status'=>1,'info'=>'操作成功'));
			}
			
			//更新操作			
			if ($User->setremark($_data)) {
				
				//更新class_user表
				$ClassUser = new ClassUser();
				$student = $ClassUser->get_class_userinfo($_data['cid']);
				$student = @json_decode( gzuncompress(base64_decode($student['uids'])),true);
				
				$student[$_data['uid']]['remark'] = $_data['remark'];
				$uids = @base64_encode(gzcompress(json_encode($student),9));
				$ClassUser->update_class_userinfo($_data['cid'], $uids);									
				return Response::json(array('status'=>1,'info'=>'操作成功'));							
			}
			return Response::json(array('status'=>0,'info'=>'操作失败'));			
		}
	}
	
	/*
	//获取二维码
	public function get_code($cid){
		
		$rqcode = '';
		
		$Wx = new Wx();
		//1.获取appid等
		$app = $Wx->get_app_info();	

		//通过redis缓存
		$Redis = Illuminate\Support\Facades\Redis::connection();
		$access_token = $Redis->GET($app['account']);
		if ($access_token) 
			goto Rq;
		
		//通过班级id 获取 二维码url
		$token = $Wx->get_access_token($app);
		if ($token) {
			$access_token = $Redis->SETEX($app['account'],$token['expires_in']-300,$token['access_token']);
		}
		//设置跳转标识
		Rq:
		//echo $token['access_token'].'<br/>';
		
		//通过token获取永久二维码
		if ( $ticket = $Wx->getQR($access_token, $cid)){

			//更新班级表-二维码
			$Class = new Classes();
			$Class->update_code($ticket,$cid);
			$rqcode = urlencode($ticket);
		}		
		
		return $rqcode;
	}
	*/
}
