<?php
/**消息提醒**/

class Push {
	
	public function send($job, $res){
		
		$url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$res['data']['token']}";
		$message = $res['data']['message'];
				
		$data = array(
			'touser'=>$message['openid'],
			'template_id'=>"5Fvi6xvZ-EXjVoRGE2aUbFFW0LV1NVdJPWH1ie24qDE",
			'url'=>"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf305ea0876e9955e&redirect_uri=".urlencode("http://admin.mstudy.me/wx/authorize/7/{$message['cid']}/0/{$message['eid']}")."&response_type=code&scope=snsapi_base&state=1#wechat_redirect",
			'topcolor'=>'#FF0000',
			'data'=>array(
				'first'=>array(
					"value"=>"你有一份新作业",
					"color"=>"#009245"
				),
				'keyword1'=>array(
					"value"=>"{$message['key1']}",
					"color"=>"#173177"
				),
				'keyword2'=>array(
					"value"=>"{$message['key2']}",
					"color"=>"#173177"
				),
				'keyword3'=>array(
					"value"=>"{$message['key3']}",
					"color"=>"#173177"
				),
				'keyword4'=>array(
					"value"=>"{$message['key4']}",
					"color"=>"#173177"
				),
				'remark'=>array(
					"value"=>"{$message['remark']}",
					"color"=>"#173177"
				)
			)
		);
						
		//请求接口推送
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0 );
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2 );
		curl_setopt($curl, CURLOPT_TIMEOUT, 10 );
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_POST, 1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
	
		$result = curl_exec ($curl);
		curl_close ( $curl );
		
		//删除队列的工作
		$job->delete();
		
	}
	
}