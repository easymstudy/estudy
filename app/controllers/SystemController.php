<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 */

class SystemController extends BaseController {
	
	//验证规则
	private $rule = array(
		'username' => array('required','regex:/^13\d{9}$|^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i'),
		'password' => array('required','min:6'),    
	);
	
	//自定义error
	private $message = array(
		'username.required' => '帐号不能为空!',
		'username.regex' => '帐号不合法!',
		'password.required' => '密码不能为空!',
		'password.min' => '密码不合法!',	
	);
	
	//修改密码
	public function changePass() {

		if (Request::ajax()){
			
			$data = Input::all();
			
			//验证表单数据合法
            $input = array(
				'username' => $data['user'],
                'password' => $data['pass'],
				'password_confirmation' => $data['repass'],
            );
			$this->rule['password'][] = 'confirmed';
			$this->message['confirmed'] = '两次输入的密码不一致!';
			
			$validator = Validator::make($input, $this->rule, $this->message);
			//验证失败信息
            if ( $validator->fails() ) {
            	$message = current($validator->messages()->toArray());
				return Response::json(array('status'=>0,'info'=>$message[0]));
            }
			
			//通过uid修改密码
			$user = Cookie::get('user');
			$data['uid'] = Helpers\Helper::authcode($user['uid']);
			
			$user = new User();
			
			if ( $user->changPassword($data) ){
				return Response::json(array('status'=>1,'info'=>'密码修改成功'));
			} else {
				return Response::json(array('status'=>0,'info'=>'操作失败'));
			}
			
		} else {
			return Response::json(array('status'=>0,'info'=>'非法操作'));		
		}
	}
	
	//绑定微信
	public function bindWx() {
		
		//检测是否已经绑定
		$Wx = new Wx();
		$res = $Wx->getWxInfo();
		return View::make('admin.system.bindwx')->with('res',$res);	
	}
	
}
