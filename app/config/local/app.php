<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application Debug Mode
	|--------------------------------------------------------------------------
	|
	| When your application is in debug mode, detailed error messages with
	| stack traces will be shown on every error that occurs within your
	| application. If disabled, a simple generic error page is shown.
	|
	*/

	'debug' => true,
	'passKey'=>'study',
	'myPublic' => $_SERVER['DOCUMENT_ROOT'].'/public/',
	'Public' => $_SERVER['DOCUMENT_ROOT'],
	'pagesize' => 10,
	'ThirdClass'=>$_SERVER['DOCUMENT_ROOT'].'/../app/libraries/Class/',
	'api_url'=>'http://apis.mstudy.me:8088/',
	
	
);
