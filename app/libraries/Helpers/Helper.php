<?php 
namespace Helpers;
class Helper {
	/*
	 *  生成 随机字符串 ,默认43位数
	 *  字符范围为A-Z，a-z，0-9。
	 */
	public static function randomkeys($length = 43){
	
		$key = '';
		$pattern = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ';
		for ($i=0; $i<$length; $i++) {
			 $key .= $pattern{mt_rand(0, strlen($pattern)-1)};
		}
		return $key;
	}
	
	/*
	 * 加密解密
	 * $string 提供需要加密的字符串
	 * $operation   加密方式，ENCODE是加密，DECODE是解密
	 * $key  密钥
	 */	
	public static function authcode($string, $operation = 'DECODE', $key = 'study', $expiry = 0) {
		$ckey_length = 4;   
		$key = md5($key);   
		$keya = md5(substr($key, 0, 16));   
		$keyb = md5(substr($key, 16, 16));   
		$keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length): substr(md5(microtime()), -$ckey_length)) : '';   
		  
		$cryptkey = $keya.md5($keya.$keyc);   
		$key_length = strlen($cryptkey);   
		  
		$string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0).substr(md5($string.$keyb), 0, 16).$string;   
		$string_length = strlen($string);   
		  
		$result = '';   
		$box = range(0, 255);   
		  
		$rndkey = array();   
		for($i = 0; $i <= 255; $i++) {   
			$rndkey[$i] = ord($cryptkey[$i % $key_length]);   
		}   
		  
		for($j = $i = 0; $i < 256; $i++) {   
			$j = ($j + $box[$i] + $rndkey[$i]) % 256;   
			$tmp = $box[$i];   
			$box[$i] = $box[$j];   
			$box[$j] = $tmp;   
		}   
		  
		for($a = $j = $i = 0; $i < $string_length; $i++) {   
			$a = ($a + 1) % 256;   
			$j = ($j + $box[$a]) % 256;   
			$tmp = $box[$a];   
			$box[$a] = $box[$j];   
			$box[$j] = $tmp;   
			$result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));   
		}   
		  
		if($operation == 'DECODE') {   
			if((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26).$keyb), 0, 16)) {   
				return substr($result, 26);   
			} else {   
					return '';   
				}   
		} else {   
			return $keyc.str_replace('=', '', base64_encode($result));   
		}   
		  	
	}
		
    /*
	 * curl请求
	 * $url 提供需要请求的链接
	 * $data  请求的参数 默认为get请求 如果有post参数为post请求
     * $cookie 设置cookie
     * $header 设置请求头 默认为空 
	 * $is_header 是否返回头信息 默认为0 不显示 1显示
	 * $file 设置 读取的cookie文件
     * $timeout 设置超时时间 默认10秒
	 */
	 public static function curl($url, $data='', $cookie='', $headers='', $is_header = 0, $file='', $timeout = 10 ){
           
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0 );
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2 );
            curl_setopt($curl, CURLOPT_HEADER, $is_header );            
            curl_setopt($curl, CURLOPT_TIMEOUT, $timeout );
            
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            
            //设置头信息
            if ( !empty($headers) ){
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            }
            
            //设置cookie
            if ( !empty($cookie) ) {
                curl_setopt($curl, CURLOPT_COOKIE, $cookie);
            }
			
			//设置读取cookie文件
			if ( !empty($file)) {
	           curl_setopt($curl, CURLOPT_COOKIEFILE, $file);
     		}
	
            //post请求
            if ( isset($data['post'])) {
                curl_setopt($curl, CURLOPT_POST, 1);

				if (isset($data['post']['type']) && $data['post']['type']== 'json'){
					//传递json字符串
					curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data['post']));
				}else{
					curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data['post']));
				}
                
            }
            
            $result = curl_exec ($curl);
            curl_close ( $curl );
            return $result;
            
        }
	
	/*
	 * 过滤html标签
	 * $string 需要过滤的字符串或数组
	 */
	public static function dhtmlspecialchars($string, $flags = null) {

		if(is_array($string)) {
			foreach($string as $key => $val) {
				$string[$key] = Helper::dhtmlspecialchars($val, $flags);
			}
		} else {
			if($flags === null) {
				$string = str_replace(array('&', '"', '<', '>'), array('&amp;', '&quot;', '&lt;', '&gt;'), $string);
				if(strpos($string, '&amp;#') !== false) {
					//过滤掉类似&#x5FD7的16进制的html字符
					$string = preg_replace('/&amp;((#(\d{3,5}|x[a-fA-F0-9]{4}));)/', '&\\1', $string);
				}
			} else {
				if(PHP_VERSION < '5.4.0') {
					$string = htmlspecialchars($string, $flags);
				} else {
					if(strtolower(CHARSET) == 'utf-8') {
						$charset = 'UTF-8';
					} else {
						$charset = 'ISO-8859-1';
					}
					$string = htmlspecialchars($string, $flags, $charset);
				}
			}
		}
		return $string;		
	}
	
	/*
	 * 把img标识 替换为img 标签
	 * $string 需要格式化的字符串
	 */
	public static function replaceImg($_data){

		if (!empty($_data)) {
			
			$reg = '/(.*)\[img\](.*)\[!img\](.*)/iUs';
			$str = '';	
			$str .= preg_replace_callback($reg, function($matches){
					$img = explode("?",$matches[2]);
					$width = substr($img[1], 2);
					return $matches[1]."<img src='http://www.easymbook.com/resource/image?h={$img[0]}' width='{$width}' />";
			} , $_data);
			
			$_data = $str;
		}
		return $_data;
	}
	
	/*
	 * 格式化做题时长
	 * $n 作业的做题时长
	 */
	public static function formatduration($n){
		
		$h = floor($n/3600);
		$m = ($n%3600)/60;
		$s = ($n%3600)%60;
		
		$str = $h.'小时';
		
		if (!empty($m))
			$str .= floor($m).'分';		
		
		if (!empty($s))
			$str .= $s.'秒';
		
		return $str;
		
	}
}

