var app = angular.module('hhmAngular', [
  'ngRoute',
]);



app.run(function($rootScope) {
  $rootScope.itemtype=['复合题','单选题','多选题'];
  $rootScope.abcd=['A','B','C','D','E','F','G','H'];
}); 




app.controller('MainController', function($rootScope, $scope){
  // 监听路由开始后且完成前展示过渡动画
  $rootScope.$on('$routeChangeStart', function(){
    $rootScope.loading = true;
  });
  $rootScope.$on('$routeChangeSuccess', function(){
    $rootScope.loading = false;
  });

});


app.controller('classInfo', function($scope){
  // char js图表开始
  var randomScalingFactor = function(){ return Math.round(Math.random()*50)};
  var lineChartData = {
    labels : ["Jan","Feb","Mar","Apr","May","Jun","Jul"],
    datasets : [
      {
        label: "My First dataset",
        fillColor : "rgba(220,220,220,0.2)",
        strokeColor : "rgba(220,220,220,1)",
        pointColor : "rgba(220,220,220,1)",
        pointStrokeColor : "#fff",
        pointHighlightFill : "#fff",
        pointHighlightStroke : "rgba(220,220,220,1)",
        data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
      },
      {
        label: "My Second dataset",
        fillColor : "rgba(151,187,205,0.2)",
        strokeColor : "rgba(151,187,205,1)",
        pointColor : "rgba(151,187,205,1)",
        pointStrokeColor : "#fff",
        pointHighlightFill : "#fff",
        pointHighlightStroke : "rgba(151,187,205,1)",
        data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
      }
    ]

  }
  var ctx = document.getElementById("canvas").getContext("2d");
  window.myLine = new Chart(ctx).Line(lineChartData, {
    responsive: true
  });
  // js图表结束
});

app.controller('paperList', ['$scope', '$http',
  function($scope, $http) {
    $http.get('./data/paperlist.json').success(function(data) {
      $scope.paperlist = data;
    });
}]);

// 答题控制器
app.controller('exam', ['$scope', '$http','$location', '$anchorScroll',
  function($scope, $http, $location, $anchorScroll) {
    $http.get('./data/exam.json').success(function(data) {
      $scope.examlist =data;
      $scope.boxSlideToggleValue=false; //未开启面板
      $scope['itemOverNum']=0;  //用户已经做完的题目数量
      $scope.clickData={}; //存放用户已经选择了的数据
      $scope.itemOver={}; //存放用户已经做完的题目id
    });
    
    //开启或关闭box面板
    $scope.boxSlideToggle = function($event) {
      $scope.boxSlideToggleValue=!$scope.boxSlideToggleValue;
      jQuery('#boxcontent').slideToggle("fast");
    }

    // 监听li的点击事件 处理答案选择背景色 box数量 及 box题目id样式
    $scope.ulClick = function($event) {
      var arrAns = new Array(); //本次接收的问题id、答案id
      if($event.target.id) arrAns=$event.target.id.split('_+_');
      if(arrAns[0] && arrAns[1]){  //确定有题目id 和答案id 才执行
        if($scope.itemOver[arrAns[0]]!=undefined){ //以前这道题勾选过
          if($scope.clickData[arrAns[0]][arrAns[1]]!=undefined){ //题目勾选过 且 答案勾选过（表示取消）
            $scope.clickData[arrAns[0]][arrAns[1]] = undefined; //答案不再显示灰色背景
            if(JSON.stringify($scope.clickData[arrAns[0]])=='{}'){ //如果这道题没有答案了
              $scope['itemOverNum']--; //做完的题目总数-1
              $scope.itemOver[arrAns[0]]=undefined;
            }
          }else{  //答案没勾选过 （表示选中）
            // 如果是单选题就要取消其他选择
            if($scope.examlist[arrAns[0]]['type']==1) $scope.clickData[arrAns[0]]={};
            $scope.clickData[arrAns[0]][arrAns[1]] = true; //答案显示灰色背景
          }
        }else{ //这道题没有勾选过
          $scope.clickData[arrAns[0]]={};
          $scope.clickData[arrAns[0]][arrAns[1]] = true; //答案显示灰色背景
          $scope.itemOver[arrAns[0]]=true; //面板显示已做
          $scope['itemOverNum']++; //做完的题目总数+1
        }
      }
      // 如果面板开启，就关闭面板
      if($scope.boxSlideToggleValue){
        jQuery('#boxcontent').slideUp("fast");
        $scope.boxSlideToggleValue = false;
      }
    }

    //box li点击跳转到页面指定的位置
    $scope.boxClick = function($event) {
      var id=$event.target.id.slice(6);
      if(id){
        $location.hash('item_'+id);
        $anchorScroll();
      }
    }
}]);











// 查看解析控制器
app.controller('report', ['$scope', '$http','$location', '$anchorScroll','$sce',
  function($scope, $http, $location, $anchorScroll,$sce) {
    $http.get('./data/exam.json').success(function(data) {
      //一次循环处理所有需要的数据
      var newdata=[]; // 生成新的数组
      $scope.score=0; // 计算总分
      for (var i = 0; i < data.length; i++) {
        newdata[i]={};
        newdata[i]=data[i];
        newdata[i]['newId']=i; //题目顺序
        newdata[i]['explainhtml']=$sce.trustAsHtml(data[i]['explain']); //转换显示解析的html
        if(data[i]['useranswer'].toString()==data[i]['answer'].toString()){ //判断是否答对
          newdata[i]['rightError']=true;
          $scope.score+=parseInt(data[i]['score']);
        }
        //用户答案数组转换为对象保存 页面直接使用
        newdata[i]['answerObj']={}
        for (var x = 0; x < data[i]['useranswer'].length; x++) {
          var answerid = data[i]['useranswer'][x];
          newdata[i]['answerObj'][answerid]=true;
        };
      };
      $scope.examlist =newdata;
      $scope.examlist2=$scope.examlist; //用于过滤数据
      $scope.boxSlideToggleValue=false; //未开启面板
      $scope.botton_all=true; //按钮默认显示全部
    });
    
    //开启或关闭box面板
    $scope.boxSlideToggle = function($event) {
      $scope.boxSlideToggleValue=!$scope.boxSlideToggleValue;
      jQuery('#boxcontent').slideToggle("fast");
    }

    //选择 查看全部 查看正确 查看错误
    $scope.botton_click = function($event) {
      var id=$event.target.id;
      if(id=='botton_all' || id=='botton_right' || id=='botton_wrong'){
        $scope.botton_all=false;
        $scope.botton_right=false;
        $scope.botton_wrong=false;
        $scope[$event.target.id]=true;
        if(id!='botton_all'){ // 循环得到需要的列表
          var examlistNew=[];
          for (var i = 0; i < $scope.examlist2.length; i++) {
            if(id=='botton_right'){
              if($scope.examlist2[i]['rightError']) examlistNew.push($scope.examlist2[i]);
            }else{
              if(!$scope.examlist2[i]['rightError']) examlistNew.push($scope.examlist2[i]);
            }
          };
          $scope.examlist=examlistNew;
        }else{
          $scope.examlist=$scope.examlist2;
        }
      }
    }

    // 处理页面点击 
    $scope.ulClick = function($event) {
      // 如果面板开启，就关闭面板
      if($scope.boxSlideToggleValue){
        jQuery('#boxcontent').slideUp("fast");
        $scope.boxSlideToggleValue = false;
      }
    }

    //box li点击跳转到页面指定的位置
    $scope.boxClick = function($event) {
      var id=$event.target.id.slice(6);
      if(id){
        $location.hash('item_'+id);
        $anchorScroll();
      }
    }
}]);


// var appFilters = angular.module('appFilters', []);
// 显示html标签
// appFilters.filter('trusthtml',['$sce',  function($sce) {
//   return function(data) {
//       return $sce.trustAsHtml(data);
//   };
// }]);


app.config(function($routeProvider) {
  $routeProvider.when('/',              {templateUrl: './tpl/classList.html', reloadOnSearch: false});
  
  $routeProvider.when('/classlist',        {templateUrl: './tpl/classList.html', reloadOnSearch: false});
  $routeProvider.when('/classadd/:cid/',        {templateUrl: './tpl/classAdd.html', reloadOnSearch: false});
  $routeProvider.when('/classinfo/:cid/',        {templateUrl: './tpl/classInfo.html', reloadOnSearch: false});
  $routeProvider.when('/myclass',        {templateUrl: './tpl/myClass.html', reloadOnSearch: false}); 

  $routeProvider.when('/exam',        {templateUrl: './tpl/exam.html', reloadOnSearch: false}); //作业中
  $routeProvider.when('/report',        {templateUrl: './tpl/report.html', reloadOnSearch: false}); //查看答案
  $routeProvider.when('/paperlistnew',        {templateUrl: './tpl/paperlist.html', reloadOnSearch: false});
  $routeProvider.when('/paperhistory',        {templateUrl: './tpl/paperlist.html', reloadOnSearch: false});
  $routeProvider.when('/userinfo',        {templateUrl: './tpl/userinfo.html', reloadOnSearch: false});
  $routeProvider.when('/logout',        {templateUrl: './tpl/logout.html', reloadOnSearch: false});
});


app.controller('classlist', ['$scope', '$http',
  function($scope, $http) {
    $http.get('http://192.168.1.12:8088/classes/allList/1/1/5').success(function(data) {
      $scope.classlist = data['res'];
      console.log(data['res']);
    });
}]);

app.controller('classadd', ['$scope', '$routeParams','$location','$http', function($scope, $routeParams,$location,$http) {
    $http.get('http://192.168.1.12:8088/classes/classInfo/'+$routeParams.cid).success(function(data) {
      $scope.classinfo = data['res'];
      console.log(data);
    });

    //加入班级操作
    $scope.addclass = function() {
      $http.get('http://192.168.1.12:8088/classes/joinClass/1/'+$routeParams.cid+'/?passcode='+$scope.classinfo.inputcode).success(function(data) {
        if(data.status==1){ // 加入成功
          $location.path('/myclass');
        }else{ //没有加入成功
          $location.path('/');
        }
      });
    }
}]);

app.controller('classinfo', ['$scope', '$routeParams', '$location','$http', function($scope, $routeParams,$location,$http) {
    $http.get('http://192.168.1.14:8088/workbar/get_class/'+$routeParams.cid).success(function(data) {
        $scope.classinfo = data['data'];
        console.log(data);
      });
    
    $scope.logoutclass=function(){
      $http.get('http://192.168.1.12:8088/classes/exitClass/1/'+$routeParams.cid).success(function(data) {
        $location.path('/myclass');
      });
    }
}]);

app.controller('myclass', ['$scope', '$routeParams', '$http', function($scope, $routeParams,$http) {
  $http.get('http://192.168.1.12:8088/classes/joinList/1/1/5').success(function(data) {
        $scope.classlist = data['res'];
        console.log(data);
      });

}]);
