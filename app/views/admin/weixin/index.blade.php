<?php
require_once Config::get('app.ThirdClass')."/weixin/jssdk.php";
$jssdk = new JSSDK("wxf305ea0876e9955e", "a39cb813682c13d004633ac46c473bd7");
$signPackage = $jssdk->GetSignPackage();
?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <title>麦学习</title>
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimal-ui" />
      <meta name="apple-mobile-web-app-status-bar-style" content="yes" />
      <meta name="format-detection" content="telephone=no" />
      <script src="{{URL::to('/')}}/weixin/libs.min.js"></script>
      <link rel="stylesheet" href="{{URL::to('/')}}/weixin/css/app.mini.css" />
      <script>
         var Mwx = eval('('+'{{$_weixin}}'+')'); //赵聪写入的对象
         Mwx.url = "{{URL::to('/')}}/weixin";
         Mwx.apiUrl = "http://apis.mstudy.me:8088/";
         // 处理用户页面跳转的函数
         (function(){
            var addHash =window.location.href.lastIndexOf('#/'); 
            if(addHash!=-1) return ; //有后缀表示已经跳转过了
            switch(Number(Mwx.type)){
               case 2:
                  window.location.hash='#/classlist';
               break;
               case 3:
                  window.location.hash='#/classaction/'+Mwx.cid; //学生扫码查找班级
               break;
               case 10:
                  window.location.hash='#/classaction/'+Mwx.cid; //学生扫码查找班级 且已加入
               break;
               case 5:
                  window.location.hash='#/tmessage/6'; //扫码绑定 码失效
               break;
               case 6:
                  window.location.hash='#/tmessage/1'; //扫码绑定 码成功
               break;
               case 7:
                  window.location.hash='#/exam/'+Mwx.cid+'/'+Mwx.eid; //用户点击作业名进来
               break;
               case 8:
                  window.location.hash='#/classlist'; //（老师身份点击微信我的班级）
               break;
               case 9:
                  window.location.hash='#/tmessage/4'; //老师点击加入班级链接  （点击微信加入班级  及老师扫描学生二维码）
               break;
               case 11:
                  window.location.hash='#/tmessage/7'; //老师点击作业， 提示错误
               break;
            }
         })();

         angular.element(document).ready(function(){
            $.post(Mwx.apiUrl+'teacher/study/role',{'oid':Mwx.openid},function(data){
               if(data.status == 1){
                  Mwx.tid = 0;
                  Mwx.uid = 0;
                  if(data.data.role == 1){
                     Mwx.uid = data.data.uid;
                  }else if(data.data.role == 2){
                     Mwx.tid = data.data.tid;
                  }
               }
               angular.bootstrap(document, ['mxxapp']);
            });
         });

         wx.config({
            debug: false,
            appId: '<?php echo $signPackage["appId"];?>',
            timestamp: <?php echo $signPackage["timestamp"];?>,
            nonceStr: '<?php echo $signPackage["nonceStr"];?>',
            signature: '<?php echo $signPackage["signature"];?>',
            jsApiList: [
               'scanQRCode',
               'previewImage'
            ]
         });
         
         // 页面刷新函数
         Mwx.refresh = function(callback){
            var dom = document.getElementById("refresh"),
               starX,
               starY,
               spin; //是否开始刷新
            dom.addEventListener("touchstart", function(e){ // 触摸开始事件
               var touch = event.touches[0];
               starX = touch.clientX; //点击开始的坐标
               starY = touch.clientY;
               console.log(starY);
            }, false);
            dom.addEventListener("touchmove", function(e){ // 移动开始事件
               var touch = event.touches[0],
                 x = touch.clientX - starX,
                 y = touch.clientY - starY;
                 if (Math.abs(y) > Math.abs(x) && y > 0 && document.body.scrollTop == 0) {
                  event.preventDefault();
                  y = y*0.5; //不用下拉的太快
                  var cssStr = "transition:'';-moz-transition:'';-webkit-transition:''; -o-transition:'';";
                  cssStr += 'transform:translateY('+y+'px);-ms-transform:translateY('+y+'px);-moz-transform:translateY('+y+'px); -webkit-transform:translateY('+y+'px);-o-transform:translateY('+y+'px);';
                  dom.style.cssText = cssStr;
                  document.getElementById("pullDown").style.display = 'block';
                  if(y > 50 && !spin){ // 修改显示内容
                     document.getElementById("pullDown").innerHTML="<i class='fa fa-circle-o-notch fa-spin'></i>&nbsp;&nbsp;松开刷新页面...";
                     spin = true;
                  }
                }
            }, false);
            dom.addEventListener("touchend", function(e){
               dom.style.cssText = "transition:transform 400ms;-moz-transition:-moz-transform 400ms;-webkit-transition:-webkit-transform 400ms; -o-transition:-o-transform 400ms;";
               document.getElementById("pullDown").style.cssText = 'display:none;';
               document.getElementById("pullDown").innerHTML="<i class='fa fa-circle-o-notch fa-spin'></i>&nbsp;&nbsp;下拉刷新页面...";
               setTimeout(function() {
                  if(spin) callback();
               }, 400);
            }, false);
         }
      </script>
   </head>
   <body ng-controller="MainController">
      <div ng-show="loading" class="app-content-loading">
         <i class="fa fa-spinner fa-spin loading-spinner"></i>
      </div>
      <ng-view></ng-view>
      <div class='cnzz'><script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1254516488'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1254516488' type='text/javascript'%3E%3C/script%3E"));</script></div>
   </body>
</html>