@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class selected">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/classList" class="class-list">班级列表</a>
    <a href="{{URL::to('/')}}/classCreate" class="class-create-selected selected">创建班级</a>
</div>
<div class="detail">
    <div class="class-create-from">
        <form class="form-horizontal" role="form">
            <input type="text" class="form-control" name="className" placeholder="设定班级名(*必填)"  /> 
            <input type="text" class="form-control" name="classNum"  placeholder="设定班级最大人数(*必填)" />
            <div>
            <input type="text" class="form-control" style="width:520px; float:left;"  name="classPass"  placeholder="设定班级邀请码（限定6-8位数字）" />
            <button type="button" class="create-password" >随机生成</button>
            </div>
            <textarea class="form-control" name="classAnnounce " placeholder="此处填写班级任课老师通告(140字以内)"></textarea>
            <button type="button" class="btn btn-success creataSubmit">确定提交</button>
        </form>                
    </div>
</div> 
@stop

@section('footer')
{{ HTML::script('admin/js/mini/class.js') }}
<script type="text/javascript">
$("input").placeholder();
</script>


</body>
</html>
@stop