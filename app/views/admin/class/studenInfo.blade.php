@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class selected">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/classList" class="class-list-selected selected">班级列表</a>
    <a href="{{URL::to('/')}}/classCreate" class="class-create">创建班级</a>
</div>
<div class="detail">
    <div class="info">
        <p class="left">
            <span class="class-name">{{$class_res['cname']}}</span><br />
            <span>已加入人数:&nbsp;&nbsp; {{$class_res['hasnum']}}/{{$class_res['topnum']}}人</span>
            <span>密码: {{$class_res['passcode']}}</span>
        </p>
        <a href="{{URL::to('/')}}/classDetail/{{$class_res['cid']}}" class="btn btn-success goback">返回学生列表</a>
    </div>
    <div class="sub-sidebar left">
        <div class="qrCode">
            <img width="140" height="140" src="http://code.mstudy.me/qcode/{{$class_res['rqcode']}}.png" />
            <p>班级二维码</p>
        </div>
        <ul class="three-navigation">
            <li><a href="{{URL::to('/')}}/classDetail/{{$class_res['cid']}}" >本班学员</a></li>
            <li><a href="{{URL::to('/')}}/workList/{{$class_res['cid']}}">作业记录</a></li>
             <li><a href="{{URL::to('/')}}/modifyclass/{{$class_res['cid']}}">修改班级信息</a></li>    
        </ul>
    </div>
    <div class="student-info left" >
         <div class="student-info-header">
                <div class="userinfo">
                	@if (empty($userInfo['headimgurl']))
	                    <img src="{{URL::to('/')}}/admin/images/face.png"/>
                    @else
	                    <img src="{{$userInfo['headimgurl']}}" width="72" height="72"/>
                    @endif
                   	
                    @if (empty($userInfo['remark'])) 
                    	<h2>（备注名）</h2>
                    @else
                    	<h2>{{$userInfo['remark']}}</h2>
                    @endif
                    <p class="wxnum">微信昵称: {{$userInfo['nickname']}}</p>
                    <p class="total">正常交作业<span class="green">&nbsp;<?php if(isset($totalstat['total_submit'])) echo $totalstat['total_submit'];  ?>&nbsp;</span>次</p>
                    <p class="total">班级共发布作业<span class="green">&nbsp;<?php if(isset($totalstat['total_exam'])) echo $totalstat['total_exam'];  ?>&nbsp;</span>次</p>
                </div>
         </div>
         <div class="student-info-body">
                <div class="usedcount">
                    <h3>总答题数/道题</h3>
                    <div class="num"><?php if(isset($totalstat['total_answer'])) echo $totalstat['total_answer'];  ?></div>
                </div>
                <div class="usedcount rightcount">
                    <h3>答对/道题</h3>
                    <div class="num"><span class="green"><?php if(isset($totalstat['total_right'])) echo $totalstat['total_right'];  ?></span></div>
                </div>
				<div class="clear"></div>                
                <div class="stat" id="container">
                  	
                </div>
                <div class="paper-info stat-info">
                	<div>
                    	学生作业提交的时间:
                    	<select name="userexam">
                        	@if (!empty($exams))
                			@foreach ($exams as $k=>$v)
                            	<option  data-eid="{{$v['eid']}}" value="{{$v['ueid']}}"><?php echo date('Y-m-d H:i:s',$v['endtime'])  ?></option>	
                            @endforeach
                			@endif
                        </select>
                    </div>
                    <p class="title paper-title"><span class="lightgray">试 卷 名：-</span></p>
                    <p class="title paper-time"><span class="lightgray">规定取作业的时间：-</span></p>
                    <div class="exam-info">
                        <div><span class="num">-</span>道</div>
                        <div>得分<span class="num">-</span>分</div>
                        <div><span class="num"><span class="green">-</span></span>名次</div>
                        <div>答对<span class="num">-</span>道</div>
                        <div>平均分<span class="num">-</span>分</div>
                        <div>最高分<span class="num">-</span>分</div>
                        <ul class="paper-sort">
                        </ul>
                    </div>
                </div><!-- paper-info end -->
         </div>       
    </div>
</div>
@stop

@section('footer')
<input type="hidden" name="cid" value="{{$class_res['cid']}}" />
<input type="hidden" name="uid" value="{{$uid}}" />
{{ HTML::script('admin/highcharts/highcharts.js') }}
{{ HTML::script('admin/highcharts/exporting.js') }}
<script type="text/javascript">
var imgdata = {{$imgdata}};

$(function(){
		
	//生成统计图
	$('#container').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: '<span style="color:#009234">成绩得分</span>/<span style="color:#bdbdbd">班级平均分 %</span>',
			
        },
        xAxis: {
            categories: imgdata.time
        },
        yAxis: {
            title: {
                text: ''
            },
            labels: {
                formatter: function() {
                    return this.value;
                }
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        plotOptions: {
        },
        series: [{
            name: '成绩得分',
			color:'#009234',
            data: imgdata.user 
       }, {
            name: '班级平均分',
			color:'#bdbdbd',
            data: imgdata.class 
        }]
    });
});
</script>
{{ HTML::script('admin/js/mini/class.js') }}
</body>
</html>
@stop