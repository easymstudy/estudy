@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class selected">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/classList" class="class-list-selected selected">班级列表</a>
    <a href="{{URL::to('/')}}/classCreate" class="class-create">创建班级</a>
</div>
<div class="detail">
    <div class="info">
        <p class="left">
        <span class="class-name">{{$res['cname']}}</span><br />
        <span>已加入人数:&nbsp;&nbsp; {{$res['hasnum']}}/{{$res['topnum']}}人</span>
        <span>密码: {{$res['passcode']}}</span>
        </p>
        <a href="{{URL::to('/')}}/classList" class="btn btn-success goback">返回班级列表</a>
    </div>
    <div class="sub-sidebar left">
        <div class="qrCode">
            <img width="140" height="140" src="http://code.mstudy.me/qcode/{{$res['rqcode']}}.png" />
            <p>班级二维码</p>
        </div>
        <ul class="three-navigation">
            <li><a href="{{URL::to('/')}}/classDetail/{{$cid}}/{{$p}}" class="selected" >本班学员</a></li>
            <li><a href="{{URL::to('/')}}/workList/{{$cid}}">作业记录</a></li>
            <li><a href="{{URL::to('/')}}/modifyclass/{{$cid}}">修改班级信息</a></li>    
        </ul>
    </div>
    <div class="right" >
        <div class="student-list">
            <div class="student-list-header">
                <div class="input-group input-group-sm page">
                  <div class="page-left"><button class="left-detail-button" type="button">◀</button></div>
                  	<?php
                    	if (empty($total))
							$total = 1;
					
					?>
                    <div class="page-num"><input type="text"  value="{{$p}}/<?php echo $total;?>" class="pagination-info" disabled="disabled"/></div>
                  <div class="page-right"><button class="right-detail-button" type="button">▶</button></div>
                </div>
                <div class="search">
                  <div class="input-search"><input type="text" class="form-control" placeholder='搜索学员'></div>
                <button class="btn btn-success" type="button">搜索</button>
            </div>
            </div>
            <ul class="student-list-body">
            	@if (!empty($student))
                @foreach ($student as $k=>$v)
                <li>
                	@if (empty($v['headimgurl']))
                    	<img class="student_img_icon" src="{{URL::to('/')}}/admin/images/face.png" />
                    @else
                    	<img class="student_img_icon" src="{{$v['headimgurl']}}" width="36" height="36" />
                    @endif
                    <?php if ($v['remark'] != '') $v['remark'] = '('.$v['remark'].')';  ?>
                    <span>{{$v['nickname']}}<b><?php echo  $v['remark'];?></b></span>
                    <a class="btn remark js_msgSenderRemark" data-uid="{{$v['uid']}}" data-fakeid="108432">修改备注</a>
                    <a href="{{URL::to('/')}}/studentInfo/{{$cid}}/{{$v['uid']}}" class="show-score">查看统计成绩</a>
                </li>
                @endforeach
                @endif
            </ul>
            <!--
            <div class="student-list-foot">
                <div class="input-group input-group-sm page">
                </div>
            </div>
            -->
        </div>
    </div>
</div>
@stop

@section('footer')
<form>
<input type="hidden" name="cid" value="{{$cid}}" />
<input type="hidden" name="uid" value="" />
</form>

<!--遮罩弹窗-->
<div class="dialog_wrp simple label_block none">
	<div class="dialog">
		<div class="dialog_hd">
			<h3>添加备注</h3>
			<a href="javascript:;" onclick="return false" class="icon16_opr closed pop_closed">关闭</a>
		</div>
		<div class="dialog_bd"><div class="simple_dialog_content">
    <form id="popupForm_" method="post" class="form" onsubmit="return false;" novalidate="novalidate">
         <div class="frm_control_group">
            <span class="frm_input_box">
                <input type="text" class="frm_input" name="popInput" value="">
                <input style="display:none">
            </span>
            <p class="frm_msg fail" style="display: block;"><span for="popInput" class="frm_msg_content" style="display: none;">备注不能超过15个字</span></p>
        </div>       
        
        <div class="js_verifycode"></div>
    </form>
</div></div>
		
		<div class="dialog_ft">
            <span class="btn btn_primary btn_input js_btn_p"><button type="button" class="js_btn btn_remark" data-index="0">确认</button></span>
            <span class="btn btn_default btn_input js_btn_p"><button type="button" class="js_btn btn_close" data-index="1">取消</button></span>
		</div>
	</div>
</div>

<div class="mask none"><iframe frameborder="0" style="filter:progid:DXImageTransform.Microsoft.Alpha(opacity:0);position:absolute;top:0px;left:0px;width:100%;height:100%;" src="about:blank"></iframe></div>


<script>
$(function(){
	$(".js_msgSenderRemark").click(function(){
		$(".dialog_wrp").show();
		$(".mask").show();
		
		var uid = $(this).attr('data-uid');
		$("input[name=uid]").val(uid);
		
		var remark = $(this).prev().find('b').text(),
			remark = remark.substr(1,remark.length-2);

		$(".frm_input").val(remark);
	});
	
	$(".pop_closed,.btn_close").click(function(){
		$(".dialog_wrp").hide();
		$(".mask").hide();
		
		$("input[name=uid]").val('');
		$(".frm_input").val('');
	});
});

</script>
{{ HTML::script('admin/js/mini/class.js') }}
</body>
</html>
@stop