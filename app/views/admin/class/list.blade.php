@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class selected">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
	<a href="{{URL::to('/')}}/classList" class="class-list-selected selected">班级列表</a>
    <a href="{{URL::to('/')}}/classCreate" class="class-create">创建班级</a>
</div>
<div class="detail">
    <div class="class-list">
        <div class="class-list-handle">
            <img src="{{URL::to('/')}}/admin/images/check.png" class="allcheck"/>
            <!--<img src="admin/images/checking.png" class="allcheck"/>-->
            <div class="allcheck">全选</div>
            <div class="input-group input-group-sm page">
              <div class="page-left"><button type="button" class="pre-class-list">◀</button></div>
                <div class="page-num"><input type="text" value="{{$p}}/{{$total}}" class="pagination-info" disabled="disabled"/></div>
              <div class="page-right"><button type="button" class="next-class-list">▶</button></div>
            </div>
            <div class="search">
                  <div class="input-search"><input type="text" class="form-control" placeholder='班级搜索'></div>
                <button class="btn btn-success search-class" type="button">搜索</button>
            </div>
            <div id="handlebox">
                <button type="button" id="handle">操作</button>
                <ul class="handle-menu none">
                	<li class="delete-class">删除班级</li>
                    <li class="modify-class">更改班级信息</li>
                    <li style="color:#999;cursor:default;">班级链接分享</li>
                </ul>
            </div>
        </div>
        <table class="class-list-table">
            <thead>
                <tr class="class-list-header">
                    <td>序号</td>
                    <td>班级名</td>
                    <td>班级编号</td>
                    <td>密码</td>
                    <td>人数</td>
                    <td>班级动态</td>
                </tr>
            </thead>
            <tbody>
                @if (!empty($res)) 
                @foreach ($res as $key=>$val)
                <tr data-cid="{{$val['cid']}}">
                    <td class="class-list-number">
                        <img src="{{URL::to('/')}}/admin/images/check.png" data-cid="{{$val['cid']}}"/>
                        <span>{{$key+1}}</span>
                    </td>
                    <td>{{$val['cname']}}</td> 
                    <td>{{$val['cid']}}</td>
                    <td>{{$val['passcode']}}</td>
                    <td><span class="lightblue">
                        @if (empty($val['hasnum']))
                            0
                        @else
                            {{$val['hasnum']}}
                        @endif
                    </span>/{{$val['topnum']}}</td>
                    <td><a href="{{URL::to('/')}}/classDetail/{{$val['cid']}}"><span class="lookup">查看</span></a></td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>                    
    </div>
</div> 
@stop


@section('footer')
<form>
<input type="hidden" value="{{$total}}" name="total"  />
</form>
<script>

$(function () {
	var handlebtn = $('#handle');
	var menu = handlebtn.next();
	$('#handlebox').hover(function () {
			var offset=handlebtn.offset();
			menu.css( {'top':offset.top+handlebtn.height()+1, 'left':offset.left} );
			menu.show();
	},function () {
			menu.hide();
	});	
});	
</script>
{{ HTML::script('admin/js/mini/class.js') }}
</body>
</html>
@stop