@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class selected">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/classList" class="class-list">班级列表</a>
    <a href="{{URL::to('/')}}/classCreate" class="class-create-selected selected">创建班级</a>
</div>
<div class="detail">
    <div class="class-create-info">
        <p class="info">{{$res['cname']}} 创建成功！</p>
        <span class="class-name">班级名称：{{$res['cname']}}</span>
        <a href="{{URL::to('/')}}/classCreate" class="btn btn-success goback agan">再新增一个班级</a>
    </div>
    <div class="class-detail-info class-qrcode-detail">
        <div class="qrCode left">
        	<div class="class-qrcode ">
                <img width="140" height="140" class="class-qrcode-img" src="http://code.mstudy.me/qcode/{{$res['rqcode']}}.png" />
                <p>班级二维码</p>
            </div>
            <div class="teacher-qrcode">
				<a class="make-qrcode" href="javascript:;">生成老师二维码</a>
                <div class="teacher-qrcode-detail">
                </div>
				<div class="clear"></div>
                <!--
                <div class="qrcode-detail">
					<img width="140" height="140" src="http://code.mstudy.me/qcode/{{$res['rqcode']}}.png" />
                    <p>openid用户名 绑定成功</p>
                    <a>解除绑定</a>		                
                </div>
                -->                                    
            </div>
        </div>
        <div class="detail-info right">
            <p>人数:&nbsp;&nbsp;{{$res['topnum']}}人</p>
            <p>密码:&nbsp;&nbsp;{{$res['passcode']}}</p>
            <p>班级公告:&nbsp;&nbsp;{{$res['introduce']}}</p>
 			<p>（扫一扫老师二维码，绑定为本班老师，即可在微信查看学员成绩和作业信息）</p>           
            <img src="{{URL::to('/')}}/admin/images/class-show.jpg"/>
        </div>
    </div>	
</div>
@stop

@section('footer')
<input type="hidden" name="cid" value="{{$res['cid']}}" />
{{ HTML::script('admin/js/mini/class.js') }}
</body>
</html>
@stop