@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class selected">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/classList" class="class-list-selected selected">班级列表</a>
    <a href="{{URL::to('/')}}/classCreate" class="class-create">创建班级</a>
</div>
<div class="detail">
    <div class="info">
        <p class="left">
            <span class="class-name">{{$class_res['cname']}}</span><br />
            <span>已加入人数:&nbsp;&nbsp; {{$class_res['hasnum']}}/{{$class_res['topnum']}}人</span>
            <span>密码: {{$class_res['passcode']}}</span>
        </p>
        <a href="{{URL::to('/')}}/workList/{{$class_res['cid']}}" class="btn btn-success goback">返回作业列表</a>
    </div>
    <div class="sub-sidebar left">
        <div class="qrCode">
            <img width="140" height="140" src="http://code.mstudy.me/qcode/{{$class_res['rqcode']}}.png" />
            <p>班级二维码</p>
        </div>
        <ul class="three-navigation">
            <li><a href="{{URL::to('/')}}/classDetail/{{$class_res['cid']}}" >本班学员</a></li>
            <li><a href="{{URL::to('/')}}/workList/{{$class_res['cid']}}"  class="selected">作业记录</a></li>
            <li><a href="{{URL::to('/')}}/modifyclass/{{$class_res['cid']}}">修改班级信息</a></li>    
        </ul>
    </div>
    <div class="student-info left" >
         <div class="student-info-header">
                <div class="userinfo-test">
                    <h2 class="fn">{{$examstat['exam_name']}}</h2>
                    <p class="total">已提交人数<span class="green">{{$examstat['submit_total']}}</span>人</p>
                    <!--<p class="total">参与人数<span class="green">9</span>人</p>-->
                    <div class="clear"></div>
                    <p class="preset-time">规定取作业时间: <?php if(!empty($examstat['startime']) && !empty($examstat['endtime'])) echo date('Y/m/d H:i',$examstat['startime']).'-'.date('Y/m/d H:i',$examstat['endtime']); ?></p>
                    <p class="preset-time">作业时长: <?php echo Helpers\Helper::formatduration($examstat['duration']); ?></p>
                 </div>
         </div>
         <div class="student-info-body">
                <div class="usedcount">
                    <h3>平均分/分</h3>
                    <div class="num">{{$examstat['avg_score']}}</div>
                </div>
                <div class="usedcount rightcount">
                    <h3>最高分/分</h3>
                    <div class="num"><span class="green">{{$examstat['top_score']}}</span></div>
                </div>
				<div class="clear"></div>                
                <!--统计图表-->
                <div class="stat" id="container">
                </div>
         </div>  
         <div class="student-info-rank">
			<select name="rank" class="select-option">
              <option value="1">分数</option>
              <option value="3">正确题数</option>
              <option value="2">答题时间</option>
            </select>
            <span>排名</span>
            <div class="rank-type">
            </div>
         </div>  
    </div>
</div>
@stop

@section('footer')
<input type="hidden" name="cid" value="{{$class_res['cid']}}" />
<input type="hidden" name="eid" value="{{$eid}}" />

{{ HTML::script('admin/highcharts/highcharts.js') }}
{{ HTML::script('admin/highcharts/exporting.js') }}
<script type="text/javascript">
var imgdata = {{$item}};

$(function(){
		
	//生成统计图
	$('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: '题目/对错人数',
			
        },
        xAxis: {				
            categories: imgdata.key
        },
        yAxis: {
            title: {
                text: '人'
            },
			/*tickPositions: [0, 5, 10, {{$class_res['topnum']}}],*/ 			
            labels: {
                formatter: function() {
                    return this.value;
                }
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true
        },
        plotOptions: {
        },
        series: [{
            name: '做错',
			color:'#e05833',
            data: imgdata.error 
       }, {
            name: '做对',
			color:'#009239',
            data: imgdata.right 
        }]
    });
});
</script>
{{ HTML::script('admin/js/mini/class.js') }}
</body>
</html>
@stop