@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class selected">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/classList" class="class-list-selected selected">班级列表</a>
    <a href="{{URL::to('/')}}/classCreate" class="class-create">创建班级</a>
</div>
<div class="detail">
    <div class="info">
        <p class="left">
        <span class="class-name">{{$res['cname']}}</span><br />
        <span>已加入人数:&nbsp;&nbsp; {{$res['hasnum']}}/{{$res['topnum']}}人</span>
        <span>密码: {{$res['passcode']}}</span>
        </p>
        <a href="{{URL::to('/')}}/classList" class="btn btn-success goback">返回班级列表</a>
    </div>
    <div class="sub-sidebar left">
        <div class="qrCode">
            <img width="140" height="140" src="http://code.mstudy.me/qcode/{{$res['rqcode']}}.png"/>
            <p>班级二维码</p>
        </div>
        <ul class="three-navigation">
            <li><a href="{{URL::to('/')}}/classDetail/{{$res['cid']}}" >本班学员</a></li>
            <li><a href="{{URL::to('/')}}/workList/{{$res['cid']}}">作业记录</a></li>
            <li><a href="{{URL::to('/')}}/modifyclass/{{$res['cid']}}" class="selected" >修改班级信息</a></li>    
        </ul>
    </div>
    <div class="update-form left" >
        <div class="class-update-from">
            <form class="form-horizontal" role="form">
                <input type="hidden" name="cid" value="{{$res['cid']}}" />
                <p>修改班级名</p>
              <input type="text" name="className" class="form-control" value="{{$res['cname']}}" placeholder="设定班级名"  title="设定班级名"/>
                <p>修改班级人数</p>
              <input type="text" name="classNum" class="form-control" value="{{$res['topnum']}}"  placeholder="设定班级人数" title="设定班级人数"/>
                <p>修改班级密码</p>
                <input type="password" name="classPass" class="form-control" value="{{$res['passcode']}}"  placeholder="(6-8位数字)" title="设置进入班级的密码"/>
                <p>重新绑定老师 (微信扫一扫二维码成为本班老师，即可查看成绩和作业信息)</p>
                <div class="teacher-qrcode">
                    <a class="make-qrcode" href="javascript:;">生成老师二维码</a>
                    <div class="teacher-qrcode-detail">
                	</div>
					<div class="clear"></div>
                    <!--
                    <div class="qrcode-detail">
                        <img width="140" height="140" src="http://code.mstudy.me/qcode/{{$res['rqcode']}}.png" />
                        <p>openid用户名 绑定成功</p>
                        <a>解除绑定</a>		                
                    </div>
                    -->                                    
                </div>
                <p>留言内容</p>
              <textarea class="form-control" placeholder="班级公告(140字以内 )"title="班级公告">{{$res['introduce']}}</textarea>
                <button type="button" class="btn btn-success class-modify">确认修改</button>
            </form>                
        </div>
    </div>
</div> 
@stop

@section('footer')
<form>
<input type="hidden" name="cid" value="{{$res['cid']}}" />
</form>
{{ HTML::script('admin/js/mini/class.js') }}
</body>
</html>
@stop