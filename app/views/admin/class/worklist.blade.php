@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class selected">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/classList" class="class-list-selected selected">班级列表</a>
    <a href="{{URL::to('/')}}/classCreate" class="class-create">创建班级</a>
</div>
<div class="detail">
    <div class="info">
        <p class="left">
        <span class="class-name">{{$class_res['cname']}}</span><br />
        <span>已加入人数:&nbsp;&nbsp; {{$class_res['hasnum']}}/{{$class_res['topnum']}}人</span>
        <span>密码: {{$class_res['passcode']}}</span>
        </p>
        <a href="{{URL::to('/')}}/classList" class="btn btn-success goback">返回班级列表</a>
    </div>
    <div class="sub-sidebar left">
        <div class="qrCode">
            <img width="140" height="140" src="http://code.mstudy.me/qcode/{{$class_res['rqcode']}}.png" />
            <p>班级二维码</p>
        </div>
        <ul class="three-navigation">
            <li><a href="{{URL::to('/')}}/classDetail/{{$class_res['cid']}}">本班学员</a></li>
            <li><a href="{{URL::to('/')}}/workList/{{$class_res['cid']}}" class="selected">作业记录</a></li>
            <li><a href="{{URL::to('/')}}/modifyclass/{{$class_res['cid']}}">修改班级信息</a></li>    
        </ul>
    </div>
    <div class="right" >
        <div class="student-list">
            <div class="student-list-header">
                <div class="input-group input-group-sm page">
                  <div class="page-left"><button class="left-worklist-button" type="button">◀</button></div>
                    <div class="page-num"><input type="text"  value="{{$p}}/{{$total}}" class="pagination-info" disabled="disabled"/></div>
                  <div class="page-right"><button class="right-worklist-button" type="button">▶</button></div>
                </div>
                <div class="search">
                  <div class="input-search"><input type="text" class="form-control" placeholder='搜索作业'></div>
                <button class="btn btn-success" type="button">搜索</button>
            </div>
            </div>
            <ul class="student-list-body">
            	@if (!empty($examlist))
                @foreach ($examlist as $k=>$v)
                <li>
                <div class="student-list-body-text">
                    <ul>
                        <li>{{$v['title']}}</li>
                        <li><p>规定取作业时间</p> <?php echo date('Y/m/d H:i',$v['startime']).'-'.date('Y/m/d H:i',$v['endtime']);  ?></li>
                        <li><p>作业时长</p> <?php echo Helpers\Helper::formatduration($v['duration']); ?></li>
                        <li><p>已提交</p> {{$v['join_num']}}</li>
                        <a href="{{URL::to('/')}}/workstat/{{$class_res['cid']}}/{{$v['eid']}}" class="show-score">查看统计成绩</a>
                    </ul>
                </div>
                </li>
                @endforeach
                @endif
            </ul>
            <!--
            <div class="student-list-foot">
                <div class="input-group input-group-sm page">
                </div>
            </div>
            -->
        </div>
    </div>
</div>
@stop

@section('footer')
<form>
<input type="hidden" name="cid" value="{{$class_res['cid']}}" />
</form>
{{ HTML::script('admin/js/mini/class.js') }}
</body>
</html>
@stop