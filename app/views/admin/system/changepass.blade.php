@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system selected">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
	<a href="{{URL::to('/')}}/changePass" class="personal-center-selected selected">个人中心</a>
	<a href="{{URL::to('/')}}/bindWx" class="bind-wx">绑定微信</a>
</div>
<div class="detail">
    <div class="class-create-from">
        <form class="form-horizontal" role="form">
            <input type="hidden" name="user" value="{{$res['user']}}"  />
            <input type="text" class="form-control"  readonly="readonly" value="用户名:{{$res['user']}}"  />
            <input type="password" class="form-control" name="pass"  placeholder="输入新密码" />
            <input type="password" class="form-control" name="repass"  placeholder="确定新密码" />
            <button type="button" class="btn btn-success changepass">确定修改</button>
        </form>      	
    </div>
</div> 
@stop

@section('footer')
{{ HTML::script('admin/js/mini/system.js') }}
</body>
</html>
@stop

