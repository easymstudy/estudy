@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system selected">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/changePass" class="personal-center">个人中心</a>
    <a href="{{URL::to('/')}}/bindWx" class="bind-wx-selected selected">绑定微信</a>
</div>
<div class="detail">
    <p class="wx-status">微信公众帐号状态: 
        @if (isset($res['is_bind']) and $res['is_bind'] == 1 ) 
        <span class="wx-status-bind">{{$res['nickname']}}&nbsp;&nbsp; 绑定正常</span>
        @else
        <span class="wx-status">公众帐号未绑定(请输入微信公众帐号)</span>
        @endif
    </p>
    <div class="class-create-from">
        <form class="form-horizontal wx" role="form">
 			<?php
				$readonly = $disabled = '' ;
				if (isset($res['is_bind']) && $res['is_bind']) {
					$readonly = 'readonly="readonly"';
					$disabled = 'disabled="disabled"';						
				}
			?>
            <p>微信公众账号名称</p>
            <input type="text" class="form-control" name="wx_account" <?php echo $readonly; ?> value="wheatinlife"  placeholder="输入微信公众帐号"  />
          	<p>公众账号登陆密码</p>
            <input type="password" class="form-control" name="wx_pass"  <?php echo $readonly; ?> value="mkq87298233"  placeholder="输入密码" />
            <p>微信AppSecret(应用密匙)</p>
            <input type="text" class="form-control" name="wx_secret"  <?php echo $readonly; ?> value="0393758d45a6bd1df24a64abc99c1310"  placeholder="AppSecret(应用密钥)" />
            <div class="need-code none">
            
            <input type="text" class="form-control" name="wx_code" placeholder="请输入验证码" value="" />
            <img src="" id="verifyImg" /><a href="javascript:;" class="changeCode">换一张</a>
            </div>   
            <button type="button" class="btn btn-success bindWx" <?php echo $disabled; ?>>确认</button>
        </form>      	
    </div>
</div> 
@stop

@section('footer')
{{ HTML::script('admin/js/mini/system.js') }}
</body>
</html>
@stop

