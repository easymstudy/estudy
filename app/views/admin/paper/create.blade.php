@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper selected">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
	<a href="{{URL::to('/')}}/paperlist" class="paper-list">作业列表</a>
    <a href="{{URL::to('/')}}/createPaper" class="paper-create-selected selected">录入作业</a>
</div>
<div class="detail">
    <div class="create-paper">
        <div class="create-paper-step">
            <img class="create-step" src="{{URL::to('/')}}/admin/images/step1.png" />
                    <div class="create-paper-form">
            <form class="form-horizontal" role="form">
                <input type="text" value="<?php if(isset($res['title'])) echo $res['title']; ?>" class="form-control" name="paper-name" placeholder="输入作业名" />
            </form>       
        </div>	
            <a class="btn btn-success nextstep create-paper-button" href="javascript:;">下一步</a>
            <div class="clear"></div>    
        </div>
    </div>
</div> 
@stop

@section('footer')
{{ HTML::script('admin/js/mini/paper.js') }}
</body>
</html>
@stop