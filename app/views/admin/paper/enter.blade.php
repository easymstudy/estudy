@extends('admin/base')

@section('cssjs')
{{ HTML::script('admin/jquery-ui/jquery-ui.min.js') }}
@stop

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper selected">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
	<a href="{{URL::to('/')}}/paperlist" class="paper-list">作业列表</a>
    <a href="{{URL::to('/')}}/createPaper" class="paper-create-selected selected">录入作业</a>
</div>

<div class="mtk-all-operate">
	
    <!--左边导入区域1-->
    <div>
        <div class="mtk-import fl ">
            <!--上传按钮-->
       		<a id="pickfiles1" class="import-word import-word1" href="javascript:;">导入本地作业文档</a>
        </div>

    <!--左边导入区域2-->
        <div class="mtk-import fl none">
            <!--上传按钮-->
            <a id="pickfiles2" class="import-word" href="javascript:;"style="float:left;">导入本地作业文档</a>
            <div class="import-a">
                <a class="mtk-paper-back fl import-a1" href="javascript:;">返回上一步</a>
                <a class="mtk-paper-model fr" href="javascript:;">题目排序</a>
            </div>
            <div id="self-editor"></div>    
        </div>
    </div>
	
    <!--右边编辑区域-->
    <div class="mtk-bd fr">
        <img class="create-step create-step-editor" src="{{URL::to('/')}}/admin/images/step2.png">
        <div class="mtk-paper-fun clearfix">
            <div class="fl mtk-paper-name"><i>卷名：</i>
               	{{$paperInfo['title']}}
            </div>

            <div class="fr mtk-paper-btn clearfix" id="mtk-paper-btn-li">
                <ul>
                    <li class="mtk-paper-back fl">返回上一步</li>
                        <li class="mtk-paper-model fr" href="javascript:;">排序模式</li>
                    <!--<li class="mtk-paper-preview fr">保存并预览作业</li>-->
                </ul>
            </div>

            <div class="clearfix"></div>
           
        </div>
    
        <div class="mtk-item">
                            
            <dl class="mtk-item-dl">
                <!--默认1道普通题-->
                <dt data-type="0">
                    <!-- 收起状态 -->
                    <div class="mtk-item-slide mtk-item-slide-noa">
                        <div class="fl mtk-item-slide-">
                            <span class="mtk-item-slide-tit">1.</span>
                            <span class="mtk-item-slide-type">普通题</span>
                        </div>
                        <div class="mtk-item-txt fl">
                            <div class="clearfix">
                            <div class="fl mtk-item-slide-txt">
                                
                            </div>
                            <div class="fl">
                                ...
                            </div>
                            </div>
                        </div>
                        
                        <span class="mtk-del-item fr" title="删剪此题">
                        </span>
                        
                        <span class="mtk-show-item a" title="点击展开收起">
                        </span>
                    </div>
                    
                    <div class="mtk-item-w940">
                        
                        <div class="mtk-item-sel-switch">
    
                            <div class="mtk-sel-switch-div">
                                    <!--普通题-->
                                    <div class="mtk-item-border mtk-common-item">
                                        <!--提干-->
                                        <div class="mtk-problem clearfix">
                                            <div class="mtk-problem-ico fl" title="填写问题">
                                                <img src="{{URL::to('/')}}/admin/images/mtk-01.png"/>
                                            </div>
                                            <div data-attr="title" contenteditable="true" class="edit-div mtk-item-tm fl" dir="ltr"></div>
                                            
                                            <div class="mtk-item-score mtk-item-score-pt fl">
                                                <input type="text" maxlength="3" data-attr="score" value="0"  placeholder="此题分数" style="width:50px;border-style:none;" />
                                            </div>
                                            
                                        </div>
                                        <!--选项-->
                                        <div class="mtk-choice">
                                            <ul class="sortable">
                                                <li class="clearfix">
                                                    <div class="fl mtk-c1">
                                                        <img src="{{URL::to('/')}}/admin/images/mtk-99.png" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">A</span><span>.</span>
                                                    </div>
                                                    <div id="editor1" data-attr="choice" contenteditable="true" class="edit-div fl mtk-c2"  dir="ltr"></div>
                                                    <div class="fr mtk-c3">
                                                        <div class="mtk-sel-del" del-attr="choice" ><img src="{{URL::to('/')}}/admin/images/mtk-04.png"/></div>
                                                    </div>
                                                </li>
                                                <li class="clearfix">
                                                    <div class="fl mtk-c1">
                                                        <img src="{{URL::to('/')}}/admin/images/mtk-99.png" title="点击设置为正确答案"/>&nbsp;<span class="mtk-sel-letter">B</span><span>.</span>
                                                    </div>
                                                    <div id="editor2" data-attr="choice" contenteditable="true" class="edit-div fl mtk-c2"  dir="ltr"></div>
                                                    <div class="fr mtk-c3">
                                                        <div class="mtk-sel-del" del-attr="choice" ><img src="{{URL::to('/')}}/admin/images/mtk-04.png"/></div>
                                                    </div>
                                                </li>
                                                <li class="clearfix">
                                                    <div class="fl mtk-c1">
                                                        <img src="{{URL::to('/')}}/admin/images/mtk-99.png" title="点击设置为正确答案"/>&nbsp;<span class="mtk-sel-letter">C</span><span>.</span>
                                                    </div>
                                                    <div id="editor3" data-attr="choice" contenteditable="true" class="edit-div fl mtk-c2"  dir="ltr"></div>
                                                    <div class="fr mtk-c3">
                                                        <div class="mtk-sel-del" del-attr="choice" ><img src="{{URL::to('/')}}/admin/images/mtk-04.png"/></div>
                                                    </div>
                                                </li>
                                                <li class="clearfix">
                                                    <div class="fl mtk-c1">
                                                        <img src="{{URL::to('/')}}/admin/images/mtk-99.png" title="点击设置为正确答案"/>&nbsp;<span class="mtk-sel-letter">D</span><span>.</span>
                                                    </div>
                                                    <div id="editor4" data-attr="choice" contenteditable="true" class="edit-div fl mtk-c2"  dir="ltr"></div>
                                                    <div class="fr mtk-c3">
                                                        <div class="mtk-sel-del" del-attr="choice" ><img src="{{URL::to('/')}}/admin/images/mtk-04.png"/></div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <!-- 添加选项 -->
                                            <span class="mtk-add-item clearfix" title="新增一条选项">
                                                <div class="mtk-add-item-txt1 fl">+</div>
                                                <div class="mtk-add-item-txt2 fl">新增一条选项</div>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <!--单选题解析-->
                                    <div class="mtk-item-analysis">
                                        <div class="mtk-item-analysis-tit">本题解析</div>
                                        <div class="mtk-item-analysis-div">
                                            
                                            <div class="mtk-item-analysis-ul">
                                                <ul>
                                                    <li class="clearfix">
                                                        <div class="mtk-item-analysis-li1 fl">
                                                            <input type="text" data-attr="attr" maxlength="10" placeholder="相关备注"/>
                                                        </div>
                                                        <div class="mtk-item-analysis-li2 fl ">
                                                            /
                                                        </div>
                                                        <div class="mtk-item-analysis-li3 edit-div fl" data-attr="attr" contenteditable="true" ></div>
                                                        <div class="mtk-item-analysis-li4 fl">
                                                            <div class="mtk-sel-del" del-attr="attr"><img src="{{URL::to('/')}}/admin/images/mtk-04.png"/></div>
                                                        </div>
                                                    </li>
                                                </ul>
                                                
                                                <!-- 添加解析 -->
                                                <span class="mtk-add-analysis clearfix" title="新增一条选项">
                                                    <div class="mtk-add-analysis-txt1 fl">+</div>
                                                    <div class="mtk-add-analysis-txt2 fl">新增一条解析</div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <!--普通题 end-->
                        </div>
                    </div>
                </dt>
            </dl>
            <!--新增下一题 普通题 复合题-->
            <div class="mtk-add-new-common fl">
                新增普通题	
            </div>
            <div class="mtk-add-new-composite fl">
                新增复合题	
            </div>
            <div class="mtk-paper-preview fl">
            	保存（预览）
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>

</div>
@stop

@section('footer')
<div class="actGotop"><a href="javascript:;"></a></div>
<form>
<input type="hidden" id="paperId" value="{{$pid}}" />
<input type="hidden" id="teacher" value="<?php echo Session::get('tid');?>" />
<input type="hidden" id="is_editor" value="1" />
</form>

{{ HTML::script('admin/ueditor/ueditor.config.js') }}
{{ HTML::script('admin/ueditor/ueditor.all.min.js') }}
{{ HTML::script('admin/ueditor/lang/zh-cn/zh-cn.js') }}
<script type="text/javascript">
var uploadConfig = {
		flash_swf_url : "{{URL::to('/')}}/admin/plupload/js/Moxie.swf",
		silverlight_xap_url : "{{URL::to('/')}}/admin/plupload/js/Moxie.xap",
		url : 'http://www.easymbook.com/upload/word',
		getHtml : 'http://www.easymbook.com/word/convert?h='
	};

	//注册按钮
	UE.registerUI('button', function(editor, uiName) {

		//创建一个button
		var btn = new UE.ui.Button({
			//按钮的名字
			name: 'caneditor',
			//提示
			title: '编辑与不可编辑模式',
			//添加额外样式，指定icon图标，这里默认使用一个重复的icon
			cssRules: 'background-position: -700px -40px;',
			//点击时执行的命令
			onclick: function() {
				
				var flag = parseInt($("#is_editor").val());
				if (flag) {
					UE.getEditor('self-editor').setDisabled('fullscreen');
					$("#is_editor").val(0);
				} else {
					UE.getEditor('self-editor').setEnabled();
					$("#is_editor").val(1);
				}	
				
			}
		});
		//因为你是添加button,所以需要返回这个button
		return btn;
	});	
	
	window.ue = UE.getEditor('self-editor',{  
                //这里可以选择自己需要的工具按钮名称,此处仅选择如下五个  
                toolbars:[['FullScreen', 'Source', 'Undo', 'Redo','bold','test','forecolor']],   
                //关闭elementPath  
                elementPathEnabled:false,  
				enableAutoSave : false,
				wordCount:false,
				
            });
			
	setTimeout(function(){
		$("#edui11_body").trigger('click');
	},800)
</script>
{{ HTML::script('admin/plupload/js/plupload.full.min.js') }}
{{ HTML::script('admin/js/socket.io.js') }}
{{ HTML::script('admin/js/mini/mtk_paper.js') }}
{{ HTML::script('admin/js/mini/paper.js') }}
</body>
</html>
@stop