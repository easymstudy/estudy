@extends('admin/base')

@section('cssjs')
{{ HTML::script('admin/jquery-ui/jquery-ui.min.js') }}
@stop

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper selected">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
	<a href="{{URL::to('/')}}/paperlist" class="paper-list">作业列表</a>
    <a href="{{URL::to('/')}}/createPaper" class="paper-create-selected selected">录入作业</a>
</div>

<div class="mtk-all-operate">

    <!--左边导入区域2-->
    <div>
        <div class="mtk-import fl">
            <!--上传按钮-->
            <div class="import-a">
                <a id="pickfiles2"  class="import-word" href="javascript:;">导入本地作业文档</a>
                <a class="mtk-paper-back fl" href="javascript:;">返回上一步</a>
                <a class="mtk-paper-model fr" href="javascript:;">排序模式</a>	
            </div>
            <div id="self-editor"></div>    
        </div>
    </div>
    
    <!--右边编辑区域-->
    <div class="mtk-bd fr">
        <img class="create-step create-step-editor" src="{{URL::to('/')}}/admin/images/step2.png">
        <div class="mtk-paper-fun clearfix">
            <div class="fl mtk-paper-name"><i>卷名：</i>
                {{$paperInfo['title']}}
            </div>
            <div class="clearfix"></div>
           <!-- <div class="self-editor">
                <ul>
                    <li><a href="javascript:;" class="upload-img">图像</a></li>
                    <li><a href="javascript:;">加黑</a></li>
                    <li><a href="javascript:;">粗体</a></li>
                    <li><a href="javascript:;">下划线</a></li>	            
                </ul>
            </div> -->       
        </div>
    
        <div class="mtk-item">
                    
            <dl class="mtk-item-dl">
                @if (!empty($data))
                    <?php $arrChioces = array('A','B','C','D','E','F','G','H','I','J'); ?>
                    @foreach ($data as $key=>$value)
                        @if ($value['type'] == 0)
                        <!--如果为普通题-->
                        <dt data-type="0" data-id="{{$value['mid']}}">
                            <!-- 收起状态 -->
                            <div class="mtk-item-slide mtk-item-slide-noa">
                                <div class="fl mtk-item-slide-">
                                    <span class="mtk-item-slide-tit">{{$key+1}}.</span>
                                    <span class="mtk-item-slide-type">普通题</span>
                                </div>
                                <div class="mtk-item-txt fl">
                                    <div class="clearfix">
                                    <div class="fl mtk-item-slide-txt">
                                        
                                    </div>
                                    <div class="fl">
                                        ...
                                    </div>
                                    </div>
                                </div>
                                
                                <span class="mtk-del-item fr" title="删剪此题">
                                </span>
                                
                                <span class="mtk-show-item a" title="点击展开收起">
                                </span>
                            </div>
                            
                            <div class="mtk-item-w940">
                                
                                <div class="mtk-item-sel-switch">
            
                                    <div class="mtk-sel-switch-div">
                                            <!--普通题-->
                                            <div class="mtk-item-border mtk-common-item">
                                                <!--提干-->
                                                <?php 
													$saveTitle = '';  
													if ($value['title']!== '') { 
														$saveTitle = 'save';
													} 
												?>
                                                <div class="mtk-problem clearfix <?php echo $saveTitle;?> ">
                                                    <div class="mtk-problem-ico fl" title="填写问题">
                                                        <img src="{{URL::to('/')}}/admin/images/mtk-01.png"/>
                                                    </div>
                                                    <div data-attr="title" contenteditable="true" class="edit-div mtk-item-tm fl" dir="ltr">{{$value['title']}}</div>
                                                    
                                                    <div class="mtk-item-score mtk-item-score-pt fl">
                                                        <input type="text" maxlength="3" data-attr="score" value="{{$value['score']}}"  placeholder="此题分数" class="xxxx" style="width:50px;border-style:none;" />
                                                    </div>
                                                    
                                                </div>
                                                <!--选项-->
                                                <div class="mtk-choice">
                                                    <ul class="sortable">
                                                        @if (!empty($value['choices_list']))
                                                            @foreach ($value['choices_list'] as $k=>$v)
                                                            <?php
																$saveChoice = '';
																if ($v !== '') {
																	$saveChoice = 'save';	
																}		                                                            
															?>
                                                            <li class="clearfix <?php echo $saveChoice; ?>">
                                                                <div class="fl mtk-c1">
                                                                    <?php
                                                                        if (empty($value['correct'])) $value['correct'] = array();	
                                                                    
                                                                        if (in_array($k, $value['correct'])){
                                                                            //正确答案
                                                                    ?>
                                                                        	<img src="{{URL::to('/')}}/admin/images/mtk-02.png" class="correct" title="点击切换" />
                                                                    <?php
                                                                        } else{
                                                                    ?>
                                                                        	<img src="{{URL::to('/')}}/admin/images/mtk-99.png" title="点击设置为正确答案"/>
                                                                    <?php		
                                                                        }
                                                                    ?>
                                                                   <span class="mtk-sel-letter"><?php echo $arrChioces[$k]; ?></span><span>.</span>
                                                                </div>
                                                                <div id="editor1" data-attr="choice" contenteditable="true" class="edit-div fl mtk-c2"  dir="ltr">{{$v}}</div>
                                                                <div class="fr mtk-c3">
                                                                    <div class="mtk-sel-del" del-attr="choice"><img src="{{URL::to('/')}}/admin/images/mtk-04.png"/></div>
                                                                </div>
                                                            </li>
                                                            @endforeach
                                                        @endif
                                                    </ul>
                                                    <!-- 添加选项 -->
                                                    <span class="mtk-add-item clearfix" title="新增一条选项">
                                                        <div class="mtk-add-item-txt1 fl">+</div>
                                                        <div class="mtk-add-item-txt2 fl">新增一条选项</div>
                                                    </span>
                                                </div>
                                            </div>
                                            
                                            <!--单选题解析-->
                                            <div class="mtk-item-analysis">
                                                <div class="mtk-item-analysis-tit">本题解析</div>
                                                <div class="mtk-item-analysis-div">
                                                    
                                                    <div class="mtk-item-analysis-ul">
                                                        <ul>
                                                            @if (!empty($value['attribute_list']))
                                                                @foreach ($value['attribute_list'] as $k=>$v)
                                                            	<?php
                                                                	$saveAttribute = '';
																	if ($k !== '') {
																		$saveAttribute = 'save';		
																	}
																?>
                                                            <li class="clearfix <?php echo $saveAttribute; ?>">
                                                                <div class="mtk-item-analysis-li1 fl">
                                                                    <input type="text" data-attr="attr" value="{{$k}}" maxlength="10" class="aaa"/>
                                                                </div>
                                                                <div class="mtk-item-analysis-li2 fl ">
                                                                    /
                                                                </div>
                                                                <div class="mtk-item-analysis-li3 edit-div fl" data-attr="attr" contenteditable="true" >{{$v}}</div>
                                                                <div class="mtk-item-analysis-li4 fl">
                                                                    <div class="mtk-sel-del" del-attr="attr"><img src="{{URL::to('/')}}/admin/images/mtk-04.png"/></div>
                                                                </div>
                                                            </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                        
                                                        <!-- 添加解析 -->
                                                        <span class="mtk-add-analysis clearfix" title="新增一条选项">
                                                            <div class="mtk-add-analysis-txt1 fl">+</div>
                                                            <div class="mtk-add-analysis-txt2 fl">新增一条解析</div>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </dt>
                        @elseif ($value['type'] == 1)
                            <!--复合题-->
                            <dt data-type="1" data-id="{{$value['gid']}}">
                                <div class="mtk-item-slide mtk-item-slide-noa">
                                    <div class="fl mtk-item-slide-">
                                        <span class="mtk-item-slide-tit">{{$key+1}}.</span>
                                        <span class="mtk-item-slide-type">复合题</span>
                                    </div>
                                    <div class="mtk-item-txt fl">
                                        <div class="clearfix">
                                            <div class="fl mtk-item-slide-txt"></div>
                                            <div class="fl">... </div>
                                        </div>
                                    </div>
                                    <span class="mtk-del-item fr" title="删剪此题"></span>
                                    <span class="mtk-show-item a" title="点击展开收起"></span>
                                </div>
                                <div class="mtk-item-w940">
                                    <div class="mtk-item-sel-switch">
                                        <div class="mtk-sel-switch-div">
                                        	<?php
                                            	$saveMaterial = '';
												if ($value['material'] !== '') {
													$saveMaterial = 'save';
												}
											?>
                                            <div class="mtk-edit-data-div <?php echo $saveMaterial;?>">
                                                <div class="edit-div mtk-edit-data a" data-attr="material" contenteditable="true">{{$value['material']}}</div>
                                            </div>
                                            
                                            <!--子题 start-->
                                            @if (!empty($value['sublist']))
                                                @foreach ($value['sublist'] as $k=>$v)	
                                                <div class="mtk-question" data-qid="{{$v['mid']}}">
                                                    <div class="mtk-item-border mtk-recom-item">
														<?php
                                                            $saveTitle = '';
                                                            if ($v['title'] !== '') {
                                                                $saveTitle = 'save';
                                                            }
                                                        ?>
                                                        <div class="mtk-problem clearfix <?php echo $saveTitle; ?>">
                                                            <div class="mtk-item-num fl" title="填写问题">
                                                                <span class="mtk-item-num-txt">{{$key+1}}-{{$k+1}}</span>. 
                                                            </div>
                                                            <div data-attr="title" class="edit-div mtk-item-tm fl" contenteditable="true" dir="ltr">{{$v['title']}}</div> 
                                                            <div class="mtk-item-score fl"><input type="text" data-attr="score" value="{{$v['score']}}" maxlength="3" placeholder="此题分数"/></div>
                                                            <div class="mtk-sel-del-fuhe fl" title="删剪此题"><img src="{{URL::to('/')}}/admin/images/mtk-03.png"/></div>
                                                        </div>
                                                        <div class="mtk-choice">
                                                            <ul class="sortable">
                                                                @if (!empty($v['choices_list']))
                                                                    @foreach ($v['choices_list'] as $_k=>$_v)
                                                                    <?php
																		$saveChoice = '';
                          												if ($_v !== '') {
																			$saveChoice = 'save';											
																		}		                                          
																	?>
                                                                    <li class="clearfix <?php echo $saveChoice; ?>">
                                                                        <div class="fl mtk-c1">
                                                                        <?php
                                                                            if (empty($v['correct'])) $v['correct'] = array();	
                                                                    
                                                                            if (in_array($_k, $v['correct'])){
                                                                                //正确答案
                                                                        ?>
                                                                                <img src="{{URL::to('/')}}/admin/images/mtk-02.png" class="correct" title="点击切换"/>
                                                                        <?php
                                                                            } else{
                                                                        ?>
                                                                                <img src="{{URL::to('/')}}/admin/images/mtk-99.png" title="点击设置为正确答案"/>
                                                                        <?php		
                                                                            }
                                                                        ?>
                                                                        <span class="mtk-sel-letter"><?php echo $arrChioces[$_k];?></span><span>.</span>
                                                                        </div>
                                                                        <div id="editor1"  class="edit-div fl mtk-c2" data-attr="choice" contenteditable="true" dir="ltr">{{$_v}}</div>
                                                                        <div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="{{URL::to('/')}}/admin/images/mtk-04.png"/></div></div>
                                                                    </li>
                                                                    @endforeach
                                                                @endif
                                                            </ul>
                                                            <span class="mtk-add-item clearfix" title="新增一条选项">
                                                                <div class="mtk-add-item-txt1 fl">+</div>
                                                                <div class="mtk-add-item-txt2 fl">新增一条选项</div>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="mtk-item-analysis">
                                                        <div class="mtk-item-analysis-tit">本题解析</div>
                                                        <div class="mtk-item-analysis-div">
                                                            <div class="mtk-item-analysis-ul">
                                                                <ul>
                                                                    @if (!empty($v['attribute_list']))
                                                                        @foreach ($v['attribute_list'] as $_k=>$_v)	
                                                                        <?php
																			$saveAttribute = '';
                                                        					if ($_k !== '') {
																				$saveAttribute = 'save';	
																			}                
																		?>
                                                                        <li class="clearfix <?php echo $saveAttribute; ?>">
                                                                            <div class="mtk-item-analysis-li1 fl"><input type="text" data-attr="attr" value="{{$_k}}" maxlength="10" placeholder="填写标题" class="aaa"/></div>
                                                                            <div class="mtk-item-analysis-li2 fl ">/ </div>
                                                                            <div class="mtk-item-analysis-li3 edit-div fl" contenteditable="true" data-attr="attr">{{$_v}}</div>
                                                                            <div class="mtk-item-analysis-li4 fl">
                                                                                <div class="mtk-sel-del" del-attr="attr" ><img src="{{URL::to('/')}}/admin/images/mtk-04.png"/></div>
                                                                            </div>
                                                                        </li>
                                                                        @endforeach
                                                                    @endif
                                                                </ul>
                                                                <span class="mtk-add-analysis clearfix" title="新增一条选项">
                                                                    <div class="mtk-add-analysis-txt1 fl">+</div>
                                                                    <div class="mtk-add-analysis-txt2 fl">新增一条解析</div>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach 
                                            @endif
                                            <!--end mtk-question-->
                                            <div class="mtk-add-question">新增一条问答 </div>
                                        </div>
                                        <!--end mtk-sel-switch-div-->
                                    </div>
                                    <!--end mtk-item-sel-switch-->
                                </div>
                                <!--end mtk-item-w940-->
                            </dt>
                        @endif
                    @endforeach        
                @endif
            </dl>
            
            <!--新增下一题 普通题 复合题-->
            <div class="mtk-add-new-common fl">
                新增普通题	
            </div>
            <div class="mtk-add-new-composite fl">
                新增复合题	
            </div>
            <div class="mtk-paper-preview fl">
            	保存（预览）	
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>

</div>    
@stop

@section('footer')
<div class="actGotop"><a href="javascript:;"></a></div>
<form>
<input type="hidden" id="paperId" value="{{$pid}}" />
<input type="hidden" id="teacher" value="<?php echo Session::get('tid');?>" />
<input type="hidden" id="is_editor" value="1" />
</form>

{{ HTML::script('admin/ueditor/ueditor.config.js') }}
{{ HTML::script('admin/ueditor/ueditor.all.min.js') }}
{{ HTML::script('admin/ueditor/lang/zh-cn/zh-cn.js') }}
<script type="text/javascript">
var word = "{{$paperInfo['word']}}",
	uploadConfig = {
		flash_swf_url : "{{URL::to('/')}}/admin/plupload/js/Moxie.swf",
		silverlight_xap_url : "{{URL::to('/')}}/admin/plupload/js/Moxie.xap",
		url : 'http://www.easymbook.com/upload/word',
		getHtml : 'http://www.easymbook.com/word/convert?h='
	};
	
	
	//注册按钮
	UE.registerUI('button', function(editor, uiName) {

		//创建一个button
		var btn = new UE.ui.Button({
			//按钮的名字
			name: 'caneditor',
			//提示
			title: '编辑与不可编辑模式',
			//添加额外样式，指定icon图标，这里默认使用一个重复的icon
			cssRules: 'background-position: -700px -40px;',
			//点击时执行的命令
			onclick: function() {
				
				var flag = parseInt($("#is_editor").val());
				if (flag) {
					UE.getEditor('self-editor').setDisabled('fullscreen');
					$("#is_editor").val(0);
				} else {
					UE.getEditor('self-editor').setEnabled();
					$("#is_editor").val(1);
				}	
				
			}
		});
		//因为你是添加button,所以需要返回这个button
		return btn;
	});
	
	window.ue = UE.getEditor('self-editor',{  
                //这里可以选择自己需要的工具按钮名称,此处仅选择如下五个  
                toolbars:[['FullScreen', 'Source', 'Undo', 'Redo', 'bold', 'test', 'forecolor','caneditor']],   
                labelMap : {'iseditor':'编辑模式'},
				//关闭elementPath  
                elementPathEnabled:false,  
				enableAutoSave : false
            });
	
	setTimeout(function(){
		$("#edui11_body").trigger('click');
	},800)
</script>
{{ HTML::script('admin/plupload/js/plupload.full.min.js') }}
{{ HTML::script('admin/js/socket.io.js') }}
{{ HTML::script('admin/js/mini/mtk_paper.js') }}
{{ HTML::script('admin/js/mini/paper.js') }}
<script type="text/javascript">

//编辑页面初始化百度编辑器和word内容
$(function(){
	
	if (word){
		var rquest_html = uploadConfig.getHtml+word;
		$.get( rquest_html, '', function(res) {
			setTimeout(function(){
				window.ue.setContent(res);
			},500);	
		}).fail(function(){
			alert('获取word失败');	
		});
	}	
});

</script>
</body>
</html>
@stop


