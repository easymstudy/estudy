@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper selected">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
	<a href="{{URL::to('/')}}/paperlist" class="paper-list-selected selected">作业列表</a>
    <a href="{{URL::to('/')}}/createPaper" class="paper-create">录入作业</a>
</div>

<div class="detail">
    <div class="create-paper">
        <div class="create-paper-step">
            <img class="create-step" src="{{URL::to('/')}}/admin/images/step4.png" />
            <div class="left"><i>卷名：</i>{{$paperInfo['title']}}</div>
            <div class="clear"></div>    
        </div>
        <div class="check-paper">
              <p>作业已提交,请在作业列表审核作业！</p>
              <p class="paper-info">总分: <span>{{$score}}</span>分&nbsp;&nbsp;&nbsp;&nbsp;共<span>{{$count}}</span>道题</p>
              <a class="btn btn-success nextstep" href="{{URL::to('/')}}/paperlist">完成</a> 
        </div>			
    </div>
</div>
@stop

@section('footer')
{{ HTML::script('admin/js/mini/paper.js') }}
</body>
</html>
@stop 


