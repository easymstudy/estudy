@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper selected">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
	<a href="{{URL::to('/')}}/paperlist" class="paper-list-selected selected">作业列表</a>
    <a href="{{URL::to('/')}}/createPaper" class="paper-create">录入作业</a>
</div>
<div class="detail">
    <div class="class-list">
        <div class="class-list-handle">
            <img src="{{URL::to('/')}}/admin/images/check.png" class="allcheck"/>
            <div class="allcheck">全选</div>
            <div class="input-group input-group-sm page">
              <div class="page-left"><button type="button" class="pre-page">◀</button></div>
                <div class="page-num"><input type="text" value="{{$p}}/{{$total}}" class="pagination-info" disabled="disabled"/></div>
              <div class="page-right"><button type="button" class="next-page">▶</button></div>
            </div>
            <div class="search">
                  <div class="input-search"><input type="text" class="form-control" placeholder='作业搜索'></div>
                <button class="btn btn-success  search-paper-list" type="button">搜索</button>
            </div>
            <div id="handle" style="text-align:center; line-height:60px;" >
                操作
                <ul class="handle-menu">
                	<li class="delete-paper">删除作业</li>
                    <li class="pass-paper">通过审核</li>
                    <li class="nopass-paper">撤销审核</li>
                    <li style="color:#999;cursor:default;">复制所选作业</li>
                    <li style="color:#999;cursor:default;">导入本地作业</li>
                    <li style="color:#999;cursor:default;">输出word</li>
                </ul>
            </div>
        </div>
        <table class="class-list-table">
            <thead>
                <tr class="class-list-header">
                    <td>序号</td>
                    <td>作业名</td>
                    <td>创建时间</td>
                    <td>状态</td>
                    <td>编辑</td>
                </tr>
            </thead>
            <tbody>
                @if (!empty($res))
                @foreach ($res as $key=>$value)
                <tr data-pid="{{$value['pid']}}">
                    <td class="paper-list-number">
                        <img src="{{URL::to('/')}}/admin/images/check.png"/>
                        <span>{{$key + 1}}</span>
                    </td>
                    <td>
                        {{$value['title']}}
                    </td>
                    <td><?php echo date('Y/m/d/H:i', $value['update_time']); ?></td>
                    <td class="status">
                        @if ($value['status'] == 1)
                            未审核
                        @else
                            <span class="ischeck">☆已审核</span>
                        @endif
                    </td>
                    <td>
                        <!-- 如果是未审核状态 直接跳到 编辑页面-->
                        <!-- 如果是已经审核状态 直接跳到 预览页面--> 
                        @if ($value['status'] == 1)
                            <a href="{{URL::to('/')}}/enterPaper/{{$value['pid']}}" class="lookup">查看</a>
                        @else
                            <a href="{{URL::to('/')}}/paperpreview/{{$value['pid']}}/2" class="lookup">查看</a>                                    	
                        @endif
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div> 
@stop

@section('footer')
<form>
<input type="hidden" value="{{$total}}" name="total"  />
</form>
{{ HTML::script('admin/js/mini/paper.js') }}
<script type="text/javascript">
$(function(){
	$("#handle").hover(function(){
		//移入
		$('.handle-menu').show();
	}, 
	function(){
		//移出
		$('.handle-menu').hide();
	})
});

</script>
</body>
</html>
@stop
