@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper selected">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
	<a href="{{URL::to('/')}}/paperlist" class="paper-list ">作业列表</a>
    <a href="{{URL::to('/')}}/createPaper" class="paper-create-selected selected">录入作业</a>
</div>

<div class="detail">
    <div class="create-paper preview-paper">
        <div class="create-paper-step">
        	@if ($status == 2)
	            <img class="create-step" src="{{URL::to('/')}}/admin/images/step4.png" />
            @else
	            <img class="create-step" src="{{URL::to('/')}}/admin/images/step3.png" />
            @endif
            <div class="left"><i>卷名：</i>{{$paperInfo['title']}}</div>
            <div class="right">
                @if ( $status == 2 )
                <a class="btn btn-success nextstep nextstepp" href="{{URL::to('/')}}/paperlist">返回列表</a>
                @else
                <a class="btn btn-success nextstep submit-check" href="{{URL::to('/')}}/submitcheck/{{$pid}}/{{$total_points}}/{{$serial_number}}">提交</a>
                <a class="btn btn-success nextstep back-edit" href="{{URL::to('/')}}/enterPaper/{{$pid}}">返回编辑</a>
                @endif
            </div>
            <div class="clear"></div>    
        </div>
        <!--预览内容-->
        <div class="paper-preview-main">
             <!--题目序号-->
             <div class="paper-preview-serial">
                <ul>
                    @for ($i=0; $i < $serial_number ; $i++)
                    <li>{{$i+1}}</li>
                    @endfor
                </ul>
                <div class="clearfix"></div>
             </div>
             <!-- 预览题目内容 -->
             <div class="paper-preview-main-box">
                 <div class="paper-preview-detail">
                        <dl>
                            @if (!empty($data))
                            <?php $arrChioces = array('A','B','C','D','E','F','G','H','I','J'); ?>
                            @foreach ($data as $key=>$value)

                                @if ($value['type'] == 0)
                                    <dt data-type="0" data-id="{{$value['mid']}}">
                                        
                                        <div class="sub-question">
                                            <div class="sub-title">
                                                <div class="serial-number">{{$key+1}}.</div>
                                                <div class="qustion-score">(本题<b>{{$value['score']}}</b>分)</div>
                                                <div class="question_text">{{$value['title']}}</div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="sub-chioce">
                                                <ul>
                                                	<?php
                                                    	if (empty($value['correct'])) $value['correct'] = array();	
													?>
                                                    @if (!empty($value['choices_list']))
                                                        @foreach ($value['choices_list'] as $k=>$v)
                                                    <li>
                                                        <div class="qustion-choice">
                                                        	
                                                        	<div class="right_img">
                                                            	<?php
																if (in_array($k, $value['correct'])) {
																?>
                                                                <img  src="{{URL::to('/')}}/admin/images/mtk-02.png"/>
                                                            	<?php
																}
																?>
                                                            </div>
                                                        	<div class="abcd">{{$arrChioces[$k]}}.</div>
                                                            <div class="answer_text">{{$v}}</div>
                                                               <div class="clearfix"></div>
                                                        </div>
                                                    </li>
                                                        @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                            <div class="clearfix"></div>
                                            
                                            <div class="sub-attr">
                                                <ul>
                                                    @if (!empty($value['attribute_list']))
                                                        @foreach ($value['attribute_list'] as $k=>$v)
                                                        <li>
                                                            <div class="attr">{{$k}}</div>
                                                            <div class="attr-detail">{{$v}}</div>
                                                        </li>
                                                        @endforeach
                                                    @endif
                                                </ul>	
                                            </div>
                                        </div>
                                    </dt>
                                @else
                                    <!--复合题-->
                                    <dt data-type="1" data-id="{{$value['gid']}}">
                                        <!--题目序号和类型-->
                                        <div>
                                            <div class="serial-number">{{$key+1}}.</div>
                                            <div class="yl_tixin">复合题材料</div>
                                        </div>
                                        <!--材料--> <div class="clear"></div>
                                        <div class="sub-material">
                                            {{$value['material']}}
                                        </div>
                                        
                                        <!--子题 start-->
                                        @if (!empty($value['sublist']))
                                            @foreach ($value['sublist'] as $k=>$v)	
                                                <div class="sub-question" data-qid="{{$v['mid']}}">
                                                    <div class="sub-title">
                                                        <div class="qustion-score">{{$key+1}}-{{$k+1}}.</div>
                                                        <div class="qustion-score">(本题<b>{{$v['score']}}</b>分)</div>
                                                        <div class="question_text">{{$v['title']}}</div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="sub-chioce">
                                                        <ul>
                                  						<?php
                                                    		if (empty($v['correct'])) $v['correct'] = array();	
														?>                      
                                                            @if (!empty($v['choices_list']))
                                                                @foreach ($v['choices_list'] as $_k=>$_v)	
                                                            <li>
                                                                <div class="sub-chioce-text">
                                                 					
                                                             		<div class="right_img">
                                                                   		<?php 
																		if (in_array($_k, $v['correct'])) {
                                                                		?>
                                                                    	<img class="right_img" src="{{URL::to('/')}}/admin/images/mtk-02.png"/>
                                                                     	<?php
																		}
																		?>
                                                                    </div>
                                                                   
                                                                    <div class="abcd">{{$arrChioces[$_k]}}.</div>
                                                                    <div class="answer_text">{{$_v}}</div>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    
                                                    <div class="sub-attr">
                                                        <ul>
                                                            @if (!empty($v['attribute_list']))
                                                                @foreach ($v['attribute_list'] as $_k=>$_v)
                                                            <li>
                                                                <div class="attr">{{$_k}}</div>
                                                                <div class="attr-detail">{{$_v}}</div>
                                                            </li>
                                                                @endforeach
                                                            @endif
                                                        </ul>	
                                                    </div>
                                                </div>
                                            @endforeach 
                                        @endif				
                                    </dt>                                            
                                @endif
                            @endforeach
                            @endif
                        </dl>		
                 </div>
                 <div style="height:20px;"><!--空div--></div>
             </div>
        </div>			
    </div>
</div> 
@stop

@section('footer')
{{ HTML::script('admin/js/mini/paper.js') }}
<script type="text/javascript">
$(function(){

	//最后一个dt没有border
	$('dt:last').css('border','none');
});
</script>
</body>
</html>
@stop