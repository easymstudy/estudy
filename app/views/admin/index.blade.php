@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index selected">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="shouye">
    <div class="shouye-guide">
        当前 (<?php echo date('Y/m/d H:i');  ?>)
    </div>
    <div class="shouye-total-stat">
        <div class="total-stat">
        	<p class="total-stat-summary">班级总数</p>
            <p class="total-stat-count"><?php if(isset($statbase['class']['class_total'])) echo $statbase['class']['class_total']; ?></p>
            <p class="total-stat-base">学生总数&nbsp;&nbsp;<span class="stat-base-data"><?php if(isset($statbase['class']['class_hasnum'])) echo $statbase['class']['class_hasnum']; ?></span></p>
            <p class="total-stat-base">平均每个班人数&nbsp;&nbsp;<span class="stat-base-data"><?php if(isset($statbase['class']['class_avg_num'])) echo $statbase['class']['class_avg_num']; ?></span></p>
        </div>
        <div class="total-stat">
        	<p class="total-stat-summary">布置的作业次数</p>
            <p class="total-stat-count"><?php if(isset($statbase['exam']['exam_total'])) echo $statbase['exam']['exam_total']; ?></p>
            <p class="total-stat-base">提交做作业人数&nbsp;&nbsp;<span class="stat-base-data"><?php if(isset($statbase['exam']['exam_join'])) echo $statbase['exam']['exam_join']; ?></span></p>
            <p class="total-stat-base">平均每次作业提交人数&nbsp;&nbsp;<span class="stat-base-data"><?php if(isset($statbase['exam']['exam_avg_join'])) echo $statbase['exam']['exam_avg_join']; ?></span></p>
        </div>
        <div class="total-stat">
        	<p class="total-stat-summary">作业总数/份</p>
            <p class="total-stat-count"><?php if(isset($statbase['paper']['paper_total'])) echo $statbase['paper']['paper_total']; ?></p>
            <p class="total-stat-base">题目总数&nbsp;&nbsp;<span class="stat-base-data"><?php if(isset($statbase['paper']['question_total'])) echo $statbase['paper']['question_total']; ?></span></p>
            <p class="total-stat-base">平均每次作业题目数&nbsp;&nbsp;<span class="stat-base-data"><?php if(isset($statbase['paper']['avg_question'])) echo $statbase['paper']['avg_question']; ?></span></p>
        </div>
        <div class="clear"></div>
    </div>
    
    @if (empty($exam_total))
		<div class="new-remind">
			<a href="{{URL::to('/')}}/classCreate">点击创建班级</a>
     	</div>
    @else
        <div class="shouye-exam-stat">
            <p class="exam-stat">最近作业情况</p>
            <div class="exam-stat-detail">
                <ul class="exam-stat-info">
                    <li class="witdth10">时间</li>
                    <li class="witdth30">作业名</li>
                    <li class="witdth15">参与班级</li>
                    <li class="witdth10">参与人数</li>
                    <li class="witdth10">作业平均分</li>
                    <li class="witdth10">最高分</li>
                    <li class="witdth10">最低分</li>
                </ul>
                @if (!empty($res)) 
                <ul class="exam-stat-data">
                    @foreach ($res as $key=>$val)
                    <li>
                        <p>今天</p>
                        <p>{{$val['exam_name']}}</p>
                        <p>{{$val['cname']}}</p>
                        <p>{{$val['join_num']}}</p>
                        <p><?php if($val['join_num']) echo round($val['total_points']/$val['join_num'], 2);  ?></p>
                        <p>{{$val['top_score']}}</p>
                        <p>{{$val['low_score']}}</p>
                    </li>
                    @endforeach
                </ul>
                @endif
            </div>
                
            <!--分页-->
             @if (!empty($res))
            <div class="exam-stat-p">
                <div class="p-detail">
                    共 <span>{{$count}}</span> 条记录 ,每页显示<span>{{$pagesize}}</span>条
                </div>
                <div class="p-fenye">{{$show}}</div>
            </div>
            @endif
        </div>
    @endif
</div>
@stop

@section('footer')
<script>
$(function(){
	
	// 判断360浏览器(极速模式)
	function isChrome360() {
		if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ) {
			var desc = navigator.mimeTypes['application/x-shockwave-flash'].description.toLowerCase();
			if (desc.indexOf('adobe') > -1) {
				 return true;           
			}
		}
		return false;
	}
	
	if ($.browser.chrome) {
		
		//判断是否为360浏览器
		if (isChrome360()) {
			//$("#browser_test").show();
			t_show();
		}
						
	} else if ($.browser.mozilla){
		
		if (parseInt($.browser.version) < 11) {
			// < ie11
			//$("#browser_test").show();		
			t_show();
		}
	} else {
		//不是谷歌浏览器
		t_show();	
	}
	
	function t_show(){
		
		$("#browser_test").show();
		
		var top = $("#browser_test").offset().top;
		
		//提示拖动
		$(window).scroll(function(){

			var overtop = $(window).scrollTop();
			
			if (overtop > top) {
				$("#browser_test").css({position:'fixed', top:'0px'});
			} else {
				$("#browser_test").css({position:'static'});
			}
			
			
		});
		
	}	

});
</script>
</body>
</html>
@stop