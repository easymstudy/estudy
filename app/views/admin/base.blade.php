<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type"  content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible"  content="IE=11">
<meta name="renderer" content="webkit"> 
<title>麦学习</title>
{{ HTML::style('admin/bootstrap-3.2.0/css/bootstrap.min.css') }} 
{{ HTML::style('admin/css/all-common.css') }}
{{ HTML::style('admin/css/all.min.css') }}
{{ HTML::script('admin/js/jquery-1.8.3.min.js') }}
{{ HTML::script('admin/bootstrap-3.2.0/js/bootstrap.min.js') }}
<script type="text/javascript">
var base_url = '{{URL::to('/')}}';
</script>
@section('cssjs')
@show
</head>
<body>
<div id="box">

	<!--预览加载 效果-->
	<div id="redactor_modal_overlay" class="none" >
    	<a href="javascript:;" class="loading-info"></a>
    </div>
    
    <!--提示浏览器-->
    <div id="browser_test" class="none">
    	<p>建议使用谷歌浏览器(首选) ， IE11或者更高版本浏览器 </p>
        <a href="http://pan.baidu.com/s/1qWp2akc" target="_blank">下载谷歌浏览器</a>
    </div>
    
    <div class="navigation common">
   	   	<a href="{{URL::to('/')}}/logout" class="logout">退出</a>
        <a href="{{URL::to('/')}}/changePass" class="user-center">
        	<?php
				$user = Cookie::get('user');
				echo $user['user'];		
			?>
        </a> 
 
    </div>
    <div class="main common">
        <div class="sidebar left">
            <div>
                <ul>
                	@section('sidebar')
        			@show
                </ul>
            </div>
       	</div>
        <div class="content right">
        	@section('content')
        	@show
        </div>
        <div class="clear"></div>
       	<div class="admin-icp">©2015 www.mstudy.me 麦学习 鄂ICP备12001098号-4</div>
        <!--反馈-->
        <script src="//tdcdn.blob.core.windows.net/toolbar/assets/prod/td.js" async data-trackduck-id="54fe9dbae1524bf50924076a"></script>
        <!--CNZZ-->
        <div style="display:none;">
        <script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1254516488'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1254516488%26show%3Dpic1' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
   </div>
</div>
@section('footer')
@show
