@extends('admin/base')

@section('cssjs')
{{ HTML::style('admin/jscrollpane/jquery.jscrollpane.css') }} 
@stop

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test selected">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/worklist" class="work-list">作业列表</a>
    <a href="{{URL::to('/')}}/work" class="work-create-selected selected">安排作业</a>
</div>
<div class="detail">
	<div class="exam-plan-top">
    	<a href="{{URL::to('/')}}/worklist">返回列表</a>
    </div>
    <div class="exam-plan-from">
		<div class="fl select-term">
        	<label for="select-homework"><img src="{{URL::to('/')}}/admin/images/examplan_01.png" />&nbsp;&nbsp;1.已选作业</label>
        	<div class="clear"></div>
        	<div class="selected-work">
                <div class="paper-lists">
                    <img src="{{URL::to('/')}}/admin/images/radio_selected.png" />
                    {{$res['title']}}
                </div>
            </div>
        </div>        
		<div class="fl select-term">
        	<label for="select-homework"><img src="{{URL::to('/')}}/admin/images/examplan_02.png" />&nbsp;&nbsp;2.已选班级</label>
			<div class="clear"></div>
            <div class="selected-work">
            	@if (!empty($students))
                    	@foreach ($students as $k=>$v)
                    	<div class="classes-lists" data-cid="{{$v['cid']}}">
                        	<img class="classes-lists-img" src="{{URL::to('/')}}/admin/images/radio_selected.png" />
                            {{$v['cname']}}
                            @if (!empty($v['uids']))
                            <?php $uids = json_decode(gzuncompress(base64_decode($v['uids'])),true);?>
                            <ul class="student-lists"><!--学生列表-->
                            	@if (!empty($res['uids']) && isset($res['uids'][$v['cid']]))
                                    @foreach ($res['uids'][$v['cid']] as $_v)
                                    <li data-uid="{{$_v}}">
                                        <img class="student-lists-img" src="{{URL::to('/')}}/admin/images/remove.png" />
                                        <?php echo $uids[$_v]['nickname']; ?>
                                    </li>
                                    @endforeach
                                @endif    
                            </ul>
                    		@endif        
                        </div>
                    	@endforeach	
                @endif
            </div>
        </div>        
		<div class="fl select-term">
        	<label for="select-homework"><img src="{{URL::to('/')}}/admin/images/examplan_03.png" />&nbsp;&nbsp;3.获取作业时间</label>
			<div class="clear"></div>
            <div class="selected-work">
            	<p>起始时间: <?php echo date('Y/m/d H:i',$res['startime']); ?></p>
                <p>截止时间: <?php echo date('Y/m/d H:i',$res['endtime']); ?></p>
            </div>
        </div>        
		<div class="fl select-term">
        	<label for="select-homework"><img src="{{URL::to('/')}}/admin/images/examplan_04.png" />&nbsp;&nbsp;4.作业时长</label>
            <div class="clear"></div>
            <div class="selected-work">
            	<p>{{$res['duration']}}</p>
            </div>
        </div>
        <div class="clear"></div>
        <?php 
			$checked = '';
			if ($res['is_see']) {
				$checked = 'checked';
			}
		?>
        <div class="form-group check <?php echo $checked; ?>">
              学生作业提交后,答案直接公布
        </div>       
    </div>
    <!-- end exam-plan-from-->
</div> 
@stop

@section('footer')
{{ HTML::script('admin/jscrollpane/jquery.jscrollpane.min.js') }}
{{ HTML::script('admin/jscrollpane/jquery.mousewheel.js') }}
<script type="text/javascript">
$(function(){
	//滚动条
	$(".scroll-div").jScrollPane();
});
</script>
</body>
</html>
@stop