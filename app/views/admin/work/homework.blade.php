@extends('admin/base')

@section('cssjs')
{{ HTML::style('admin/jscrollpane/jquery.jscrollpane.css') }} 
@stop

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test selected">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/worklist" class="work-list">作业列表</a>
    <a href="{{URL::to('/')}}/work" class="work-create-selected selected">安排作业</a>
</div>
<div class="detail">
    <div class="exam-plan-from">
    	<div class="fl select-term">
        	<label for="select-homework"><img src="{{URL::to('/')}}/admin/images/examplan_01.png" />&nbsp;&nbsp;1.选择作业（审核通过的作业）</label>
            <input type="text" class="form-control fl select-term-input" placeholder="选择作业名">
            <button class="fl" id="search-paper">搜索</button>
            <div class="clear"></div>
        	<div class="select-paper-div">
            	<div class="select-paper-name scroll-div">
					@if (!empty($papers))
                    	@foreach ($papers as $k=>$v)
                    	<div class="paper-lists" data-id="{{$v['pid']}}">
                        	<img src="{{URL::to('/')}}/admin/images/radio_select.png" />
                        	{{$v['title']}}
                        </div>
                    	@endforeach	
                    @endif                
                </div>
            </div>
            <div class="selected-paper-name">
            	<!-- 选择的作业名 -->		
            </div>
        </div>
        <div class="fl select-term">
            <label for="select-class"><img src="{{URL::to('/')}}/admin/images/examplan_02.png" />&nbsp;&nbsp;2.选择班级</label>
            <input type="text" class="form-control fl select-term-input"  placeholder="选择班级名">
			<button class="fl" id="search-paper">搜索</button>       			
        	<div class="clear"></div>
			<div class="select-classes-div">
            	<div class="select-classes-name scroll-div">
					@if (!empty($classes))
                    	@foreach ($classes as $k=>$v)
                    	<div class="classes-lists" data-cid="{{$v['cid']}}" title="{{$v['cname']}}" >
                        	<img class="classes-lists-img" src="{{URL::to('/')}}/admin/images/radio_select.png" />
                            {{$v['cname']}}
                            @if (!empty($v['uids']))
                            <?php $v['uids'] = json_decode(gzuncompress(base64_decode($v['uids'])),true);  ?>
                            <ul class="student-lists none"><!--学生列表-->
                            	@foreach ($v['uids'] as $_v)
                                <li data-uid="{{$_v['uid']}}" data-oid="{{$_v['openid']}}">
                                	<img class="student-lists-img" src="{{URL::to('/')}}/admin/images/radio_select.png" />
                                    {{$_v['nickname']}}
                                </li>
                                @endforeach
                            </ul>
                    		@endif        
                        </div>
                    	@endforeach	
                    @endif                
                </div>
            </div>
        </div>
        <div class="fl">
        	<!--获取作业时间-->
            <div>
                <p><label><img src="{{URL::to('/')}}/admin/images/examplan_03.png" />&nbsp;&nbsp;3.获取作业时间</label></p>
                <input type="text" class="form-control startime" placeholder="起始时间">
                <input type="text" class="form-control endtime" placeholder="截至时间">
        		<div class="clear"></div>
            </div>
            <!--作业时长-->
            <div class="work-duration">
                <p><label><img src="{{URL::to('/')}}/admin/images/examplan_04.png" />&nbsp;&nbsp;4.作业时长</label></p>
                <div class="fl">
                	<input type="text" class="form-control hours" value="1" maxlength="2" placeholder="">
                    <span>小时</span>
                </div>
                <div class="fl">
                	<input type="text" class="form-control minute" value="0" maxlength="2" placeholder="">
                    <span>分钟</span>
                </div>
                <div class="fl">
                	<input type="text" class="form-control second" value="0" maxlength="2" placeholder="">
                    <span>&nbsp;秒</span>
        		</div>
                <div class="clear"></div>
            </div>
            <?php $checked = empty($res['is_see']) ? '' : 'checked';  ?>
            <div class="form-group check {{$checked}}">
              学生作业提交后,答案直接公布
            </div>
        </div>
    </div><!-- end exam-plan-from-->
    <div class="form-group submit-work">
        <button type="button" id="setWork" class="btn btn-default">确认提交</button>
    </div>    
</div> 
@stop

@section('footer')
{{ HTML::script('admin/js/mini/work.js') }}
{{ HTML::script('admin/my97date/WdatePicker.js') }}
<script type="text/javascript">
$(function(){
	//直接发布
	$('.check').on('click', function(){
			var _this = $(this);
			if ( _this.hasClass('checked') ) {
				_this.removeClass('checked');						
			} else {
				_this.addClass('checked');
			}
	});	
	
	//起始时间
	$('.startime').on('click', function(){
		WdatePicker({
			dateFmt:'yyyy-MM-dd HH:mm',
			minDate:'<?php echo date('Y-m-d H:i'); ?>'
		});		
	});
	
	//结束时间
	$('.endtime').on('click', function(){
		WdatePicker({
			dateFmt:'yyyy-MM-dd HH:mm',
			minDate:'<?php echo date('Y-m-d H:i'); ?>'		
		});		
	});
	
	//滚动条
	//$(".scroll-div").jScrollPane();
		
});
</script>
</body>
</html>
@stop