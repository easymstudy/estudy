@extends('admin/base')

@section('sidebar')
<li><a href="{{URL::to('/')}}/index" class="index">首页</a></li>
<li><a href="{{URL::to('/')}}/paperlist" class="manger-paper">作业管理</a></li>
<li><a href="{{URL::to('/')}}/classList" class="manger-class">班级管理</a></li>
<li><a href="{{URL::to('/')}}/worklist" class="set-test selected">安排作业</a></li>
<li><a href="{{URL::to('/')}}/bindWx" class="set-system">系统设置</a></li>
@stop

@section('content')
<div class="sub-navigation">
    <a href="{{URL::to('/')}}/worklist" class="work-list-selected selected">作业列表</a>
    <a href="{{URL::to('/')}}/work" class="work-create">安排作业</a>
</div>
<div class="detail">
    <div class="class-list">
        <div class="class-list-handle">
            <!--<img src="{{URL::to('/')}}/admin/images/check.png" class="allcheck"/>-->
            <div class="allcheck"></div>
            <div class="input-group input-group-sm page">
              <div class="page-left"><button type="button" class="pre-page">◀</button></div>
              	<?php 
					if (empty($total)) 
						$total = 1;		
				 ?>
                <div class="page-num"><input type="text" value="{{$p}}/<?php echo $total;?>" class="pagination-info" disabled="disabled"/></div>
              <div class="page-right"><button type="button" class="next-page">▶</button></div>
            </div>
            <div class="search">
                  <div class="input-search"><input type="text" class="form-control" placeholder='作业搜索'></div>
                <button class="btn btn-success" type="button">搜索</button>
            </div>
            <div id="handle" style="text-align:center; line-height:60px;" >
                操作
                <ul class="handle-menu">
                    <li class="delete-exam">删除作业</li>
                    <li style="color:#999;cursor:default;">输出成绩报告PDF</li>
                </ul>
            </div>
        </div>
        <table class="class-list-table">
            <thead>
                <tr class="class-list-header">
                    <td>序号</td>
                    <td>作业名</td>
                    <td>开始时间</td>
                    <td>结束时间</td>
                    <td>公布答案时间</td>
                    <td>查看详情</td>                    
                </tr>
            </thead>
            <tbody>
                @if (!empty($res))
                @foreach ($res as $key=>$value)
                <tr data-eid="{{$value['eid']}}">
                    <td class="paper-list-number">
                        <img src="{{URL::to('/')}}/admin/images/check.png"/>
                        <span>{{$key + 1}}</span>
                    </td>
                    <td>{{$value['title']}}</td>
                    <td><?php if(!empty($value['startime'])) echo date('Y/m/d H:i', $value['startime']);?></td>
                    <td><?php if(!empty($value['endtime']))  echo date('Y/m/d H:i', $value['endtime']);?></td>
                    <td>
						<?php
							if ($value['is_see']){
								echo '答题完成即出答案';							
							} else {
								if(!empty($value['finishtime'])) { 
									echo date('Y/m/d H:i', $value['finishtime']);
								}
							}
						?>
                    </td>
                    <td><a href="{{URL::to('/')}}/workdetail/{{$value['eid']}}"class="lookup">查看</a></td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div> 
@stop

@section('footer')
<form>
<input type="hidden" value="{{$total}}" name="total"  />
</form>
{{ HTML::script('admin/js/mini/work.js') }}
<script type="text/javascript">

$(function(){
	$("#handle").hover(function(){
		//移入
		$('.handle-menu').show();
	}, 
	function(){
		//移出
		$('.handle-menu').hide();
	})
});
</script>
</body>
</html>
@stop