@extends('user.base')
@section('main')
<div class="center">
    <div class="findpsw-ok">
       <ul>
       	<li class="active"><i>01</i>发送注册邮件</li>
        <li  class="choice"><i>02</i>进行安全验证</li>
        <li><i>03</i>设置新密码</li>
       </ul>    <div class="clear"></div>
       @if (empty($status))  
       <div class="findpsw-ok-text">我们已经向您的注册邮箱{{$username}}发送了一封找回密码的邮件
       </br>请您注意接收邮件!</div>
       @elseif ($status == 1)
       <div class="findpsw-ok-text">修复链接失效
       </br>重新发送邮件<a href="{{URL::to('/')}}/findpsw">返回</a>!</div>       
       @elseif ($status == 2)
       <div class="findpsw-ok-text">
       </br>新密码不能和旧密码一致</div>
       @elseif ($status == 3)
       <div class="findpsw-ok-text">
       </br>密码修复失败</div>
       @endif
    </div>

</div>
@stop