<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type"  content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible"  content="IE=11">
<meta name="renderer" content="webkit"> 
<title>麦学习</title>
{{ HTML::style('admin/css/all-common.css') }} 
{{ HTML::style('admin/css/login-register.min.css') }}
</head>
<body>
<div id="header">
    <!--导航1-->
    <div class="navigation">
        <div class="logo fl"></div>
        <div class="info fl">微信上的智能作业本</div>
        <div class="menu fl">
            <a href="http://www.mstudy.me/">首页</a>
            <a href="http://www.mstudy.me/pro.html">产品特点</a>
            <a href="http://help.mstudy.me">常见问题</a>
            <a href="http://www.mstudy.me/business.html">加入我们</a>
            <a href="http://q.mstudy.me/">互动讨论</a>
            <a href="http://admin.mstudy.me/" class="admin">登录教师后台</a>
        </div>
        <div class="clear"></div>
    </div>


</div>

<div id="main">
    @yield('main')
</div>

<div id="intrdoce">
    <div class="center">
        <div class="siteinfo">
            <ul>
                <li>
                    <img src="{{URL::to('/')}}/admin/images/kao.png" />
                    <p class="introduction">自定义考卷</p>
                    <p>专注教学辅助,自定义上传作业</p>
                    <p>进行作业安排和布置作业</p>
                </li>
                <li>
                    <img src="{{URL::to('/')}}/admin/images/lian.png" />
                    <p class="introduction">班级学员管理</p>
                    <p>结合微信,学员及时通知</p>
                    <p>互动,协同教学学习</p>
                </li>
                <li>
                    <img src="{{URL::to('/')}}/admin/images/ping.png" />
                    <p class="introduction">能力评估</p>
                    <p>学员学习互动数据统计紧密跟踪</p>
                    <p>实时掌握学习动态</p>
                </li>
            </ul>	
        </div>
    </div>
</div>

<div id="footer">
    <div class="icp">
        ©2015 www.mstudy.me 麦学习 鄂ICP备12001098号-4
    </div>
   <!--CNZZ-->
        <div style="display:none;">
        <script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1254516488'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s95.cnzz.com/z_stat.php%3Fid%3D1254516488%26show%3Dpic1' type='text/javascript'%3E%3C/script%3E"));</script>
        </div>
        <!--反馈-->
        <script src="//tdcdn.blob.core.windows.net/toolbar/assets/prod/td.js" async data-trackduck-id="54fe9dbae1524bf50924076a"></script>
  
</div>
{{ HTML::script('admin/js/jquery-1.8.3.min.js') }}  
<script>
var base_url = "{{URL::to('/')}}";
$(function(){
	//ie提示
	$("input").placeholder();
	
	$(".logo").click(function(){
		window.location.href = 'http://www.mstudy.me/';
	});
	
});
</script>
{{ HTML::script('admin/js/mini/user.js') }} 
</body>
</html>
