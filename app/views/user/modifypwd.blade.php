@extends('user.base')
@section('main')
<div class="center">
    <div class="findpsw-ok">
           <ul>
            <li class="active"><i>01</i>发送注册邮件</li>
            <li class="active"><i>02</i>进行安全验证</li>
            <li  class="choice"><i>03</i>设置新密码</li>
           </ul>  
           <div class="clear"></div> 
 	   @if (empty($success))
           <div class="user-form" style="margin:0 auto;">
           <form action="{{URL::to('/')}}/setpwd" method="post" >
                <input type="hidden" name="mailType" value="{{$data['mailType']}}" />
                <input type="hidden" name="sid" value="{{$data['sid']}}" /> 
                <input value="" name="newpass" placeholder="设置新密码" class="form-control newpwd"/>
                <div class="form-control">
                    <input type="text" name="rnewpass" class="code r_newpwd" placeholder="确认新密码" />
                </div>
                <button type="submit" class="btn btn-default fixpwd">确认修改</button>
           </form>
           </div>  
       @else
	   <div class="findpsw-ok-text">您已成功设置密码，请使用新密码<a href="{{URL::to('/')}}/login">登录</a>!</div>
       @endif
    </div>

</div>
@stop