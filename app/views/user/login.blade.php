@extends('user.base')
@section('main')
<div class="center">
	<div class="user-third fl">
    
    </div>
    <div class="user-form fl">
            <form role="form">
            	<i class="tis"></i> <!--tis_-->
                <div class="form-control">
                <input type="text" name="user" class="account"  placeholder="输入邮箱" /></div>
                <i class="tis"></i>
                <div class="form-control">
                <input type="password" name="pass" class="account" placeholder="输入密码" /></div>
                <i class="tis"></i>
                <div class="check_code">
                <input type="text" name="code" class="code"placeholder="输入验证码" /></div>
                <img src="{{URL::to('/')}}/code" id="code" />
                <button type="button" class="btn btn-default login">登录</button>
                <div class="operate">
                    <input type="checkbox" id="remember" />
                    <label for="remember">默认保持帐号登录</label>
                    <a href="{{URL::to('/')}}/findpsw" class="forgetpass">忘记密码?</a>
                    <a href="register" class="goregister">注册新帐号</a>
                </div>
            </form>            
    </div>
    <div class="clear"></div>
</div>
@stop
