@extends('user.base')
@section('main')
<div class="center">
	@if ($res['status'] == 1)
        <div class="findpsw-ok-text" style="margin:0 auto;">
            你的帐号已经激活成功,返回登录页面进行登录。
            <a href="{{URL::to('/')}}/login">返回登录</a>
        </div>  
    @else
    	
        @if ($type == 1)
            <div class="findpsw-ok-text" style="margin:0 auto;">
                你的账号尚未激活。请点击一下"激活"按钮发送到你的邮箱，进入邮箱点击链接激活帐号。
                <a href="javascript:;" class="activation">激活</a>
            </div>  
        @else
            <div class="findpsw-ok-text" style="margin:0 auto;">
                恭喜你，注册成功。请点击一下"激活"按钮发送到你的邮箱，进入邮箱点击链接激活帐号。
                <a href="javascript:;" class="activation">激活</a>
            </div>  
        @endif  
    @endif
</div>
@stop