@extends('user.base')
@section('main')
<div class="center">
    <div class="findpsw-ok">
           <ul>
            <li class="active"><i>01</i>发送注册邮件</li>
            <li class="active"><i>02</i>进行安全验证</li>
            <li  class="choice"><i>03</i>设置新密码</li>
           </ul>  
           <div class="clear"></div> 
	   	   <div class="findpsw-ok-text">您已成功设置密码，请使用新密码<a href="{{URL::to('/')}}/login">登录</a>!</div>
    </div>

</div>
@stop