<?php
require_once Config::get('app.ThirdClass')."/weixin/jssdk.php";
$jssdk = new JSSDK("wxf305ea0876e9955e", "a39cb813682c13d004633ac46c473bd7");
$signPackage = $jssdk->GetSignPackage();
?>
<html lang="zh-CN" class=" -webkit-">
<head>
	<meta charset="utf-8">
	<meta name="author" content="joneshe">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<title>麦学习 | 微信智能作业本</title>
	<link rel="stylesheet" href="{{URL::to('/')}}/share/share.css" />
</head>
	<script type="text/javascript" src="{{URL::to('/')}}/share/zepto.min.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/share/zepto.touch.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/share/prefixfree.min.js"></script>
	<script type="text/javascript" src="{{URL::to('/')}}/share/XMANSlide.module.js"></script>
	<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
	
<body>
	<div class="container" >
		<div class="slide-content">
			<div class="slide-layout slide-layout-1" id="slideLayout">
				<div class="slide-layout__page page-1">
					<div class="item-01">
						<img src="{{URL::to('/')}}/share/images/p1_i1.png"></div>
					<div class="animation item-02" data-item="item-02" data-animation="bounceInDown" data-delay="500">
						<div class="animation item-03-box" data-item="item-03-box" data-animation="bounceIn" data-delay="1500"> <i class="item-03"></i>
						</div>
					</div>
					<div class="animation item-04 big-txt htt" data-item="item-04" data-animation="fadeInLeft" data-delay="2000">Hey,从今天起</div>
					<div class="animation item-05 small-txt htt" data-item="item-05" data-animation="fadeInRight" data-delay="2500">一起用微信做作业</div>
				</div>
				<div class="slide-layout__page page-2">
					<div class="animation item-01 big-txt htt" data-item="item-01" data-animation="fadeInLeft" data-delay="200">从前……</div>
					<div class="animation item-02 small-txt htt" data-item="item-02" data-animation="fadeInRight" data-delay="700">要去组长那里交作业</div>
					<div class="item-03"> <i class="i1"></i>
						<i class="i2"></i>
						<i class="animation i3" data-item="i3" data-animation="fadeIn" data-delay="1500"></i>
						<i class="animation i4" data-item="i4" data-animation="fadeIn" data-delay="1600"></i>
						<i class="animation i5" data-item="i5" data-animation="fadeIn" data-delay="1800"></i>
						<i class="animation i6" data-item="i6" data-animation="fadeIn" data-delay="2000"></i>
						<i class="animation i7" data-item="i7" data-animation="fadeIn" data-delay="2200"></i>
						<i class="animation i8" data-item="i8" data-animation="bounceIn" data-delay="2400"></i>
						<i class="animation i9" data-item="i9" data-animation="bounceIn" data-delay="2800"></i>
						<i class="animation i10" data-item="i10" data-animation="fadeIn" data-delay="3000"></i>
					</div>
				</div>
				<div class="slide-layout__page page-3">
					<div class="item-box">
						<i class="item-01"></i>
						<i class="animation item-02" data-item="item-02" data-animation="bounceIn" data-delay="1700"></i>
						<i class="animation item-02-wave" data-item="item-02-wave" data-animation="wave" data-duration="2000" data-timing-function="ease-out" data-iteration-count="infinite" data-delay="2700"></i>
					</div>
					<div class="animation item-05 title-txt htt" data-item="item-05" data-animation="fadeInLeft" data-delay="200">微信扫码加入班级</div>
					<div class="animation item-03 big-txt htt" data-item="item-03" data-animation="fadeInLeft" data-delay="700">现在……</div>
					<div class="animation item-04 small-txt htt" data-item="item-04" data-animation="fadeInRight" data-delay="1000">随时随地，微信一起做作业~</div>
				</div>
				<div class="slide-layout__page page-4">
					<div class="item-box">
						<div class="item-01"></div>
						<i class="animation item-02" data-item="item-02" data-animation="bounceIn" data-delay="1700"></i>
					</div>
					<div class="animation item-03 big-txt htt" data-item="item-03" data-animation="fadeInLeft" data-delay="200">微信，让你对学习更有信心</div>
					<div class="animation item-04 small-txt htt" data-item="item-04" data-animation="fadeInRight" data-delay="700">能一起答题，一起当学霸哦</div>
				</div>
				<div class="slide-layout__page page-5">
					<div class="item-box">
						<div class="item-01"></div>
						<i class="animation item-02" data-item="item-02" data-animation="bounceIn" data-delay="1700"></i>
					</div>
					<div class="animation item-03 big-txt htt" data-item="item-03" data-animation="fadeInLeft" data-delay="200">当你遇到问题时</div>
					<div class="animation item-04 small-txt htt" data-item="item-04" data-animation="fadeInRight" data-delay="700">我们的老师们无处不在</div>
				</div>
				<div class="slide-layout__page page-6">
					<div class="item-01">
						<img src="{{URL::to('/')}}/share/images/p6_i1.png"></div>
					<div class="item-02">
						<div class="item-03-box">
							<i class="item-03"></i>
						</div>
					</div>
					<div class="animation item-04 big-txt htt" data-item="item-04" data-animation="fadeInLeft" data-delay="200">同学们都来这了</div>
					<div class="animation item-05 small-txt htt" data-item="item-05" data-animation="fadeInRight" data-delay="700">使用微信做作业</div>
					<a id="goChong" class="animation item-btn htt" href="<?php echo "https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf305ea0876e9955e&redirect_uri=".urlencode('http://admin.mstudy.me/wx/authorize/1')."&response_type=code&scope=snsapi_base&state=1#wechat_redirect"; ?>" data-item="item-btn" data-animation="bounceIn" data-delay="1700">立即体验</a>
					<span class="animation item-btn-wave htt" data-item="item-btn-wave" data-animation="wave" data-duration="1800" data-timing-function="ease-out" data-iteration-count="infinite" data-delay="2700"></span>
					<i class="animation item-06" data-item="item-06" data-animation="cloud" data-duration="10000" data-timing-function="linear" data-iteration-count="infinite" data-delay="700"></i>
					<i class="animation item-07" data-item="item-07" data-animation="cloud" data-duration="10000" data-timing-function="linear" data-iteration-count="infinite" data-delay="1200"></i>
					<i class="animation item-08" data-item="item-08" data-animation="cloud" data-duration="10000" data-timing-function="linear" data-iteration-count="infinite" data-delay="1700"></i>
				</div>
			</div><div class="slide-nav"><a class="nav-item nav-item--current" href="javascript:;">1</a><a class="nav-item" href="javascript:;">2</a><a class="nav-item" href="javascript:;">3</a><a class="nav-item" href="javascript:;">4</a><a class="nav-item" href="javascript:;">5</a><a class="nav-item" href="javascript:;">6</a></div>
		</div>
	</div>
	<script src="{{URL::to('/')}}/share/index.js"></script>
	<script>
	wx.config({
            debug: false,
            appId: '<?php echo $signPackage["appId"];?>',
            timestamp: <?php echo $signPackage["timestamp"];?>,
            nonceStr: '<?php echo $signPackage["nonceStr"];?>',
            signature: '<?php echo $signPackage["signature"];?>',
            jsApiList: [
               'onMenuShareAppMessage',
               'onMenuShareTimeline'
            ]
         });
	wx.ready(function(){
		  // 2.1 监听“分享给朋友”，按钮点击、自定义分享内容及分享结果接口
		   wx.onMenuShareAppMessage({
		      title: '微信智能作业本',
		      desc: '史上最NB的作业神器！不一样的布置作业方式，让作业随时随地，在微信一起作业吧！',
		      link: 'http://admin.mstudy.me/sharelink',
		      imgUrl: '{{URL::to('/')}}/share/images/logo1.png',
		    });
		  // 2.2 监听“分享到朋友圈”按钮点击、自定义分享内容及分享结果接口
		 wx.onMenuShareTimeline({
		      title: '微信智能作业本',
		      link: 'http://admin.mstudy.me/sharelink',
		      imgUrl: '{{URL::to('/')}}/share/images/logo1.png',
		  	});
	});
	</script>
</body></html>