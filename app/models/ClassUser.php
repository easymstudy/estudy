<?php
class ClassUser extends Eloquent {
	
	protected $table = 'class_user';
	public $timestamps = false;
	
	
	//查看某个班级的学员情况
	public function get_class_userinfo($cid){
		
		$res = $this->where('cid', $cid)
					->select('uids')
					->first();
		
		if ($res) {
			
			$res = $res->toArray();
			if (empty($res['uids'])){
				return false;
			} else {
				return $res;				
			}

		} else {
			return false;		
		}
	}
	
	//更新某个班级的学员情况
	public function update_class_userinfo($cid,$uids){
		
		$data['uids'] = $uids;
		$this->where('cid', $cid)->update($data);
		return true;
		
	}

}