<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 * Email - 邮件管理
 */

class Email extends Eloquent {
	
	protected $table = 'email';
	public $timestamps = false;
	
	/********注册激活***************/
	public function user_send_mail($data){

		$this->table = 'user_reg_email';
		if ($id = $this->insertGetId($data)) {
			return $id;
		} else {
			return false;
		}
	}
	
	public function get_user_acount($sid){
		
		$this->table = 'user_reg_email';
		$where = array(
			'id'=>(int)$sid,
		);
		$res = $this->where($where)
					  ->select('account_code')
					  ->first();
		
		if ($res) {
			return $res->toArray();
		}
		
		return false;
	}
	
	
	public function get_user_mail($sid){
		
		$this->table = 'user_reg_email';
		$where = array(
			'id'=>(int)$sid,
			'status'=>0,
		);
		$res = $this->where($where)
					  ->where('deadline', '>', $_SERVER["REQUEST_TIME"])
					  ->select('account_code')
					  ->first();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}		
	
	}	

	//激活成功， 修改链接状态
	public function update_status($sid){

		$this->table = 'user_reg_email';
		if ( $this->where('id', intval($sid))->update(array('status'=>1)) ){
			return true;
		} 
		return false;
	}
	
	
	/********找回密码**********/
	//生成邮件
	public function send_mail($data){
		
		if ($id = $this->insertGetId($data)) {
			return $id;
		} else {
			return false;
		}
	}
	
	//查询邮件
	public function get_mail($sid){
		
		$where = array(
			'id'=>(int)$sid,
			'status'=>0,
		);
		$count = $this->where($where)
					  ->where('deadline', '>', $_SERVER["REQUEST_TIME"])
					  ->count();
		return $count;		
	
	}	
	
	//获取用户信息
	public function get_account($sid){
		
		$where = array(
			'id'=>(int)$sid,
			'status'=>0,
		);
		
		$res = $this->where($where)
					->where('deadline', '>', $_SERVER["REQUEST_TIME"])
					->select('account_code')
					->first();
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}
	}
	
	//更新信息
	public function update_email($sid){
		
		if ( $this->where('id', intval($sid))->update(array('status'=>1)) ){
			return true;
		} else {
			return false;
		}
			
	}
	
}