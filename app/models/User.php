<?php
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'teacher';
	public $timestamps = false;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array( 'remember_token');
	
	//激活
	public function update_status($username){
		
		$where = array(
			'username'=>$username,
			'status'=>0,
		);
		
		if ( $this->where($where)->update(array('status'=>1)) ) {
			return true;
		}
		
		return false;
		
	}
	
	
	//sid查询
	public function get_admin_info($tid){
		
		$where = array(
			'tid'=>$tid,
		);
		
		$res = $this->where($where)->select('username','status')->first();
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}
	
	}
	
	
	//获取后台管理员信息
	public function get_admin($username){
		
		$res = $this->where('username', $username)->select('tid', 'password','status')->first();
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}
	}
	
	//更新后台管理员密码
	public function update_admin($data){
		
		$tid = $data['tid'];
		unset($data['tid']);
		if ($this->where('tid', $tid)->update($data)) {
			return true;
		} else {
			return false;
		}
	}
	
	//登录
	public function login($data){
		
		$res = $this->where('username',$data['user'])->select('tid','password','status')->first();
		if ($res) {
			$res = $res->toArray();
			$pass = md5($data['pass'].Config::get('app.passkey'));
			if ($res['password'] == $pass) {
				//更新登陆ip和时间
				
				$flag = $this->where('tid', $res['tid'])
							->update(array(
								'login_time'=> $_SERVER['REQUEST_TIME'],
								'login_ip' => Request::getClientIp(),
							));
				return $res;			
			}
		}
		return false;
		
	}

	//注册
	public function register($data){
				
		$data = array(
			'username' => $data['user'],
			'password' => md5($data['pass'].Config::get('app.passkey')),
			'login_time' => $_SERVER['REQUEST_TIME'],
			'login_ip' => Request::getClientIp(),
			'status'=>0, //未激活
		);
		
		if ( $id = $this->insertGetId($data) ) {
			return $id;
		} else {
			return false;
		}
	}
	
	//修改密码
	public function changPassword($data) {
		
		$arr = array(
			'password'=>md5($data['pass'].Config::get('app.passkey')),
		);
		
		if ( $this->where('tid', $data['uid'])->update($arr) ) {
			return true;		
		}
		return false;	
	}
	
	/****************微信客户端用户****************/
	//插入user
	public function subscribe($data){
		
		$this->table = 'user';
		if ( $id = $this->insertGetId($data) ) {
			return $id;
		} else {
			return false;
		}		
	}
	
	//取消关注
	public function unsubscribe($data){
		
		$this->table = 'user';
		$openid = $data['openid'];
		unset($data['openid']);
		$this->where('openid', $openid)->update($data);
		return true;					
	}
	
	//修改关注状态
	public function update_subscribe($fromUsername, $status){
		$this->where('openid', $fromUsername)->update(array('status'=>$status));
		return ;
	}
	
	//查询是否关注
	public function isSubscribe($openid){

		$this->table = 'user';
		$where = array(
			'openid'=>$openid,
			'status'=>1
		);
		$res = $this->where($where)->select('uid')->first();
		if ($res) {
			return $res->toArray();		
		} else {
			return false;
		}
					
	}
	
	
	//查询是否存在此用户
	public function findUser($openid){

		$this->table = 'user';
		$where = array(
			'openid'=>$openid,
		);
		$res = $this->where($where)->select('uid')->first();
		if ($res) {
			return $res->toArray();		
		} else {
			return false;
		}
		
	}
	
	//查询用户是否关注
	public function find_user($openid){
		
		$this->table = 'user';
		$where = array(
			'openid'=>$openid,
			'status'=>1
		);
		$res = $this->where($where)->select('uid','nickname')->first();
		
		if ($res){
			return $res->toArray();		
		}else {
			return false;
		}
	}
	
	//查看用户信息
	public function get_user_info($uid){
		
		$this->table = 'user';
		$res = $this->where('uid', $uid)->select('uid','nickname','remark','headimgurl')->first();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}
	
	}
	
	//设置备注名
	public function setremark($_data){
		$this->table = 'user';
		
		$data = array(
			'remark'=>$_data['remark']
		);
		
		if ( $this->where('uid', $_data['uid'])->update($data) ) {
			return true;
		} else {
			return false;
		}
	}
        
}
