<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 * Work manger - 作业管理
 */

class Work extends Eloquent {
	
	
	protected $table = 'exam';
	public $timestamps = false;
	
	//获取作业条数
	public function work_count(){
		//总条数
		$where = array(
			'is_delete'=>0,
			'tid'=>Session::get('tid')
		);
		
		$count = $this->where($where)->count();
		return $count;
	}
	
	//获取这次作业信息
	public function get_exam_info($eid){
		
		$res = $this->where('eid', $eid)->select('eid','title','duration','startime','endtime','is_see','uids')->first();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;		
		}
	}
	
	//作业列表
	public function worklist($limit){
		
		$where = array(
			'is_delete'=>0,
			'tid'=>Session::get('tid')
		);
		
		$res = $this->where($where)
					->orderBy('eid', 'desc')
					->select('eid', 'title', 'startime', 'endtime', 'finishtime', 'is_see')
					->skip($limit['start'])
					->take($limit['pagesize'])
					->get();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}		
				
	}
	
	//提交作业
	public function setWork($data){
				
		if ( $eid = $this->insertGetId($data) ) {
			return $eid;
		} else {
			return false;
		}		
	}
	
	//操作作业 - 删除
	public function changeStatus($data){

		$id = $data['eid'];
		unset($data['eid']);
		
		$res = $this->where('eid', $id)
					->update($data);
				
		return $res;		
	}
	
	//查询这次作业的班级
	public function get_exam_uids($eid){
		
		$res = $this->where('eid', $eid)
					->select('uids')
					->first();
					
		if ($res) {
			return $res->toArray();
		} else {
			return true;
		}
		
	}
	
}