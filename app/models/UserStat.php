<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 * class UserStat - 班级 个人作业统计详情
 */
class UserStat extends Eloquent {
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_stat';
	public $timestamps = false;	
	
	//某次考试排名详情
	public function exam_rank($cid, $eid, $order, $model = 'DESC'){
		
		$where = array(
			'cid'=>$cid,
			'eid'=>$eid
		);
		
		$res = $this->leftJoin('user', 'user_stat.uid', '=', 'user.uid')
					->where($where)
					->select($order,'nickname')
					->orderBy($order,$model)
					->get();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}
		
	}
	
	
	
	
	
			
		
}