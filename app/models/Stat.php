<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 * class Stat - 统计
 */

class Stat extends Eloquent {
	
	protected $table = 'exam_stat';
	public $timestamps = false;
	
	//获取一套作业的详情
	public function get_item_detail($cid,$eid){
		
		$where = array(
			'cid'=>$cid,
			'eid'=>$eid
		);
		
		$res = $this->where($where)
					->select('item_detail')
					->first();
		
		if ($res) {
			return $res->toArray();
		} else {
			return true;
		}
	}
	
	
	//初始化 作业统计表
	public function insert_exam_stat($_stat){
		$res = $this->insert($_stat);
		return true;
	}
	
	//获取作业统计表 总条数
	public function get_examstat_count($time){
				
		$count = $this->where('tid', Session::get('tid'))
					  ->where('finishtime','<',$time)
					  ->count();
		
		return $count;
	}
	
	//获取最近考试情况列表
	public function get_exam_detail($time, $start, $pagesize){
		
		$tid = Session::get('tid');
		
		$res = $this->leftJoin('class', 'exam_stat.cid', '=', 'class.cid')
					->where('exam_stat.tid', $tid)
					->where('finishtime','<',$time)
					->select('finishtime','exam_name','cname','join_num','total_points','top_score','low_score')
					->skip($start)
					->take($pagesize)
					->get();
		
		if ($res) {
			return $res->toArray();		
		} else {
			return false;
		}
	}
	
	
	
	
	

}