<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 * class manger - 班级管理
 */

class Wx extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'wx';
	public $timestamps = false;
	
	//创建班级
	public function updateWxInfo($data) {
		
		//暂时先存入数据库，可以生成xml文件
		$arr = array(
			'account'=>$data['account'],
			'password'=>$data['password'],
			'nickname'=>$data['user_info']['nick_name'],
			'appId'=>$data['advanced_info']['app_id'],
			'appSecret'=>$data['app_key'],
			'fake_id'=>$data['user_info']['fake_id'],
			'is_bind'=>1,	
		);
		
		$this->where('id', 1)->update($arr);	
	}
	
	//查询绑定信息
	public function getWxInfo(){
		
		$res = $this->where('id',1)->select('nickname','is_bind','account','password','appSecret')->first();
		if ($res) {
			return $res->toArray();
		} else{
			//没有数据
			return false;
		}
	}
	
	//获取appid等
	public function get_app_info(){
		//先获取appid
		$app = $this->select('account', 'appId', 'appSecret')->first();
		if ($app) {
			return $app->toArray();
		}else {
			return false;
		}
	}
	
	//获取access_token
	public function get_access_token($app) {
		
		$url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$app['appId'].'&secret='.$app['appSecret'];
		$res = json_decode(Helpers\Helper::curl($url), true);
		if (!empty($res['access_token'])){
			return $res;				
		} else {
			return false;
		}
	}
	
	//获取永久二维码
	public function getQR($token, $cid) {
		
		$url = 'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token='.$token;
		$data['post'] = array(
			'type'=>'json',
			'action_name'=>"QR_LIMIT_SCENE",
			'action_info'=>array(
				'scene'=>array(
					'scene_id'=>$cid,
				)
			)			
		);
		
		$res = json_decode(Helpers\Helper::curl($url,$data), true);
		//file_put_contents('1.php', var_export($res,true));
		if (isset($res['ticket'])){
			return $res['ticket'];
		}else{
			return false;
		}
	}
}
