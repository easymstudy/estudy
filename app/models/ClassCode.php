<?php
class ClassCode extends Eloquent {
	
	protected $table = 'class_code';
	public $timestamps = false;
	
	//此班级下的老师数	
	public function count_teacher($cid){
		$count = $this->where("cid",$cid)->count();
		return $count;
	}
	
	//新增老师二维码记录
	public function insert_code($data){
		
		if ( $id = $this->insertGetId($data) ) {
			return $id;
		} else {
			return false;		
		}	
	}
	
	//获取老师二维码
	public function getCode($cid){
		
		$res = $this->leftJoin('class_teacher', 'class_code.tid', '=', 'class_teacher.tid')
					->where('class_code.cid',$cid)
					->select('qid', 'cid', 'class_code.tid','code_name','nickname','openid','status')
					->get();
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}		 
	}
	
	//更新老师二维码地址
	public function update_code($data){
		
		$qid = $data['qid'];
		unset($data['qid']);
		
		if ($this->where('qid', $qid)->update($data) ) {
			return true;
		}
		return false;		
	}
	
	//检测此二维码是否被绑定
	public function check_bind($qid){
		
		$res = $this->where('qid', $qid)->select('status')->first();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}
		
	}
	
}