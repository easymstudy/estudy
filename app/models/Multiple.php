<?php
class Multiple extends Eloquent {
	
	//普通题
	protected $table = 'multiple_choice';
	public $timestamps = false;
	
	//新增下一题
	public function createQuestion(){
	
		$data = array();
		if ( $mid = $this->insertGetId($data) ) {
			return $mid;
		} else {
			return false;
		}
	}
	
	//更新 title choices_list attribute_list 字段
	public function updateQuestion($data){
		
		//1.目前都没压缩  2.暂时先不做行内样式去除
		switch(trim($data['field'])){
			case 'title':
				$arr['title'] = addslashes($data['title']);
				break;
			case 'choices':
				$arr['choices_list'] = $data['choices'];
				break;
			case 'score':
				$arr['score'] = (int)$data['score'];
				break;	
			case 'attr':
				$arr['attribute_list'] = json_encode($data['attr']);
				break;
			case 'group_choice_id':
				$arr['group_choice_id'] = $data['gid'];
				break;
			case 'correct':
				$arr['correct'] = $data['correct'];
				break;						
		}
		$arr['update_time'] = $_SERVER["REQUEST_TIME"];
						
		if ( $this->where('mid', $data['mid'])->update($arr) ) {
			return true;
		} else{
			return false;
		}
	}
	
	//新增 title choices_list attribute_list 字段
	public function insertQuestion($data){
		
		//1.目前都没压缩  2.暂时先不做行内样式去除
		switch(trim($data['field'])){
			case 'title':
				$arr['title'] = addslashes($data['title']);
				break;
			case 'choices':
				$arr['choices_list'] = $data['choices'];
				break;
			case 'score':
				$arr['score'] = (int)$data['score'];
				break;
			case 'attr':
				$arr['attribute_list'] = json_encode($data['attr']);
				break;
			case 'correct':
				$arr['correct'] = $data['correct'];
				break;								
		}		
		
		$arr['update_time'] = $_SERVER["REQUEST_TIME"];
		
		//如果为复合题
		if ( $data['mid'] ) {
			$arr['group_choice_id'] = $data['mid'];					
		}

		if ( $mid = $this->insertGetId($arr) ) {
			return $mid;
		} else {
			return false;
		}
	}
	
	//查询题目
	public function selectQuestion($arr) {
		
		$res = $this->whereIn('mid', $arr)
					->select('mid', 'title', 'choices_list','attribute_list', 'correct','score','group_choice_id')
					->get();
		if ($res){
			$res = $res->toArray();
			return $res;			
		} else {
			exit('数据错误');
		}
	}
	
	//删除
	public function deleteQ($ids){
		
		$res = $this->whereIn('mid', $ids)
					->update(array('is_delete'=>1));
		
		if ($res) {
			return true;
		}
		return false;
	}
	
	
	//删除
	public function deleteGq($ids){
		
		$res = $this->whereIn('group_choice_id', $ids)
					->update(array('is_delete'=>1));
		
		if ($res) {
			return true;
		}
		return false;
		
		
	}	
}


