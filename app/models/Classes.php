<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 * class manger - 班级管理
 */

class Classes extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'class';
	public $timestamps = false;
	
	//删除班级
	public function delete_class($data){

		$ids = json_decode($data['ids'], true);
		unset($data['ids']);
		
		$res = $this->whereIn('cid', $ids)
					->update($data);
		return $res;					
	}
	
	//班级数
	public function class_count(){
		
		$where = array(
			'is_delete'=>0,
			'tid'=>Session::get('tid')
		);
		
		$count = $this->where($where)->count();
		return $count;		
	}
	
	//创建班级
	public function createClass() {

		$_data = Input::all();

		//过滤html标签
		$user = Cookie::get('user');
		$uid = Helpers\Helper::authcode($user['uid']);
		
		$data = array(
			'tid'=>(int)$uid,
			'introduce'=>strip_tags($_data['introduce']), 
			'topnum'=>(int)$_data['topnum'],
			'passcode'=>$_data['pass'], 
			'cname'=>str_replace(' ', '', $_data['name']),
			'create_time'=>$_SERVER["REQUEST_TIME"],
		);

		if ( $id = $this->insertGetId($data) ) {
			return $id;
		} else {
			return false;				
		}
	}
	
	//检测是否存在此班级(新增)
	public function checkClass($cid = ''){
		
		$_data = Input::all();
		$cname = str_replace(' ', '', $_data['name']);
		
		if (empty($cid)) {
			$count = $this->where('cname',$cname)->where('is_delete',0)->count();
		} else {
			$count = $this->where('cname',$cname)
						  ->where('is_delete',0)
						  ->where('cid','!=',$cid)	
						  ->count();
		}
		
		return $count;
		
	}
	
	//获取某些班级信息
	public function get_classes_info($cids , $arr = array('cid')){
		
		$res = $this->whereIn('cid', $cids)
					->select($arr)
					->get();
		
		if ($res){
			return $res->toArray();
		} else {
			return false;			
		}
		
	}
	
	
	//获取某个班级信息
	public function get_class_info($cid){
		
		$res = $this->where('cid', $cid)->select('cid', 'introduce', 'passcode', 'topnum', 'cname', 'rqcode', 'hasnum', 'is_delete')->first();
		if ( $res ){
			return $res->toArray();
		} else{
			return false;
		}
	}
	
	//获取所有班级名和id
	public function getAllClasses(){
		
		$res = $this->where('is_delete', 0)
					->select('cname','cid')
					->orderBy('create_time', 'desc')
					->get();
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}		
	}
	
	//获取班级详情(所属学生)
	public function getAllClassesDetail(){
		
		$where = array(
			'class.is_delete'=>0,
			'tid'=>Session::get('tid')
		);
		
		$res = $this->leftJoin('class_user as u', 'class.cid', '=', 'u.cid')
					->where($where)
					->orderBy('create_time', 'desc')
					->select('class.cid','class.cname','u.uids')
					->get();
		
		if ($res){
			return $res->toArray();
		} else {
			return false;	
		}
	}
	
	//获取布置过某套作业的班级详情(所属学生)
	public function get_class_detail($cid){
		$res = $this->leftJoin('class_user as u', 'class.cid', '=', 'u.cid')
					->whereIn('class.cid', $cid)
					->orderBy('create_time', 'desc')
					->select('class.cid','class.cname','u.uids')
					->get();
		
		if ($res){
			return $res->toArray();
		} else {
			return false;	
		}
	}
	
	
	
	//更新二维码地址
	public function update_code($name, $cid){
		
		if ($this->where('cid', $cid)->update(array('rqcode'=>$name))) {
			return true;
		}
		return false;
	}
	
	//获取班级列表
	public function get_class_list($limit){

		$where = array(
			'is_delete'=>0,
			'tid'=>Session::get('tid')
		);

		$res = $this->where($where)
					  ->select('cid', 'topnum', 'hasnum', 'passcode', 'cname')
					  ->orderBy('create_time', 'desc')
					  ->skip($limit['start'])
					  ->take($limit['pagesize'])
					  ->get();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}
	}
	
	//获取班级列表数量
	public function search_class_total($wd){

		$where = array(
			'is_delete'=>0,
			'tid'=>Session::get('tid')
		);
		
		$count = $this->where($where)
					  ->where('cname', 'like', "%{$wd}%")
					  ->count();
		
		return $count;
								
	}
	
	//获取班级列表搜索
	public function search_class_list($wd, $limit){

		$where = array(
			'is_delete'=>0,
			'tid'=>Session::get('tid')
		);

		$res = $this->where($where)
					->where('cname', 'like', "%{$wd}%")
					  ->select('cid', 'topnum', 'hasnum', 'passcode', 'cname')
					  ->orderBy('create_time', 'desc')
					  ->skip($limit['start'])
					  ->take($limit['pagesize'])
					  ->get();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}
	}
	
	
	//修改班级信息(安排作业)
	public function update_class($cid, $arr){
		
		$res = $this->where('cid', (int)$cid)->update($arr);
		if ($res) {
			return true;
		} else {
			return false;
		}
	}
	
	//修改某个班级信息
	public function update_class_info(){
		
		$_data = Input::all();
		
		$data = array(
			'introduce'=>strip_tags($_data['introduce']), 
			'topnum'=>(int)$_data['topnum'],
			'passcode'=>$_data['pass'], 
			'cname'=>strip_tags($_data['name']),
		);
		
		if ($this->where('cid', (int)$_data['cid'])->update($data)){
			return $_data['cid'];
		} else{
			return false;		
		}
	}
	
	//查看某班级动态
	public function get_class_defail($cid){
		
		$res = $this->where('class.cid', '=', $cid)
					->select('cid', 'cname', 'topnum', 'hasnum', 'passcode', 'examids', 'cname', 'rqcode')
					->first();
		if ($res) {
			return $res->toArray();		
		} else {
			return false;
		}
	}
	
	//查询此作业参与的班级
	public function get_examids($cids){
		
		$res = $this->whereIn('cid', $cids)
					->select('cid','examids')
					->get();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}
			
	}	
		
}
