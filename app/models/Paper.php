<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 * Paper manger - 试卷管理
 */

class Paper extends Eloquent {
	
	protected $table = 'paper';
	public $timestamps = false;
	
	//新增试卷
	public function insert_paper($_data){
		
		$data = array(
			'title'=>$_data['title'],
			'introduce'=>$_data['introduce'],
			'structs'=>$_data['structs'],
			'status'=>2,
			'is_delete'=>0,
			'word'=>$_data['word'],
			'update_time'=>$_SERVER["REQUEST_TIME"],
			'tid'=>$_data['tid']
		);
		
		if ($this->insert($data)) {
			return true;
		}
		
		return false;
		
	}
	
	
	//保存试卷的上传word文件
	public function save_doc(){
		
		$data = Input::all();
		$pid = (int)$data['pid'];
		unset($data['pid']);
		
		if ( $this->where('pid', $pid)->update($data) ){
			return true;
		} else {
			return false;
		}
	}
	
	//试卷总条数 (可用)
	public function paper_count(){
		//总条数
		$where = array(
			'is_delete'=>0,
			'tid'=>Session::get('tid')
		);
		
		$count = $this->where($where)->count();
		return $count;
	}
	
	//获取某套试卷的信息
	public function get_paper_detail($pid, $field = array()){
		
		$res = $this->where('pid', (int)$pid)->select($field)->first();
		if ($res) {
			return $res->toArray();
		}
	}
	
	//试卷列表数量(搜索)
	public function search_paper_total($wd){
		
		$where = array(
			'is_delete'=>0,
			'tid'=>Session::get('tid')
		);
		
		$count = $this->where($where)
					  ->where('title', 'like', "%{$wd}%")
					  ->count();
		
		return $count;
				
	}
	
	
	//试卷列表(搜索)
	public function search_paper_list($wd, $limit){
		
		//skip从第几开始   take取多少条 
		//试卷列表
		$where = array(
			'is_delete'=>0,
			'tid'=>Session::get('tid')
		);
		
		$list = $this->where($where)
					->where('title', 'like', "%{$wd}%")
					->select('pid','title','status','update_time')
					->orderBy('update_time', 'desc')
					->skip($limit['start'])
					->take($limit['pagesize'])
					->get();
					
		if ($list) {
			$list = $list->toArray();
			return $list;
		} else {
			return '';
		}			
			
	}
	
	//试卷列表
	public function paper_list($limit){
		
		//skip从第几开始   take取多少条 
		//试卷列表
		$where = array(
			'is_delete'=>0,
			'tid'=>Session::get('tid')
		);
		
		$list = $this->where($where)
					->select('pid','title','status','update_time')
					->orderBy('update_time', 'desc')
					->skip($limit['start'])
					->take($limit['pagesize'])
					->get();
					
		if ($list) {
			$list = $list->toArray();
			return $list;
		} else {
			return '';
		}	
	}
	
	//创建试卷-title
	public function create_paper(){
		
		$_data = Input::all();
		$data = array(
			'title'=> strip_tags($_data['pname']),
			'update_time'=>$_SERVER["REQUEST_TIME"],
			'tid'=>Session::get('tid')
		);
		
		if ( $pid = $this->insertGetId($data) ) {
			return $pid;
		} else {
			return false;		
		}
	}
	
	//更新试卷-title
	public function update_paper(){
		$_data = Input::all();
		$data = array(
			'title'=> strip_tags($_data['pname']),
			'update_time'=>$_SERVER["REQUEST_TIME"],
		);
		
		if ( $this->where('pid', (int)$_data['pid'])->update($data) ) {
			return true;
		} else {
			return false;
		}				
	}
	
	//获取试卷信息
	public function getPaperInfo($pid, $field=''){
		
		$res = $this->where('pid', (int)$pid)->select($field)->first();
		if ($res){
			$res = $res->toArray();
			return $res;			
		} else {
			return false;
		}
	}
	
	//获取多套试卷的structs 字段
	public function getMoreStructs($ids){
		
		$res = $this->whereIn("pid", $ids)->select("structs")->get();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;			
		}
		
	}
	
	
	//获取试卷题目ids
	public function getStructs($pid){
		$res = $this->where('pid', (int)$pid)->select('structs')->first();
		
		if ($res){
			$res = $res->toArray();
			if (!empty($res['structs'])){
				$structs = json_decode( gzdecode(base64_decode($res['structs'])),true);
				return $structs;			
			}
		} else {
			return false;
		}
	}
	
	//更新Structs 字段 直接更新
	public function update_structs($pid, $arr){
		
		$data['structs'] = json_encode($arr);
		
		if ( $this->where('pid', (int)$pid)->update($data) ){
			return true;
		} else {
			return false;
		}
	}
	
	
	//更新Structs 字段 先取出后更新
	public function updateStructs($data){
		
		//先取出对应structs 字段
		$res = $this->where('pid', (int)$data['pid'])->select('structs')->first();
		if ($res){
			$res = $res->toArray();
			$structs = json_decode($res['structs'],true);
			$structs[] = array(
				'id'=>$data['mid'],
				'type'=>$data['type'],
			);
			
			$arr = array(
				'structs'=>json_encode($structs),
			);
			
			if ( $this->where('pid', (int)$data['pid'])->update($arr) ){
				return true;
			} else {
				return false;			
			}	
		} else {
			return false;	
		}
	}
	
	//改变status 字段状态 1未审核 2已审核
	public function changeStatus($data){
		
		$ids = json_decode($data['ids'], true);
		unset($data['ids']);
		
		$res = $this->whereIn('pid', $ids)
					->update($data);
				
		return $res;					
	}
	
	//获取试卷的试卷名
	public function getAllPaper( $pid = ''){
		
		$where = array(
			'is_delete'=>0,
			'status'=>2,
			'tid'=>Session::get('tid')
		);
		
		$res = '';
				
		if (empty($pid)){
			//没有指定id， 所有试卷
			$res = $this->where($where)
						->select()
						->orderBy('update_time', 'desc')
						->get();
		} else {
			//某一套试卷, title字段
			$where['pid'] = (int)$pid;
			unset($where['status']);
			$res = $this->where($where)
						->select('title')
						->first();		
		}
					
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}
	}
	
	
		
}