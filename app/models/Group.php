<?php
class Group extends Eloquent {
	
	//复合题
	protected $table = 'group_choice';
	public $timestamps = false;
	
	//更新Group
	public function updateGroup($data){
		
		switch(trim($data['field'])){
			
			case 'material':
				$arr['material'] = addslashes($data['material']);
				break;
			case 'multiple_choice_ids':
				//取出multiple_choice_ids 字段
				$res = $this->where('gid', $data['mid'])->select('multiple_choice_ids')->first(); 
				if ($res){
					$res = $res->toArray();
					$multiple_choice_ids = json_decode($res['multiple_choice_ids'],true);
					$multiple_choice_ids[] = $data['qid'];
					
					$arr['multiple_choice_ids'] = json_encode($multiple_choice_ids);
										
				} else {
					return false;
				}
				break;
		}
		$arr['update_time'] = $_SERVER["REQUEST_TIME"];
		if ( $this->where('gid', $data['mid'])->update($arr) ){
			return true;		
		} else {
			return false;
		}
	}
	
	
	//新增一条复合题
	public function insertGroup($data){
		
		switch(trim($data['field'])){
			
			case 'material':
				$arr['material'] = addslashes($data['material']);
				break;
			case 'multiple_choice_ids':
				$arr['multiple_choice_ids'][] =  $data['mid']; 
				$arr['multiple_choice_ids'] = json_encode($arr['multiple_choice_ids']); 
				break;				
		}
		$arr['update_time'] = $_SERVER["REQUEST_TIME"];
		// 此mid 为复合题目id
		if ( $mid = $this->insertGetId($arr) ) {
			return $mid;			
		}else {
			return false;
		}
		
	}
	
	//查询复合题
	public function getMids($gc){
		
		$res = $this->whereIn('gid', $gc)
					->select('gid', 'multiple_choice_ids', 'material')
					->get();
		if ($res){
			$res = $res->toArray();
			return $res;			
		} else{
			return false;
		}		
	}
	
	
}