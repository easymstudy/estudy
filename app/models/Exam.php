<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 * class exam record - 班级 作业记录
 */

class Exam extends Eloquent {
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'exam';
	public $timestamps = false;	
	
	
	//查询某班级的作业记录
	public function select_class_exam($cid,$ids){
					
		$res = $this->leftJoin('exam_stat', 'exam.eid', '=', 'exam_stat.eid')
					->where('cid', $cid)
					->whereIn('exam.eid', $ids)
					->select('exam.eid','title','startime','cid','endtime','duration','join_num')
					->orderBy('exam.eid','desc')
					->get();	

		if ($res){
			return $res->toArray();
		} else{
			return false;
		}			
	}
	
	//查询某作业的信息
	public function select_exam_info($eid){
		
		$where = array(
			'exam.eid'=>$eid,
			'exam.is_delete'=>0
		);

		$res = $this->leftJoin('paper', 'exam.pid', '=', 'paper.pid')
					->where($where)
					->select('eid', 'paper.title', 'startime', 'endtime')
					->first();
		
		if ($res) {
			return $res->toArray();
		} else {
			return false;
		}		
		
	}
	
	//查询某老师是否布置过作业
	public function is_make_exam($tid){
		
		$count = $this->where('tid',$tid)->count();
		return $count;
			
	}
	
	
}