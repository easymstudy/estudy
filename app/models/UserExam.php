<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserController
 *
 * @author zhaochong
 * class UserExam - 班级 个人作业统计详情
 */
class UserExam extends Eloquent {
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_exam';
	public $timestamps = false;	
	
	//查询某班级下某人的记录 (已经提交)
	public function user_exam_info($cid, $uid, $data= array('ueid','cid','eid','exam_submit','right_num','endtime','score')){
			
			//条件查询
			$where = array(
				'cid'=>$cid,
				'uid'=>$uid,
			);
		
			$res = $this->where($where)
						->where('is_submit', '=', 1)
						->select($data)
						->orderBy('endtime','desc')
						->get();
			
			if ($res) {
				return $res->toArray();
			} else {
				return false;
			}
	}
	
	//查询某组卷记录
	public function user_submit_detail($ueid){
		
		$res = $this->where('ueid', $ueid)
					->select('exam_submit')
					->first();
		
		if ($res) {
			return $res;
		} else {
			return false;
		}			
		
	}
}