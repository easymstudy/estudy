(function($, w){
	
	w.form = {
		'pname' : $('input[name=paper-name]'),
		'paperId':$("#paperId").val(),
		'total':$('input[name=total]').val()
    };
	
	w.tool = {
		'ajax_post' : function(url, data, callback){
			$.post(url, data, callback);			
		}		
	};
	
	//创建试卷
	$('.create-paper-button').on('click', function(){

		var url = location.href,
			arr = url.split('/'),
			paper_name = w.form.pname.val();
		
		if ( paper_name == ''){
			alert('试卷名不能为空');
			return ;	
		}
		
		if ( /\d+/.test(arr[arr.length-1]) ){
			//如果有pid直接 为更新操作
			var pid = arr[arr.length-1];
			
			tool.ajax_post(base_url+'/updatePaper', {'pname' : paper_name, 'pid':pid}, function(res){
				
					if (res.status) {
						location.href = base_url+'/enterPaper/'+pid;
					} else {
						alert(res.info);
					}
			});
			
		} else {
			//新增操作
			
			tool.ajax_post(base_url+'/createPaper', {'pname' : paper_name}, function(res){
					
					if (res.status == 1) {
						location.href = base_url+'/enterPaper/'+res.pid;
					} else {
						alert(res.info);				
					}
			});
		}
	});
	
	/*******试卷预览页面*******/
	//点击题序滑动
	$(".paper-preview-serial li").on("click", function(){
		var _this = $(this);
		var index = _this.index(),
			$detail = $('.paper-preview-detail'),
			$dt = $detail.find("dt:lt("+index+")");
		
		//获取高度	
		var dt_top = 0;	
		if ($dt.size()) {
			$.each($dt, function(k){
				var _top = ($(this).height() + 52);
				dt_top = dt_top + _top;
			});
		}	
				
		//给滚动条高度赋值
		$('.paper-preview-detail').animate({scrollTop:dt_top});	
	});
	
	/**********试卷列表页面**********/
	//前一页
	$(".pre-page").on("click", function(){
		
		//获取分页的情况
		var pagination = $(".pagination-info").val(),
		pagination = pagination.split('/');
		if (pagination[0] == 1) 
			return ;
		
		var p = pagination[0] - 1;

		//跳转页面
		location.href = base_url+'/paperlist/'+p;
	});
	
	//后一页
	$(".next-page").on("click", function(){
		//获取分页的情况
		var pagination = $(".pagination-info").val(),
			pagination = pagination.split('/');	
		
		if ( pagination[0] == form.total)
			return ;

		var p = parseInt(pagination[0]) + 1;
		
		//跳转页面
		location.href = base_url+'/paperlist/'+p;
					
	});
	
	//全选和全不选
	$(".allcheck").on("click", function(){
		
		var _this = $(this);
		if ( _this.hasClass('checked') ){
			
			//已选中状态， 让所有全不选
			var img_url = base_url+'/admin/images/check.png';
			_this.removeClass('checked').attr('src', img_url);
			$(".paper-list-number img").removeClass('checked').attr('src', img_url);
			
			//取消全选后  可以完成复制，导入，输出页面
					
		} else {
			var img_url =  base_url+'/admin/images/checking.png';
			//未选中状态， 让所有全选
			_this.addClass('checked').attr('src', img_url);
			$(".paper-list-number img").addClass('checked').attr('src', img_url);
			
			//全选后 无法完成复制，导入，输出页面
			
		}
	});
	
	//勾选
	$(".paper-list-number img").on("click", function(){
		
		var _this = $(this);
		if ( _this.hasClass('checked') ){
			var img_url = base_url+'/admin/images/check.png';
			_this.removeClass('checked').attr('src', img_url);	
		} else {
			var img_url =  base_url+'/admin/images/checking.png';
			_this.addClass('checked').attr('src', img_url);	
		}
	});
	
	//审核通过
	$(".pass-paper").on("click", function(){
		
		var ids = getCheckIds();
		if (!ids) {
			alert('请至少选择一套试卷');
			return ;
		}	
		
		var data = {
			'ids' : JSON.stringify(ids),
			'status': 2
		};
		
		var $status = $("table.class-list-table").find('img.checked').parents('tr').find('.status');
		
		tool.ajax_post(base_url+'/papercheck', data, function(res){
				
				if (res.status){
					alert(res.info);
					location.reload();
					//$status.html('<span class="ischeck">☆已审核</span>');
				} else{
					alert(res.info);
				}
		});		
	});
	
	//审核不通过
	$(".nopass-paper").on("click", function(){
		
		var ids = getCheckIds();
		if (!ids) {
			alert('请至少选择一套试卷');
			return ;
		}
		
		var data = {
			'ids' : JSON.stringify(ids),
			'status': 1
		};
		
		var $status = $("table.class-list-table").find('img.checked').parents('tr').find('.status');
		
		tool.ajax_post(base_url+'/papercheck', data, function(res){
				
				if (res.status){
					alert(res.info);
					location.reload();
					//$status.html('未审核');	
				} else{
					alert(res.info);
				}
		});		
	});
	
	//删除 - 只是改变状态， 假删除
	$(".delete-paper").on("click", function(){
		
		var ids = getCheckIds();
		if (!ids) {
			alert('请至少选择一套试卷');
			return ;
		}

		if (window.confirm("确定要删除试卷吗?")) {


			var data = {
				ids : JSON.stringify(ids),
				is_delete : 1
			};		
			
			var $status = $("table.class-list-table").find('img.checked').parents('tr');
			
			tool.ajax_post(base_url+'/paperdelete', data, function(res){
					if (res.status){
						alert(res.info);
						//刷新整个页面
						window.location.reload();
						//$status.remove();
					} else{
						alert(res.info);
					}				
			});
		}
		
	});
	
	//获取选中的paper id集合
	function getCheckIds(){
		//获取勾选的id
		var ids = [];
		var $img_checked = $("table.class-list-table").find('img.checked');
		var l = $img_checked.size();
		
		if (l == 0) {
			return false;
		}
		
		$img_checked.each(function(){
			var id = $(this).parents('tr').attr('data-pid');
			ids.push(id);
		});
		return ids;
	}
	
	//返回上一步
	$(".mtk-paper-back").on("click", function(){
			window.location.href = base_url+'/createPaper/'+form.paperId;															
	});
	
	//排序模式
	$(".mtk-paper-model").on("click", function(){
		
		var $dt = $("dt");
		if ($dt.size()) {
			$.each($dt, function(k,v){
				
				var _this = $(this);
				
				var text = _this.find('.mtk-item-tm').text();
					text = text.substring(0,10),
					$div = _this.find(".mtk-item-slide-noa");
				
				_this.find('.mtk-item-slide-txt').text(text);
				$div.removeClass('mtk-item-slide-noa').addClass('mtk-item-slide-a');
				_this.addClass('mtk-item-slide');
				
			});
		}
		$("dl").removeClass('ui-sortable-disabled');
		
	});
	
	
	//搜索试卷列表
	$(".search-paper-list").on('click', function(){
		var wd = $(this).parent('.search').find('.form-control').val();
		window.location = base_url+'/paperlist/1/'+encodeURIComponent(wd);
	});
	

	/**************************预览试卷操作******************************/
	$(".mtk-paper-preview").on("click", function(){
		
		/*
		//预览试卷判断最后一题是否有无正确答案
		var l = $("dt").size();
		
		if (l) {
			var $dt = $("dt:last"),
				type = $dt.attr('data-type');
			
			switch (parseInt(type)) {
				case 0:
					var score = $dt.find('.mtk-item-score input').val(),
						$correct = $dt.find('.mtk-c1 .correct');
					break;
				case 1:
					var $mtkQuestion = $dt.find('.mtk-question:last'),
						score = $mtkQuestion.find('.mtk-item-score input').val(),
						$correct = $mtkQuestion.find('.mtk-c1 .correct');
					break;
			}
			
			if (score==''){
				alert('请填写题目分数');
				//$dt.find('.mtk-item-score input').focus();
				return ;						
			}
			
			if ($correct.size() == 0){
				alert('请勾选正确答案');
				return ;
			}			
		}
		*/
		
		/*检测是否有题目没有填分数和正确答案*/
		var $dl = $("dl"),
			$dt = $("dt"),
			flag = false;

		$dl.addClass("ui-sortable-disabled");

		if ($dt.size()) {
			
			//展开
			$dt.each(function(){
				
				if (flag)
					return false;			
								
				var _this = $(this),
					type = _this.attr('data-type'),
					_height = _this.offset().top;
					index = (_this.index() + 1);
					
				//展开
				_this.removeClass("mtk-item-slide");
				_this.find(".mtk-item-slide-a").removeClass("mtk-item-slide-a").addClass("mtk-item-slide-noa");
				
				/**验证**/
				//0.判断题目类型 , 复合题 或者 普通题
				switch(parseInt(type)){
					case 0:
						var score = _this.find('.mtk-item-score input').val(),
							$correct = _this.find('.mtk-c1 .correct');
							

						//1.先验证分数
						if (score===''){
							flag = true;
							alert('请填写第'+index+'题分数');
							$('body,html').animate({'scrollTop':_height}, 800);
							_this.find(".mtk-item-score").addClass("checkscore");
							return false;  // break , 	return true continue
						}
						
						//2.后验证正确选项
						if ($correct.size() == 0){
							flag = true;
							alert('请勾选第'+index+'题正确答案');				
							$('body,html').animate({'scrollTop':_height}, 800);
							return false;	// break , 	return true continue
						}
						break;
					case 1:
						//复合题验证
						var $mtkQuestion = _this.find('.mtk-question');
							
						if ($mtkQuestion.size()) {

							$mtkQuestion.each(function(k){
								var score = $(this).find('.mtk-item-score input').val(),
									$correct = $(this).find('.mtk-c1 .correct'),
									_height = $(this).offset().top,
									no = index+'题第'+(parseInt(k)+1); 
									
								if (score===''){
									flag = true;
									alert('请填写第'+no+'子题分数');
									$('body,html').animate({'scrollTop':_height}, 800);
									$(this).find(".mtk-item-score").addClass("checkscore");
									return false;  // break , 	return true continue
								}
								
								if ($correct.size() == 0){
									flag = true;
									alert('请勾选第'+no+'子题正确答案');					
									$('body,html').animate({'scrollTop':_height}, 800);
									return false;	
								}
							});		
						}	
						break;
				}
			});
		}
		
		//开关
		if (flag)
			return ;
		
		//给预览试卷一个效果
		$("#redactor_modal_overlay").show();
		$("#redactor_modal_overlay .loading-info").html('loading');
		//先简单处理 ,3秒后执行
		setTimeout(function(){
				//给一个保存的提示 ,效果
				location.href = base_url+'/paperpreview/'+form.paperId;
		},3000);
		
	});
	
	
	
	
	
	

})(jQuery, window);