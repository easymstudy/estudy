(function($, w){

    w.form = {
        'user' : $('input[name=user]'),
        'pass' : $('input[name=pass]'),
        'repass' : $('input[name=repass]'),
		'code' : $('input.code') 
    };
    
    //注册操作
    $('.register', '.user-form').on('click', function(){
        
        var data = {
            user: form.user.val(),
            pass : form.pass.val(),
            repass : form.repass.val()
        };
		
		var pattern = {
			email : /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i,
			pass : /\w+/
		};
		
		if ( !pattern.email.test(form.user.val()) ){
			//alert('用户名不合法!');
			$(".user-form i").eq(0).html('请输入正确的邮箱地址，完成注册!');
			$(".account").addClass('tis_');
			return ;
		}
		
		
		if ( data.pass.length < 6 ){
			$(".tis").eq(1).html("密码长度不足6个字符!");
			$(".rpass").addClass('tis_');
			return 
		}
		
		if (data.pass != data.repass) {
			$(".tis").eq(2).html("两次密码不一致!");
			$(".repass").addClass("tis_");			
			return ;
		}
		
		$.post('register', data, function(res){
			
			if (res.status) {
				window.location.href = 'activation';
			} else {
				
				var str = res.info;
				
				if (str.indexOf("帐号") != -1) {
					$(".tis").eq(0).html("此帐号已被注册过!");
					$(".account").addClass("tis_");			
					return ;
				}
			}
		});
		
    });
    
	//帐号
    $(".account").focus(function(){
		$(".account").removeClass('tis_');
		$(".user-form i").eq(0).html('');
		
	});
	
	$(".rpass").focus(function(){
		$(".rpass").removeClass('tis_');
		$(".user-form i").eq(1).html('');	});
	
	$(".repass").focus(function(){
		$(".repass").removeClass('tis_');
		$(".user-form i").eq(1).html('');
	});
	
	//验证码
	$(".code").focus(function(){
		$(".code").removeClass('tis_');
		$(".user-form i").eq(2).html('');
	});
	
	
    //登录操作
    $('.login', '.user-form').on('click', function(){
		//默认保持帐号登录
        var data = {
            user : form.user.val(),
            pass : form.pass.val(),
            code : form.code.val(),
			remember : Number($("#remember").is(":checked"))
        };		
				
		var pattern = {
			email : /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/i
		};
		
		if ( !pattern.email.test(form.user.val()) ){
			//alert('用户名不合法!');
			$(".user-form i").eq(0).html('请输入正确的邮箱地址!');
			$(".account").addClass('tis_');
			return ;
		}		
			
		$.post('login', data, function(res){

			if (res.status == 1) {
				window.location.href = 'index';
			} else if (res.status == 2){
				//没有激活
				window.location.href = 'activation/1';
			} else {

				$("#code").trigger("click");
				
				var str = res.info;
								
				if (str.indexOf('验证码') != -1) {
					$(".code").addClass('tis_');
					$(".user-form i").eq(2).html("验证码错误");
					return ;
				}

				if (str.indexOf('帐号') != -1 || str.indexOf('密码') != -1) {
					$(".account").addClass('tis_');
					$(".user-form i").eq(0).html("帐号或密码错误");
					return ;				
				}
				//window.location.reload();
			}
						
		});
		//}
    });
	
	//刷新验证码(登录)
	$("#code").on("click", function(){
		var _this = $(this);			
		_this.attr('src', base_url+'/code/'+Math.random());
		
	});
	
	//刷新验证码(忘记密码)
	$("#getimg").on("click", function(){
		var _this = $(this);			
		_this.attr('src', base_url+'/getimg/'+Math.random());
		
	});
	
	$(document).keydown(function(e){
		if ( e.keyCode == 13 ) {
			
			if (form.code.size() ){
				$('.login', '.user-form').trigger('click');
			} else {
				$('.register', '.user-form').trigger('click');
			}
			
		}
	});
	
	/***注册激活***/
	$(".activation").on('click',function(){
		
		var _this = $(this);
		_this.off('click');
		
		var data =  {
			sid : $('.sid').val()
		}
		
		$.post(base_url+'/activationMail' , data ,function(res){
			
			if (res) {
				//发送成功,跳转
				window.location.href = base_url+'/mailok';
			} else {
				//发送失败
				_this.bind('click');
			}
			
		});
			
	});
	
	/*****发送邮件*****/
	$(".sendmail").on('click', function(){
		
		var _this = $(this);
		_this.off('click').css({backgroundColor:'#333',cursor:'default'});
		
		var data = {
			username:$('.email').val(),
			code:$('.code').val()			
		}
		
		$.post(base_url+'/sendmail' ,data ,function(res){
			
			switch (parseInt(res.status)) {
				case -1://验证码错误
					alert(res.info);
					break;
				case -2://邮箱错误
					alert(res.info);
					break;
				case 0: //发送失败
										
					break;
				case 1: //发送成功
					window.location.href = base_url+'/sendok'
					break;
			}
						
			_this.bind('click').css({backgroundColor:'#009245',cursor:'pointer'});;
			$("#getimg").trigger('click');			
		});						
	});
	
	//修改密码
	$(".fixpwd").click(function(){
		//2个密码不一致
		if ($('.newpwd').val() != $('.r_newpwd').val()) {
			alert('输入的两次密码不一致');			
			return false;
		}
	});
	
	
})(jQuery, window);