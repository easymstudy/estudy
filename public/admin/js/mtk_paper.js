﻿//顺序
;(function($){
	
	//重置题目序号
	$.fn.mtkResetItemNum=function(){
		var mtk_item_num=1;
		
		$(".mtk-item-slide-tit").each(function(){
			
			var _this = $(this);
			_this.text(""+mtk_item_num+".");
			
			//如果是复合题，还要改变子题序号
			var type = _this.parents('dt').attr('data-type');

			if (type == 1){
					
				var $subquestion = _this.parents('dt').find('.mtk-item-num-txt');
				
				if ($subquestion.size()){
					
					var num = 1;
					$subquestion.each(function(){
						no = mtk_item_num +'-'+ num;
						$(this).html(no);			
						num++;		
					});
				}
			}	
			mtk_item_num++;
		});
	};
	
	$.fn.mtkResetItemOption=function(){
		$(".mtk-choice").each(function(){
			var i=0;
			$(this).find(".mtk-sel-letter").each(function(){
				switch (i){
				case 0:
				  $(this).text("A");
				  break;
				case 1:
				  $(this).text("B");
				  break;
				case 2:
				  $(this).text("C");
				  break;
				case 3:
				  $(this).text("D");
				  break;
				case 4:
				  $(this).text("E");
				  break;
				case 5:
				  $(this).text("F");
				  break;
				case 6:
				  $(this).text("G");
				  break;
				case 7:
				  $(this).text("H");
				  break;
				case 8:
				  $(this).text("I");
				  break;
				case 9:
				  $(this).text("J");
				  break;
				case 10:
				  $(this).text("K");
				  break;
				case 11:
				  $(this).text("L");
				  break;
				case 12:
				  $(this).text("M");
				  break;
				default:
				  i=0;
				}
				i++;
			});
		});
	};
	
	
	$.fn.setOrderFu = function(){
		
		var $mtk_question = this.nextAll();
		
		$mtk_question.each(function(){
			var _this = $(this);
			var $numtxt =  _this.find('.mtk-item-num-txt');
			var order_number = $numtxt.text();
			var arr = order_number.split('-');
			$numtxt.text(arr[0]+'-'+(parseInt(arr[1]-1)));			
		})
	};
			
})(jQuery);

//上传word文档操作
(function($, w, plupload){
	
	var oPlupload = {
						
		_savedoc : function(data){
			
			var _data = {
				pid : $("#paperId").val(),
				word: data.file	
			};
			
			$.post( base_url+'/savedoc', _data, function(res){
					var $oImport = $(".import-word1").parent('div');					
					//if (res.status) {
						//显示编辑器 div
						$("#redactor_modal_overlay").hide();
						$oImport.hide().next().show();
						//把内容放入 编辑器中
						oPlupload._scrollEditor();					
						w.ue.setContent(data.data);

					//} else {
					//	$("#redactor_modal_overlay").hide();
					//} 
			}).fail(function(){
				//失败
				$("#redactor_modal_overlay").hide();
			});
					
		},
		
		_scrollEditor : function(){
			
			if ( $("#pickfiles2").parent('div').is(':hidden') )
				return ;	
			
			var $editor = $(".mtk-import"),
				_height = $editor.offset().top;
			$(window).scroll(function(){
				//position:fixed;
				scrollTop = $(this).scrollTop();
												
				if ( scrollTop > _height ){
					$editor.css({position:"fixed",top: 0});			
				} else {
					$editor.css({position:"static"});
				}
			
			});
		},
		
		_import : function(){

			var uploader = new plupload.Uploader({
				
				runtimes : 'html5,flash,silverlight,html4',
				browse_button : ['pickfiles1','pickfiles2'],
				//container: document.getElementById('container'), // ... or DOM Element itself
				url : w.uploadConfig.url,
				flash_swf_url : w.uploadConfig.flash_swf_url,
				silverlight_xap_url :  w.uploadConfig.silverlight_xap_url,			

				filters : {
					max_file_size : '10mb',
					mime_types: [
						{title : "Word files", extensions : "doc,docx"}
					]
				},

				init: {
					
					//添加后 立即开始上传
					FilesAdded: function(up, files) {
						//上传效果
						$("#mtk-paper-btn-li").addClass("displaynone")
						$("#redactor_modal_overlay").show();
						$("#redactor_modal_overlay .loading-info").html('导入中');
						uploader.start();
						return false;
					},
					
					//显示进度			
					UploadProgress: function(up, file) {
						$("#redactor_modal_overlay .loading-info").html(file.percent+'%');
					},
					
					//上传完成					
					FileUploaded : function(uploader,file,responseObject){
						
						var oRes = JSON.parse(responseObject.response);
						if (oRes.code == 1) {
							
							var requestUrl = w.uploadConfig.getHtml+oRes.data.hashname;

							//导入成功,通过hashname 获取 word的str
							$.get( requestUrl, '', function(res){

									var data = {
										data : res,
										file : oRes.data.hashname
									};
									oPlupload._savedoc(data);	
																		
							}).fail(function(){
								$("#redactor_modal_overlay .loading-info").html('获取失败');
								$("#redactor_modal_overlay").hide();
							});
							
						} else {
								$("#redactor_modal_overlay .loading-info").html('获取失败');
								$("#redactor_modal_overlay").hide();
						}
					},
					
					//错误
					Error: function(up, err) {
							$("#redactor_modal_overlay .loading-info").html('获取失败');
							$("#redactor_modal_overlay").hide();
					}
				}
			});// _import end
			uploader.init();
		}			
	};
	
	//上传初始化
	oPlupload._import();
	//编辑器滑动
	oPlupload._scrollEditor();
	
})(jQuery, window, plupload);



//及时保存试卷 
(function($, w, io){
	
	var socket = {
		
		paper : io.connect('http://112.124.50.14:9999/paper'),//io.connect('http://192.168.1.14:8080/paper'),
		/****通信****/
		//保存 socket.io 触发事件
		_save : function(data, callback){
			
			data.tid = parseInt($("#teacher").val());
			
			data.handle = 'save_question';
			data.callback_handle = 'save_status';
			//socket.paper.emit('save_question', data);
			socket.paper.emit('paper_handle', data);
			socket.paper.on("save_status", callback);
		},
		
		//删除试题
		_delete : function(data, callback){
			data.handle = 'del_question';
			data.callback_handle = 'del_status';			
			socket.paper.emit('paper_handle', data);
			//socket.paper.emit('del_question', data);
			socket.paper.on("del_status", callback);
		},
		
		//拖动选项
		_sortChoice : function(data, callback){
			data.handle = 'sort_choice';
			data.callback_handle = 'sort_choice_status';
			socket.paper.emit('paper_handle', data);			
			//socket.paper.emit('sort_choice', data);
			socket.paper.on('sort_choice_status', callback);
		},
		
		//拖动题目
		_sortQuestion : function(data, callback){
			data.handle = 'sort_question';
			data.callback_handle = 'sort_question_status';
			socket.paper.emit('paper_handle', data);			
			//socket.paper.emit('sort_question', data);
			socket.paper.on('sort_question_status', callback);
		},
		
		_remove : function(str){
			socket.paper.removeListener(str);
		},
	};
	
	
	var $element = {
		dl : $('.mtk-item-dl'),
		common : $('.mtk-add-new-common'),
		composite : $('.mtk-add-new-composite'),
		imgurl : base_url+'/admin/images/mtk-99.png',
		//拖动和点击 控制开关
		flag : true
	};
	
	// 填入的信息
	var papersdetail = {
		score : '',
		editDiv : '',
		attr : '',
		dropDiv : ''
	};
	
	//事件操作
	var mtk_paper = {
			
		//初始化方法
		initialize : function(){
			this.delQuestion();
			this.showdel();	
			this.addCommon();
			this.addComposite();
			this.addChoice();
			this.addAnalysis();
			this.addSubQuestion();
			this.showQuestion();
			this.delComposite();
			this.clickEditDiv();
			this.leaveEditDiv();
			
			//需要初始化的保存
			this.saveScore();
			this.saveAttrInput();
			//删除选项
			this.delChoice();
			//保存正确答案
			this.savecorrect();
			/****拖动******/
			//拖动选项
			this.sortable_choice();
			//拖动题目
			this.sortable_question();
			
			//置顶
			this.setTop();
			
		},
		
		
		/*******有数据交互的操作*******/
		
		//保存正确答案
		savecorrect : function(){

			$element.dl.on("click", ".mtk-c1 img", function(){
					
					var _this = $(this),
						$dt = _this.parents("dt"),
						type = $dt.attr('data-type'),
						$answer = _this.parents('.mtk-choice').find(".mtk-c1 img"),
						$li = _this.parents('li'),
						$ul = _this.parents('ul'),
						index = $ul.find('.mtk-c1 img').index(_this);

					//判断内容
					var value = $li.find('.edit-div').html();
					if (value=='')
						return ;
					
					if ( _this.hasClass('correct') ){
						_this.removeClass('correct').attr({'src': base_url+'/admin/images/mtk-99.png', 'title':'点击设置为正确答案'});
					} else {
						_this.addClass('correct').attr({'src': base_url+'/admin/images/mtk-02.png', 'title':'点击切换'});
					}

					//数据交互					
					var correct = [];
					$answer.each(function(k){
						if ( $(this).hasClass('correct') ){
							correct.push(k);
						}								
					});

					switch( parseInt(type) ){
						case 0:
							var mid = $dt.attr('data-id');
							break;
						case 1:
							var $question = _this.parents('.mtk-question'),
								mid = $question.attr('data-qid'),
								qid = $dt.attr('data-id');
							break;
					}
					
					var data = {
						//correct:JSON.stringify(correct),
						'correct':correct,
						mid: mid == undefined ? '' : mid,
						type: type,
						pid:$("#paperId").val(),
						qid:qid == undefined ? '' : qid,
						field:'correct'
					};
					
					var callback = function(res){
						
						if (res.status){
														
						} else {
							//失败							
							alert(res.info);
							//还原操作
							if ( _this.hasClass('correct') ){
								_this.removeClass('correct').attr({'src': base_url+'/admin/images/mtk-99.png', 'title':'点击设置为正确答案'});
							} else {
								_this.addClass('correct').attr({'src': base_url+'/admin/images/mtk-02.png', 'title':'点击切换'});
							}					
							
						}
						//删除监听的某个事件
						socket._remove('save_status');
					};									
					socket._save(data, callback);				
			});
											
		},
		
		//删除题目 是可以全部删掉的
		delQuestion : function(){
			
			$element.dl.on("click", ".mtk-del-item", function(){

					//判断此题是否存在id
					var _this = $(this),
						$dt = _this.parents('dt'),
						type = $dt.attr('data-type'),
						id = $dt.attr('data-id');	
					
					if ( id == undefined ) {
						//不存在试题id, 直接删除dom
						$dt.remove();
						$().mtkResetItemNum();					
					} else {
						//通过请教接口返回值操作
						switch(parseInt(type)){
							case 0:
								var mid = id;
								break;
							case 1:
								var qid = id;
								break;
						}
						
						var data = {
							type : type,
							mid : mid == undefined ? '' : mid,
							qid : qid == undefined ? '' : qid,
							pid:$("#paperId").val(),
						};

						var callback = function(res){
							
							if (res.status){
								$dt.remove().mtkResetItemNum();								
							} else {
								alert(res.info);
							}
							//删除监听的某个事件
							socket._remove('del_status');
						}
							
						socket._delete(data, callback);
					}
			});
			
		},

		//删除选项或者属性 必须要留一个选项
		delChoice : function(){
			
			$element.dl.on("click", ".mtk-sel-del", function(){
					
					//1.获取此删除按钮的父级ul
					var _this = $(this),
						$ul = _this.parents('ul'),
						$li = $ul.find('li'),
						del_attr = _this.attr('del-attr'),
						l = $li.size();
					
					switch (del_attr) {
						case 'choice':
							if ( l < 2 ) {
								alert('至少要保留一个选项');
								return ;
							}
							break;
						case 'attr':
							break;					
					}

					//重组数据
					//先给一个删除的状态， 取值时不会取道
					//要删除的li
					var $del_li = _this.parents('li'),
						$dt = _this.parents('dt'),
						type = $dt.attr('data-type');
					$del_li.addClass("is_delete"); //临时增加一个删除标识
					
					//通过类型判断 是普通题还是复合题
					switch(parseInt(type)){
						case 0:
							var mid = $dt.attr('data-id');
							break;
						case 1:
							var $question = _this.parents('.mtk-question'),
								mid = $question.attr('data-qid'),
								qid = $dt.attr('data-id');
							break;				
					}
					
					//判断此题是否存在id, 没有直接删除
					if (!mid) {
						$del_li.remove().mtkResetItemOption();	
						return ;			
					}
					

					//定义数据
					var data = {
						'mid': mid == undefined ? '' : mid,
						'type': type,
						'pid':$("#paperId").val(),
						'qid': qid == undefined ? '' : qid
					};

					//通过属性判断是 选项还是解析
					switch (del_attr){
						case 'choice':
							//初始化选项					
							var choice = [];
							$li.each(function(){
								var __this = $(this);
								if (!__this.hasClass('is_delete')){
									var $choice = __this.find('div[data-attr="choice"]');
									choice.push($choice.html());
								}
							});	
							
							//data.choices_list = JSON.stringify(choice);
							data.choices_list = choice;
							data.field = 'choices_list';
							break;
						case 'attr':
							var attr = {};
							$li.each(function(){
								var __this = $(this);
								if (!__this.hasClass('is_delete')){
									var attr_name = __this.find('input[data-attr="attr"]').val(),
										attr_value = __this.find('.edit-div').html();
									
									attr[attr_name] = attr_value;
								}	
							});
							
							//data.attribute_list = JSON.stringify(attr);
							data.attribute_list = attr;
							data.field = 'attribute_list';
							break;
					}

					//与后台通信,成功即删除
					var callback = function(res){
						//console.log(res);
						if (res.status == 1){
							
							//成功删除
							$del_li.remove().mtkResetItemOption();
														
						} else {
							$del_li.removeClass('is_delete');
							alert(res.info);
						}
						//删除监听的某个事件
						socket._remove('save_status');
					};

					socket._save(data, callback);			
			});
				
		},
		
		//删除复合题 子题
		delComposite : function(){
			
			// 至少保留一道子题
			$element.dl.on("click", ".mtk-sel-del-fuhe", function(){
					
					var _this = $(this),
						$dt = _this.parents('dt'),
						$switchDiv = _this.parents('.mtk-sel-switch-div'),
						$question = $switchDiv.find('.mtk-question');
						$this_question = _this.parents('.mtk-question');
						
					if ( $question.size() < 2 ) {
						alert('至少有一道子题');
						return ;
					} else {
						
						var type = $dt.attr('data-type'),
							mid = $this_question.attr('data-qid'),
							qid = $dt.attr('data-id');
						
						//如果存在子题id 才请求数据库						
						if ( mid ) {
							//通信根据返回的结果 做删除子题
							var data = {
								mid : parseInt(mid),
								qid : qid == undefined ? '' : qid,
								type : type,
								pid : $("#paperId").val(),
							};
							
							var callback = function(res){
								
								if (res.status){
									$this_question.remove().mtkResetItemNum();								
								} else {
									alert(res.info);
								}
								//删除监听的某个事件
								socket._remove('del_status');
							}	
							socket._delete(data, callback);						
						} else {
							$this_question.remove().mtkResetItemNum();
						}
					}
			});
		},
		
		//保存分数(主动式)
		saveScore : function(){
			
			$element.dl.on("focus", ".mtk-item-score input", function(){
					//记录下分数
					papersdetail.score = $(this).val();
					
			});
			
			$element.dl.on("blur", ".mtk-item-score input", function(){
					var _this = $(this),
						value = _this.val(),
						$dt = _this.parents('dt'),
						type = $dt.attr('data-type');
					
					//如果为空不做任何操作 或者值没有改变 或者不是数字
					if ( value == '' || value == papersdetail.score ){
						return ;	
					}
					
					//非法数据
					if (!/^\d+$/.test(value)){
						_this.focus();
						return ;	
					}
					
					switch(parseInt(type)){
						case 0:
							var mid = $dt.attr('data-id');	
							break;
						case 1:
							var $quedtion = _this.parents('.mtk-question'),
								mid = $quedtion.attr('data-qid'),
								qid = $dt.attr('data-id');
							break;
					}
					
					var data = {
						'score': value,
						'mid': mid == undefined ? '' : mid,
						'type': type,
						'pid':$("#paperId").val(),
						'qid': qid == undefined ? '' : qid,
						'field':'score'
					};		
					
					var callback = function(res){

						_this.parents('.mtk-item-score').removeClass('checkscore');	

						if (res.status == 1){
							
							if (res.data){
							
								var _data = JSON.parse(res.data);
		
								if (data.type == 0){
									if (_data.mid){
										$dt.attr('data-id',_data.mid);						
									}
								} else {
									if (_data.mid){
										_this.parents('.mtk-question').attr('data-qid', _data.mid);	
									}
									if (_data.gid){
										$dt.attr('data-id',_data.gid);		
									}
								}
							}
						} else {
							alert(res.info);
						}
						//删除监听的某个事件
						socket._remove('save_status');
					};
					socket._save(data, callback);								
			});
			
		},
		
		//保存分数(被动式)
		pSaveScore : function(score, type){
			
			var data = {
				'score': score,
				'mid': '',
				'type': type,
				'pid':$("#paperId").val(),
				'qid': '' ,
				'field':'score'
			};
			
			if (arguments[2]) {
				data.mid = arguments[2];
			}
			
			var callback = function(res){
				
				if (res.status == 1){
						
					if (res.data){
						
						var _data = JSON.parse(res.data);
						
						if (_data.mid) {
							//_data.mid;
						}
						
						if (_data.gid) {
							//_data.gid;
						}
													
					}
				
				} else {
					alert('新增失败');
					return ;
				}
				//删除监听的某个事件
				socket._remove('save_status');
			};
			socket._save(data, callback);
			
		},
		
		//保存标题
		saveTitle : function(obj){

			var value = obj.html(),
				$dt = obj.parents('dt'),
				type = $dt.attr('data-type');

			if (value == null) {
				value = '';
			}
			
			switch(parseInt(type)){
				case 0:
					var mid = $dt.attr('data-id');			
					break;
				case 1:
					var mid = obj.parents('.mtk-question').attr('data-qid'),			
						qid = $dt.attr('data-id');		
					break;
			}
			
			var data = {
				'title': value,
				'mid': mid == undefined ? '' : mid,
				'type': type,
				'pid':$("#paperId").val(),
				'qid': qid == undefined ? '' : qid,
				'field':'title'
			};						
			
			var callback = function(res){
				//console.log(res);
				if (res.status == 1){

					//保存成功, 添加样式
					obj.parents('.mtk-problem').addClass('save');
										
					if (res.data){
					
						var _data = JSON.parse(res.data);

						if (data.type == 0){
							if (_data.mid){
								$dt.attr('data-id',_data.mid);						
							}
						} else {
							if (_data.mid){
								obj.parents('.mtk-question').attr('data-qid', _data.mid);	
							}
							if (_data.gid){
								$dt.attr('data-id',_data.gid);		
							}
						}
					}
				} else {
					alert(res.info);
				}
				//删除监听的某个事件
				socket._remove('save_status');
			};
			socket._save(data, callback);			
		},
		
		//保存选项
		saveChoice : function(obj){
			
			var $dt = obj.parents('dt'),
				$choice = $dt.find('div[data-attr="choice"]'),
				//获取mid
				mid = $dt.attr('data-id'),
				type = $dt.attr('data-type');
			
				var choice = [];
				
				switch(parseInt(type)){
					case 0:
						var mid = $dt.attr('data-id');			
						break;
					case 1:
						var $question = obj.parents('.mtk-question'),
							mid = $question.attr('data-qid'),
							qid = $dt.attr('data-id'),
							$choice = $question.find('div[data-attr="choice"]');						
						break;
				}				
				
				$choice.each(function(k, v){
					
					var val = $(this).html();
					if (val == null) {
						val = '';
					}
					choice.push(val);
				});								
				
				var data = {
					//'choices_list':JSON.stringify(choice),
					'choices_list' : choice,
					'mid': mid == undefined ? '' : mid,
					'type': type,
					'pid':$("#paperId").val(),
					'qid':qid == undefined ? '' : qid,
					'field':'choices_list'
				};									
				
				var callback = function(res){
					
					if (res.status == 1){
						
						//保存成功 , 添加样式
						obj.parents('li').addClass('save');
						
						if (res.data){
						
							var _data = JSON.parse(res.data);
	
							if (data.type == 0){
								if (_data.mid){
									$dt.attr('data-id',_data.mid);						
								}
							} else {
								if (_data.mid){
									obj.parents('.mtk-question').attr('data-qid', _data.mid);	
								}
								if (_data.gid){
									$dt.attr('data-id',_data.gid);		
								}
							}
							//console.log('save');
						}
					} else {
						alert(res.info);
					}
					//删除监听的某个事件
					socket._remove('save_status');
				};
				socket._save(data, callback);			
		},
		
		//保存属性
		saveAttr : function(obj){

			var $dt = obj.parents('dt');	
			//获取mid
			var type = $dt.attr('data-type');	
			var $attr = $dt.find('div[data-attr="attr"]');
			
			switch (parseInt(type)){
				case 0:
					var mid = $dt.attr('data-id');
					break;
				case 1:
					var $question = obj.parents('.mtk-question'),
						$attr = $question.find('div[data-attr="attr"]'),
						mid = $question.attr('data-qid'),
						qid = $dt.attr('data-id');				
					break;
			}
			
			var attr = {};
			$attr.each(function(k, v){
				var _this = $(this);
				var attr_name = _this.parent().find('input[data-attr="attr"]').val();
				attr[attr_name]=_this.html();
			});	
		
			var data = {
				//'attribute_list': JSON.stringify(attr),
				'attribute_list': attr,
				'mid': mid == undefined ? '' : mid,
				'type': type,
				'pid':$("#paperId").val(),
				'qid':qid == undefined ? '' : qid,
				'field':'attribute_list'
			};	
			
			var callback = function(res){
				//console.log(res);
				if (res.status == 1){
					
					if (res.data){
					
						var _data = JSON.parse(res.data);

						if (data.type == 0){
							if (_data.mid){
								$dt.attr('data-id',_data.mid);						
							}
						} else {
							if (_data.mid){
								obj.parents('.mtk-question').attr('data-qid', _data.mid);	
							}
							if (_data.gid){
								$dt.attr('data-id',_data.gid);		
							}
						}
					}
				} else {
					alert(res.info);
				}
				//删除监听的某个事件
				socket._remove('save_status');
			};
			socket._save(data, callback);		
		},
		
		//保存属性-input key
		saveAttrInput : function(){
			
			//拖动保存
			$element.dl.on("drop", "input[data-attr=attr]", function(){
					var _this = $(this);
					_this.focus();			
			});
			
			$element.dl.on("focus", "input[data-attr='attr']", function(){
					papersdetail.attr = Tool.trim($(this).val());	
			}),
			
			$element.dl.on("blur", "input[data-attr='attr']", function(){

					var _this = $(this),
						$dt = $(this).parents('dt'),
						value = _this.val(),
						tr_value = Tool.trim(value),
						type = $dt.attr('data-type');
					
					//判断如果为空或者 没有改变不保存
					if ( tr_value == '' || tr_value == papersdetail.attr )
						return ;
					
					switch(parseInt(type)){
						case 0:
							var mid = $dt.attr('data-id'),
								$attr = $dt.find('input[data-attr="attr"]');
							break;
						case 1:
							var $question = _this.parents('.mtk-question'),
								$attr = $question.find('input[data-attr="attr"]'),
								mid = $question.attr('data-qid'),
								qid = $dt.attr('data-id');
							break;
					}
					
					var attr={};
					$attr.each(function(k, v){
						var _this = $(this);
						var attr_val = _this.parents('li').find('.edit-div').html();
						
						if (attr_val == null) {
							attr_val = '';
						}					
						
						attr[_this.val()] = attr_val;				
					});					

					var data = {
						//'attribute_list': JSON.stringify(attr),
						'attribute_list': attr,
						'mid': mid == undefined ? '' : mid,
						'type': type,
						'pid':$("#paperId").val(),
						'qid':qid == undefined ? '' : qid,
						'field':'attribute_list'
					};
					
					var callback = function(res){
						if (res.status == 1){
							
							//保存成功, 添加样式
							_this.parents("li").addClass("save");							
							
							if (res.data){
							
								var _data = JSON.parse(res.data);
								
								if (data.type == 0){
									
									if (_data.mid){
										$dt.attr('data-id',_data.mid);						
									}
								} else {
									if (_data.mid){
										_this.parents('.mtk-question').attr('data-qid', _data.mid);	
									}
									if (_data.gid){
										$dt.attr('data-id',_data.gid);		
									}
								}
							}
						} else {
							alert(res.info);
						}
						//删除监听的某个事件
						socket._remove('save_status');
					};
					socket._save(data, callback);		
			});			
		},
		
		//保存材料
		saveMaterial : function(obj){

			//获取mid - 注意这里是复合题id
			var $dt = obj.parents('dt'),	
				qid = $dt.attr('data-id'),
				type = $dt.attr('data-type'),
				material = obj.html();
				
			if (material == null) {
				material = '';
			}
				
			var data = {
				'material':material,
				'type': type,
				pid:$("#paperId").val(),
				//pid : 1,
				'qid': qid == undefined ? '' : qid,
				'field':'material'
			};
			
			var callback = function(res){
				//console.log(res);
				if (res.status == 1){
					
					//保存成功
					obj.parent().addClass('save');
					
					if (res.data){
					
						var _data = JSON.parse(res.data);
						if (_data.gid){
							$dt.attr('data-id',_data.gid);		
						}
					}
				} else {
					alert(res.info);
				}
				//删除监听的某个事件
				socket._remove('save_status');
			};
			
			socket._save(data,callback);		
		},
		
		//可编辑的div 失去焦点, 可以拖动, 并且保存数据
		leaveEditDiv : function(){
						
			$element.dl.on("focus", ".edit-div", function(){
					papersdetail.editDiv = Tool.trim($(this).html());
			}),
			
			//拖动
			$element.dl.on("drop", ".edit-div", function(){
				var _this = $(this);
				_this.focus();
				//papersdetail.editDiv = Tool.trim(_this.html());
				
				/*
				_this.on("input", function(){
					papersdetail.editDiv = Tool.trim(_this.html());
					_this.trigger("blur");
					_this.off("input");
				});
				*/
			}),
			
			$element.dl.on("blur", ".edit-div", function(){
								
				//此处判断 如果展开了无法拖动
				mtk_paper.sortable_choice();
				//$(".mtk-item-dl").sortable("enable");
				$(".sortable").sortable("enable");
				$('.sortable').sortable({axis:'y'});
					
				//通过判断类型, 组装不同结构的数据
				var _this = $(this),
					value = _this.html(),
					tr_value = Tool.trim(value),
					attr = _this.attr('data-attr'); 
				
				//如果 为空 或者没有改变内容				
				if ( tr_value == '' || tr_value == papersdetail.editDiv )
					return ;
				
				//分别为 标题，选项，属性，材料
				switch(attr) {
					//标题
					case 'title':
						mtk_paper.saveTitle(_this);
						break;
					//选项
					case 'choice':
						mtk_paper.saveChoice(_this);
						break;
					//属性
					case 'attr':
						mtk_paper.saveAttr(_this);
						break;
					//材料
					case 'material':
						mtk_paper.saveMaterial(_this);
						break;
				}
			});		
		},
		
		//拖动(题目)
		sortable_question : function(){
			
			$element.dl.on("mousemove", "dt", function(){
					
					var _this = $(this);
					
					mtk_paper._sortable_question();
					
					if (_this.hasClass('mtk-item-slide')){
						//隐藏时才能拖动
						$('.mtk-item-dl').sortable("enable");
					} else {
						//展开时不能拖动						
						$('.mtk-item-dl').sortable("disable");
					}
			});
		},
		
		//拖动触发时间函数
		_sortable_question : function(){
			
			$('.mtk-item-dl').sortable({
				axis : 'y',
				revert : true,
				
				//拖动事件开始
				start : function(){
					//改变控制开关
					$element.flag = false;
				},
				update : function(event, ui){					
					
					var ids = [];
					$("dt").each(function(){
						var _this = $(this),
							id = _this.attr('data-id'),
							type = _this.attr('data-type');
						
						if (id){
							
							var _data = {};
							_data.id = id;
							_data.type = type;
							ids.push(_data);
						}
											
					});
					
					var data = {
						//pid : 1,
						pid:$("#paperId").val(),
						structs : ids
					};
					
					//console.log(data);
					
					var callback = function(res){
						
						if (res.status){
							//$().mtkResetItemNum();							
						}
					};
					
					socket._sortQuestion(data, callback);
					$().mtkResetItemNum();		
				},
				stop: function(){
					//改变控制开关
					$element.flag = true;
				}
			});	
		},
		
		
		//拖动(选项和正确答案)
		sortable_choice : function(){
			
			//mtk-item-dl 每一道大题 .sortable可拖动元素
			$('.sortable').sortable({
				axis : 'y',
				revert : true, 
				update : function(event, ui){
					
					var $li = ui.item,
						$ul = $li.parents('ul'),
						$answer = $ul.find('.mtk-c1 img'),
						$choice = $ul.find('.edit-div'),
						$dt = $li.parents('dt'),
						type = $dt.attr('data-type');
					
					switch(parseInt(type)){
						case 0:
							var mid = $dt.attr('data-id');
							break;
						case 1:
							var mid = $li.parents('.mtk-question').attr('data-qid');
							break;
					}
											
					if (mid) {
						// mid存在
						//正确选项
						var correct = [];
						$answer.each(function(k){
							if ( $(this).hasClass('correct') ){
								correct.push(k);
							}								
						});
						
						//选项
						var choice = [];
						$choice.each(function(){
							choice.push($(this).html());
						});						
						
						var data = {
							mid : mid,
							pid : $("#paperId").val(),
							choices_list : choice,
							correct : correct,
							amount : correct.length
						};
						
						//console.log(data);
										
						var callback = function(res){
							//console.log(res);
							if (res.status){
								$().mtkResetItemNum();
								$().mtkResetItemOption();					
							} else{
								//失败
								alert(res.info);
								//删除监听的某个事件
								socket._remove('sort_choice_status');
							}
						};
			
						socket._sortChoice(data,callback);	
					
					} else {
						//改变顺序,不做顺序操作
						$().mtkResetItemNum();
						$().mtkResetItemOption();					
					} // end if	
				}		
			});
		},
			
		/**********无关数据交互的dom操作************/		
		//点击可以编辑的div ，给上焦点 并且无法拖动
		clickEditDiv : function(){
			
			$element.dl.on("click", ".edit-div", function(){
					mtk_paper.sortable_choice();
					//$(".mtk-item-dl").sortable("disable");
					$(".sortable").sortable("disable");
					$(this).focus();				
			});
		},
		
		//显示隐藏题目
		showQuestion : function(){
			$element.dl.on("click", ".mtk-show-item", function(){
				
				//console.log($element.flag);
				
				//展开控制开关
				if (!$element.flag)
					return ;
				
				var _this = $(this);
				
				if (_this.parent().is(".mtk-item-slide-noa")){
					
					//预览标题 ,只显示15个字符
					var $dt = _this.parents('dt');
					var text = $dt.find('.mtk-item-tm').text();
						text = text.substring(0,10);

					$dt.find('.mtk-item-slide-txt').text(text);
					
					_this.parent().removeClass("mtk-item-slide-noa").addClass("mtk-item-slide-a").parent().addClass("mtk-item-slide");
				} else {
					_this.parent().removeClass("mtk-item-slide-a").addClass("mtk-item-slide-noa").parent().removeClass("mtk-item-slide");
				}
												
			});
		},
				
		//移动上去 显示或隐藏删除按钮
		showdel : function(){
			
			//显示
			$element.dl.on("mouseover", ".mtk-choice li,.mtk-item-analysis li", function(){
					$(this).find('.mtk-sel-del').show();
			});
			
			//隐藏
			$element.dl.on("mouseout", ".mtk-choice li,.mtk-item-analysis li", function(){
					$(this).find('.mtk-sel-del').hide();
			});
				
		},
		
		//新增选项
		addChoice : function(){
			
			$element.dl.on("click", ".mtk-add-item", function(){
					
					var _this = $(this),
						$ul = _this.parents('.mtk-choice').find('.sortable'),
						$li = $ul.find('li');
						mtk_add_sel_string = '<li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter"></span><span>.</span></div><div id="editor1" data-attr="choice" class="edit-div fl mtk-c2" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li>';
					
					if ( $li.size() > 9 ){
							alert('至多添加10条选项');		
					} else {
							//添加
							$ul.append(mtk_add_sel_string).mtkResetItemOption();
					}
			});
		},
		
		//新增解析
		addAnalysis : function(){

			$element.dl.on("click", ".mtk-add-analysis", function(){
					
					var _this = $(this),
						$ul = _this.parents('.mtk-item-analysis').find('.mtk-item-analysis-ul ul'),
						$li = $ul.find('li');
						mtk_add_sel_string='<li class="clearfix"><div class="mtk-item-analysis-li1 fl" ><input type="text" data-attr="attr" maxlength="10" placeholder="填写属性"/></div><div class="mtk-item-analysis-li2 fl ">/ </div><div class="mtk-item-analysis-li3 edit-div fl" title="填写内容" data-attr="attr"  contenteditable="true"></div><div class="mtk-item-analysis-li4 fl"><div class="mtk-sel-del" del-attr="attr"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li>';
					
					if ( $li.size() > 4 ){
							alert('至多添加5条属性');		
					} else {
							//添加
							$ul.append(mtk_add_sel_string);	
					}
			});
		
		},
		
		//新增普通题
		addCommon : function(){
			
			$element.common.on("click", function(){
				
				//首先验证 如果此题为复合题 ，是必须存在子题的
				if (!mtk_paper.hasSon())
					return ;
				
				/*
				//验证是否存在正确答案或者分数				
				if (!mtk_paper.checkData())
					return ;
				*/
				
				//获取此类题目最后一题的分数
				var score = mtk_paper.getLastScore(0);
				
				//新增
				var data = {
					'score': score,
					'mid': '',
					'type': 0,
					'pid':$("#paperId").val(),
					'qid': '' ,
					'field':'score'
				};
				
				var callback = function(res){
					
					if (res.status == 1){
							
						if (res.data){
							
							var _data = JSON.parse(res.data);
							
							if (_data.mid) { 
							
								
								var	mtk_add_new_string='<dt data-type="0" data-id="'+_data.mid+'"><div class="mtk-item-slide mtk-item-slide-noa"><div class="fl mtk-item-slide-"><span class="mtk-item-slide-tit">.</span><span class="mtk-item-slide-type">普通题</span></div><div class="mtk-item-txt fl"><div class="clearfix"><div class="fl mtk-item-slide-txt"></div><div class="fl">... </div></div></div><span class="mtk-del-item fr" title="删剪此题"> </span><span class="mtk-show-item a" title="点击展开收起"></span></div><div class="mtk-item-w940"><div class="mtk-item-sel-switch"><div class="mtk-sel-switch-div"><div class="mtk-item-border mtk-common-item"><div class="mtk-problem clearfix"><div class="mtk-problem-ico fl" title="填写问题"><img src="'+base_url+'/admin/images/mtk-01.png"/></div><div data-attr="title" class="edit-div mtk-item-tm fl" contenteditable="true" dir="ltr"></div><div class="mtk-item-score mtk-item-score-pt fl"><input type="text" maxlength="3" data-attr="score" placeholder="此题分数" value="'+score+'" /></div></div><div class="mtk-choice"><ul class="sortable"><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">A</span><span>.</span></div><div id="editor1" class="edit-div fl mtk-c2" data-attr="choice" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">B</span><span>.</span></div><div id="editor2" data-attr="choice" class="edit-div fl mtk-c2" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">C</span><span>.</span></div><div id="editor3" data-attr="choice" class="edit-div fl mtk-c2" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del"  del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案"/>&nbsp;<span class="mtk-sel-letter">D</span><span>.</span></div><div id="editor4" data-attr="choice" class="edit-div fl mtk-c2" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del"  del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li></ul><span class="mtk-add-item clearfix" title="新增一条选项"><div class="mtk-add-item-txt1 fl">+</div><div class="mtk-add-item-txt2 fl">新增一条选项</div></span></div></div><div class="mtk-item-analysis"><div class="mtk-item-analysis-tit">本题解析</div><div class="mtk-item-analysis-div"><div class="mtk-item-analysis-ul"><ul><li class="clearfix"><div class="mtk-item-analysis-li1 fl"><input type="text" data-attr="attr" maxlength="10" placeholder="填写标题"/></div><div class="mtk-item-analysis-li2 fl ">/ </div><div class="mtk-item-analysis-li3 edit-div fl" title="填写内容"  data-attr="attr" contenteditable="true"></div><div class="mtk-item-analysis-li4 fl"><div class="mtk-sel-del"  del-attr="attr"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li></ul><span class="mtk-add-analysis clearfix" title="新增一条选项"><div class="mtk-add-analysis-txt1 fl">+</div><div class="mtk-add-analysis-txt2 fl">新增一条解析</div></span></div></div></div></div></div></div></dt>';
								$element.dl.append(mtk_add_new_string).mtkResetItemNum();
								
								
								/*****滑动当新增的题*****/
								//1. 获取新增的题目 dt 离定点距离
								var _height = $("dt:last").offset().top;
								//2. 滑动
								$('body,html').animate({'scrollTop':_height}, 800);
							}
				
						}
					
					} else {
						alert('新增失败');
						return ;
					}
					//删除监听的某个事件
					socket._remove('save_status');
				};
				socket._save(data, callback);
				
			});
		},
		
		//新增复合题		
		addComposite : function(){

			$element.composite.on("click", function(){
				
				//首先验证 如果此题为复合题 ，是必须存在子题的
				if (!mtk_paper.hasSon())
					return ;
				
				/*
				//验证是否存在正确答案或者分数				
				if (!mtk_paper.checkData())
					return ;
				*/

				//获取此类题目最后一题的分数
				var score = mtk_paper.getLastScore(1);
				
				var data = {
					'score': score,
					'mid': '',
					'type': 1,
					'pid':$("#paperId").val(),
					'qid': '' ,
					'field':'score'
				};
			
				var callback = function(res){
					
					if (res.status == 1){
							
						if (res.data){
							
							var _data = JSON.parse(res.data);
							console.log(_data);		
							var ids = {
								mid : '',
								gid : ''
							}
							
							if (_data.mid) {
								ids.mid = _data.mid;
							}
							
							if (_data.gid) {
								ids.gid = _data.gid;
							}

							var mtk_add_new_string='<dt data-type="1" data-id="'+ids.gid+'"><div class="mtk-item-slide mtk-item-slide-noa"><div class="fl mtk-item-slide-"><span class="mtk-item-slide-tit"></span><span class="mtk-item-slide-type">复合题</span></div><div class="mtk-item-txt fl"><div class="clearfix"><div class="fl mtk-item-slide-txt"></div><div class="fl">... </div></div></div><span class="mtk-del-item fr" title="删剪此题"> </span><span class="mtk-show-item a" title="点击展开收起"></span></div><div class="mtk-item-w940"><div class="mtk-item-sel-switch"><div class="mtk-sel-switch-div"><div class="mtk-edit-data-div"><div class="edit-div mtk-edit-data a" data-attr="material" contenteditable="true"></div></div><div class="mtk-question" data-qid="'+ids.mid+'"><div class="mtk-item-border mtk-recom-item"><div class="mtk-problem clearfix"><div class="mtk-item-num fl" title="填写问题"><span class="mtk-item-num-txt"></span>. </div><div data-attr="title" class="edit-div mtk-item-tm fl" contenteditable="true" dir="ltr"></div> <div class="mtk-item-score fl"><input type="text" value="'+score+'" data-attr="score" maxlength="3" placeholder="此题分数" /></div><div class="mtk-sel-del-fuhe fl"><img src="'+base_url+'/admin/images/mtk-03.png"/></div></div><div class="mtk-choice"><ul class="sortable"><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">A</span><span>.</span></div><div id="editor1"  class="edit-div fl mtk-c2" data-attr="choice" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">B</span><span>.</span></div><div id="editor2" class="edit-div fl mtk-c2" data-attr="choice" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">C</span><span>.</span></div><div id="editor3" data-attr="choice" class="edit-div fl mtk-c2" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">D</span><span>.</span></div><div id="editor4" data-attr="choice" class="edit-div fl mtk-c2" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li></ul><span class="mtk-add-item clearfix" title="新增一条选项"><div class="mtk-add-item-txt1 fl">+</div><div class="mtk-add-item-txt2 fl">新增一条选项</div></span></div></div><div class="mtk-item-analysis"><div class="mtk-item-analysis-tit">本题解析</div><div class="mtk-item-analysis-div"><div class="mtk-item-analysis-ul"><ul><li class="clearfix"><div class="mtk-item-analysis-li1 fl"><input type="text" data-attr="attr" maxlength="10" placeholder="填写标题"/></div><div class="mtk-item-analysis-li2 fl ">/ </div><div class="mtk-item-analysis-li3 edit-div fl" title="填写内容"  contenteditable="true" data-attr="attr"></div><div class="mtk-item-analysis-li4 fl"><div class="mtk-sel-del" del-attr="attr" ><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li></ul><span class="mtk-add-analysis clearfix" title="新增一条选项"><div class="mtk-add-analysis-txt1 fl">+</div><div class="mtk-add-analysis-txt2 fl">新增一条解析</div></span></div></div></div></div><div class="mtk-add-question">新增一条问答 </div></div></div></div></dt>';
							$element.dl.append(mtk_add_new_string).mtkResetItemNum();
							
							/*****滑动当新增的题*****/
							//1. 获取新增的题目 dt 离定点距离
							var _height = $("dt:last").offset().top;
							//2. 滑动
							$('body,html').animate({'scrollTop':_height}, 800);
														
						}
					
					} else {
						alert('新增失败');
						return ;
					}
					//删除监听的某个事件
					socket._remove('save_status');
				};
				socket._save(data, callback);

			});
		},
		
		//新增一条回答 (复合题里的子题)
		addSubQuestion : function(){
			
			$element.dl.on("click", ".mtk-add-question", function(){
				
				var _this = $(this),
					$switchDiv = _this.parents('.mtk-sel-switch-div'),
					l = $switchDiv.find('.mtk-question').size();
				
				if (!l){
					var	order_number = '1-1';
				} else {
				var order_number= $switchDiv.find('.mtk-question:last .mtk-item-num-txt').text(),
					arr = order_number.split('-'),
					order_number = arr[0] + '-' + (parseInt(arr[1]) + 1);
				}
				
				/*	
				//检测此复合题的最后一子题是否填 分数和正确答案
				if (l){
					var $mtkQuestion = $switchDiv.find('.mtk-question:last');
					var score = $mtkQuestion.find('.mtk-item-score input').val();
					var $correct = $mtkQuestion.find('.mtk-c1 .correct');
	
					if (score==''){
						alert('请填写此题分数');
						return ;						
					}
					
					if ($correct.size() == 0){
						alert('请勾选正确答案');
						return ;
					}							
				}
				*/

				//获取此类题目最后一题的分数
				var score = mtk_paper.getLastScore(1);
				//说明已经有id
				var mid = _this.parents("dt").attr("data-id");

				var data = {
					'score': score,
					'mid': '',
					'type': 1,
					'pid':$("#paperId").val(),
					'qid': parseInt(mid),
					'field':'score'
				};
				
				var callback = function(res){
					
					if (res.status == 1){
							
						if (res.data){
							
							var _data = JSON.parse(res.data);
							var ids = {
								mid : ''	
							}
							
							if (_data.mid) {
								ids.mid = _data.mid;
							}
						
							var	mtk_add_question_string = '<div class="mtk-question" data-qid="'+ids.mid+'"><div class="mtk-item-border mtk-recom-item"><div class="mtk-problem clearfix"><div class="mtk-item-num fl"title="填写问题"><span class="mtk-item-num-txt">'+order_number+'</span>.</div><div data-attr="title" class="edit-div mtk-item-tm fl" contenteditable="true" dir="ltr"></div><div class="mtk-item-score fl"><input type="text" maxlength="3" data-attr="score" value="'+score+'" placeholder="此题分数" /></div><div class="mtk-sel-del-fuhe fl"><img src="'+base_url+'/admin/images/mtk-03.png"/></div></div><div class="mtk-choice"><ul class="sortable"><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">A</span><span>.</span></div>';
								mtk_add_question_string += '<div id="editor1" class="edit-div fl mtk-c2" data-attr="choice" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">B</span><span>.</span></div>';
								mtk_add_question_string += '<div id="editor2" class="edit-div fl mtk-c2" data-attr="choice" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">C</span><span>.</span></div>';
								mtk_add_question_string += '<div id="editor3" class="edit-div fl mtk-c2" data-attr="choice" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li><li class="clearfix"><div class="fl mtk-c1"><img src="'+$element.imgurl+'" title="点击设置为正确答案" />&nbsp;<span class="mtk-sel-letter">D</span><span>.</span></div>';
								mtk_add_question_string += '<div id="editor4" class="edit-div fl mtk-c2" data-attr="choice" contenteditable="true" dir="ltr"></div><div class="fr mtk-c3"><div class="mtk-sel-del" del-attr="choice"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li></ul><span class="mtk-add-item clearfix" title="新增一条选项"><div class="mtk-add-item-txt1 fl">+</div><div class="mtk-add-item-txt2 fl">新增一条选项</div></span></div></div>';
								mtk_add_question_string += '<div class="mtk-item-analysis"><div class="mtk-item-analysis-tit">本题解析</div><div class="mtk-item-analysis-div"><div class="mtk-item-analysis-ul"><ul>';
								mtk_add_question_string += '<li class="clearfix"><div class="mtk-item-analysis-li1 fl"><input type="text" data-attr="attr" maxlength="10" placeholder="填写标题"/></div><div class="mtk-item-analysis-li2 fl ">/</div><div class="mtk-item-analysis-li3 edit-div fl" title="填写内容" contenteditable="true" data-attr="attr"></div><div class="mtk-item-analysis-li4 fl"><div class="mtk-sel-del" del-attr="attr"><img src="'+base_url+'/admin/images/mtk-04.png"/></div></div></li></ul>';
								mtk_add_question_string += '<span class="mtk-add-analysis clearfix" title="新增一条选项"><div class="mtk-add-analysis-txt1 fl">+</div><div class="mtk-add-analysis-txt2 fl">新增一条解析</div></span></div></div></div></div>';
							
							//添加
							_this.parents('.mtk-sel-switch-div').find('.mtk-add-question').before(mtk_add_question_string);
							
							/*****滑动当新增的题*****/
							//1. 获取新增的题目 dt 离定点距离
							var _height = $switchDiv.find('.mtk-question:last').offset().top;
							//2. 滑动
							$('body,html').animate({'scrollTop':_height}, 800);				
														
						}
					
					} else {
						alert('新增失败');
						return ;
					}
					//删除监听的某个事件
					socket._remove('save_status');
				};
				 
				 
				socket._save(data, callback);
	
			});
		},
		
		//置顶
		setTop : function(){
			$(window).scroll(function(){

				if($(window).scrollTop() >= 100){
					$('.actGotop').fadeIn(300); 
				}else{    
					$('.actGotop').fadeOut(300);    
				}  
			});	
			
			$('.actGotop').click(function(){
				$('body').animate({scrollTop: '0px'}, 800);
			});				
		},
		
		//如果最后一题为复合体 ，新增前判断此题必须要有子题
		hasSon : function(){
			
			var l = $("dt").size();
			if (l){
				var $dt = $("dt:last"),
					type = parseInt($dt.attr('data-type'));
				
				if (type) {
					var _l = $dt.find('.mtk-question').size();
					if (_l==0){
						alert('每道复合题至少有一道子题');
						return ;
					}			
				}
			}
			
			return true;
		}, 
		
		//检测是否填写正确答案或分数
		checkData : function(){

				var l = $("dt").size();
				if (l){
					
					var $dt = $("dt:last"),
						type = $dt.attr('data-type');

					//判断复合题和普通题目
					switch(parseInt(type)){
						case 0:
							var score = $dt.find('.mtk-item-score input').val(),
								$correct = $dt.find('.mtk-c1 .correct');
							break;
						case 1:
							var $mtkQuestion = $dt.find('.mtk-question:last'),
								score = $mtkQuestion.find('.mtk-item-score input').val(),
								$correct = $mtkQuestion.find('.mtk-c1 .correct');
							break;
					}
					
					if (score==''){
						alert('请填写此题分数');
						//$dt.find('.mtk-item-score input').focus();
						return ;						
					}
					
					if ($correct.size() == 0){
						alert('请勾选正确答案');
						return ;
					}
				}
				return true;
		},
		
		//获取同类型 上一题的分数
		getLastScore : function(type){
			
			var score = 0;
			var $dt = $("dt[data-type='"+type+"']").last(),
				$score = $dt.find("input[data-attr='score']").last(),	
				_score = $score.val();
			
			if (_score) {
				score = _score;
			}
			
			return score;
		}
		
	};
	
	var Tool = {
		trim : function(str){
			return str.replace(/(^\s*)|(\s*$)/g, "");	
		}
	};

	//初始化操作
	mtk_paper.initialize();
	
})(jQuery, window, io);


