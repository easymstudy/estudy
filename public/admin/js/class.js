(function($, w){
	
	w.form = {
		'name' : $('input[name=className]'),
		'num' : $('input[name=classNum]'),
		'pass' : $('input[name=classPass]'),
		'announce' : $('textarea'),
		'total' : $('input[name=total]').val(),
		'cid' : $('input[name=cid]').val(),
		'userexam': $('select[name=userexam]'),
		'examrank': $('select[name=rank]')
	};
	
	w.tool = {
		
		'validate' : function(){
			
			/*
			 * 班级名不得为空
			 * 班级最大人数必须是数字
			 * 密码不能超过6位
			 * 公告140以内
			 */
			var className = form.name.val(),
			 	num = form.num.val(),
				pass = form.pass.val(),
				announce = form.announce.val();

			if ( className == '' ) {
				alert('班级名不能为空!');
				return ;
			}
			
			if ( num.length>4 || !/^[1-9]\d+$/.test(num) ) {
				alert('班级人数为1-9999的整数');
				return ;	
			}
			
			//密码如果不为空 ，必须是小于8位的数字
			if (pass) {
				
				if ( pass.length > 8 ||  !/^[0-9]\d+$/.test(pass)) {
					alert('密码为8位以下的数字');
					return ;
				}
			}
			
			if ( announce.length > 140 ) {
				alert('公告不能超过140个字');
				return ;
			}
			
			return true;
		},
		
		'ajax_post' : function(url, data, callback){
			$.post(url, data, callback);			
		}
		
	};
	
	//创建班级
	$('.creataSubmit').on('click', function(){
			
			var _this = $(this);
			
			_this.attr('disabled',true);		

			if ( tool.validate() ) {

				var data = {
					'topnum' : form.num.val(),
					'pass' : form.pass.val(),
					'name' : form.name.val(),
					'introduce' : form.announce.val()
				};

				tool.ajax_post(base_url+'/classCreate', data, function(res){
						
						if (res.status == 1) {
							alert(res.info);
							location.href = base_url+'/classSuccess/'+res.cid;
						} else {
							alert( res.info );
						}
						_this.attr('disabled',false);
				});				
			} else {
				_this.attr('disabled',false);
			}
	});
	
	//修改班级
	$('.class-modify').on('click', function(){
			
			var _this = $(this);
			_this.attr('disabled',true);
			
			if ( tool.validate() ) {
				
				//验证成功请求服务器
				var data = {
					'topnum' : form.num.val(),
					'pass' : form.pass.val(),
					'name' : form.name.val(),
					'introduce' : form.announce.val(),
					'cid' : $('input[name=cid]').val()
				};
				
				tool.ajax_post(base_url+'/classModify', data, function(res){
						
						if (res.status == 1) {
							
							location.href = base_url+'/classSuccess/'+res.cid;

						} else {
							alert( res.info );
						}
						_this.attr('disabled',false);
				});						
				
			} else {
				_this.attr('disabled',false);			
			}
	});
	
	//班级列表
	//搜索
	$(".search-class").on("click", function(){
		var wd = $(this).parent('.search').find('.form-control').val();
		window.location = base_url+'/classList/1/'+encodeURIComponent(wd);
	});
	
	//上一页
	$(".page-left .pre-class-list").on("click", function(){
		
		//获取分页的情况
		var pagination = $(".pagination-info").val(),
		pagination = pagination.split('/');
		if (pagination[0] == 1) 
			return ;
		
		var p = pagination[0] - 1;

		//跳转页面
		location.href = base_url+'/classList/' + p;													
	
	});
	
	//下一页
	$(".page-right .next-class-list").on("click", function(){

		//获取分页的情况
		var pagination = $(".pagination-info").val(),
			pagination = pagination.split('/');	

		if ( pagination[0] == form.total)
			return ;

		var p = parseInt(pagination[0]) + 1;
		
		//跳转页面
		location.href = base_url+'/classList/' + p;
		 
	});
	
	
	//全选和全不选
	$(".allcheck").on("click", function(){
		
		var _this = $(this);
		if ( _this.hasClass('checked') ){
			
			//已选中状态， 让所有全不选
			var img_url = base_url+'/admin/images/check.png';
			_this.removeClass('checked').attr('src', img_url);
			$(".class-list-number img").removeClass('checked').attr('src', img_url);
			
			//取消全选后  可以完成复制，导入，输出页面
			
					
		} else {
			var img_url =  base_url+'/admin/images/checking.png';
			//未选中状态， 让所有全选
			_this.addClass('checked').attr('src', img_url);
			$(".class-list-number img").addClass('checked').attr('src', img_url);
			
			//全选后 无法完成复制，导入，输出页面
			
		}
	});
	
	//勾选
	$(".class-list-number img").on("click", function(){
		
		var _this = $(this);
		if ( _this.hasClass('checked') ){
			var img_url = base_url+'/admin/images/check.png';
			_this.removeClass('checked').attr('src', img_url);	
		} else {
			var img_url =  base_url+'/admin/images/checking.png';
			_this.addClass('checked').attr('src', img_url);	
		}
	});
	
	//删除班级
	$(".delete-class").on("click", function(){

		var ids = getCheckIds();
		
		if (!ids) {
			alert('请至少选择一个班级');
			return ;
		}
	
		if (window.confirm("确定要删除班级?")) {		
			
			var data = {
				'ids' : JSON.stringify(ids),
				'is_delete': 1
			};		
			
			var $status = $("table.class-list-table").find('img.checked').parents('tr');
			
			tool.ajax_post(base_url+'/deleteclass', data, function(res){
					if (res.status){
						$status.remove();
					} else {
						alert(res.info);
					}				
			});					
		}
		

	});
	
	//获取勾选的班级列表 ids
	function getCheckIds(){
		//获取勾选的id
		var ids = [];
		var $img_checked = $("table.class-list-table").find('img.checked');
		var l = $img_checked.size();
		
		if (l == 0) {
			return false;
		}
		
		$img_checked.each(function(){
			var id = $(this).parents('tr').attr('data-cid');
			ids.push(id);
		});

		return ids;
	}
	
	/*********班级详情页面********/
	//上一页
	$(".page-left .left-detail-button").on("click", function(){
		//获取分页的情况
		var pagination = $(".pagination-info").val(),
		pagination = pagination.split('/');
		if (pagination[0] == 1) 
			return ;
		
		var p = pagination[0] - 1;

		//跳转页面
		location.href = base_url+'/classDetail/'+form.cid+'/'+p;			
	});
	
	//下一页
	$(".page-right .right-detail-button").on("click", function(){
		
		//获取分页的情况
		var pagination = $(".pagination-info").val(),
			pagination = pagination.split('/');	

		if ( pagination[0] == pagination[1])
			return ;

		var p = parseInt(pagination[0]) + 1;
		
		//跳转页面
		location.href = base_url+'/classDetail/'+form.cid+'/'+p;			
							
	});
	
	/*****班级作业记录列表*****/
	//上一页
	$(".page-left .left-worklist-button").on("click", function(){
		//获取分页的情况
		var pagination = $(".pagination-info").val(),
		pagination = pagination.split('/');
		if (pagination[0] == 1) 
			return ;
		
		var p = pagination[0] - 1;

		//跳转页面
		location.href = base_url+'/workList/'+form.cid+'/'+p;			
	});
	
	//下一页
	$(".page-right .right-worklist-button").on("click", function(){
		
		//获取分页的情况
		var pagination = $(".pagination-info").val(),
			pagination = pagination.split('/');	

		if ( pagination[0] == pagination[1])
			return ;

		var p = parseInt(pagination[0]) + 1;
		
		//跳转页面
		location.href = base_url+'/workList/'+form.cid+'/'+p;			
							
	});	
	
	//跳转到班级修改页面
	$(".modify-class").on("click", function(){

		var $img_checked = $("table.class-list-table").find('img.checked');
		var l = $img_checked.size();
		if (l==0) {
			alert('请选择1个班级');
			return ;
		}
		
		if (l>1) {
			alert('只能选择1个班级修改');
			return ;
		}
		
		//跳转到修改页面		
		if (l == 1) {
			var cid = $img_checked.attr("data-cid");
			window.location.href = base_url+'/modifyclass/'+parseInt(cid);
		}
		
	});
	
	//生成随机数
	$(".create-password").on("click", function(){
			var num = MathRand();
			$(this).prev().val(num);
	});
	
	//随机数
	function MathRand() { 
		var Num=""; 
		for(var i=0;i<6;i++){ 
			Num += Math.floor(Math.random()*10); 
		}
		return Num; 
	}
	
	/**** 切换提交作业时间 显示统计*****/
	form.userexam.on("change",function(){
		
		var _this = $(this), 
			$selected = $("select option:selected"),
			ueid = _this.val(),
			eid  = $selected.attr('data-eid');
		
		var data = {
			'cid':parseInt(form.cid),
			'uid':parseInt($('input[name=uid]').val()),
			'ueid' : parseInt(ueid),
			'eid' : parseInt(eid)
		};

		if (!data.cid || !data.uid || !data.ueid || !data.eid) {
			return ;	
		}

		tool.ajax_post(base_url+'/getClassStatistics', data, function(res){
				
				if (res.status) {
					
					//作业基础信息
					if (res.examinfo) {
						//2014年9月12日 16:00 - 2014年9月16日 16:00
						$('.paper-title').html('<span class="lightgray">试 卷 名：</span>'+res.examinfo.title);
						$('.paper-time').html('<span class="lightgray">作业时间：</span>'+res.examinfo.startime+' - '+res.examinfo.endtime);
					}
					
					//六项统计数据
					if (res.examstat) {

						var $examspan = $('.exam-info').find('.num');
						//总共题数
						$examspan.eq(0).text(res.examstat.total);	
						//得分
						$examspan.eq(1).text(res.examstat.score);
						//名次
						$examspan.eq(2).html("<span class='green'>"+res.examstat.rank+"</span>"+'/'+res.examstat.alls);						
						//答对数
						$examspan.eq(3).text(res.examstat.right_num);						
						//平均分
						$examspan.eq(4).text(res.examstat.avg);												
						//最高分	
						$examspan.eq(5).text(res.examstat.top);												
					}						
					
					//题目详情
					if (res.examdetail) {

						var str3 = '';
						$(".paper-sort").empty();
						var i = 1;
						$.each(res.examdetail, function(k,v){
							
							if (v.result == 1) {
								str3 += '<li>'+i+'</li>';	
							} else {
								str3 += '<li class="error">'+i+'</li>';
							}
							i++;
						});
						$(".paper-sort").append(str3);
					}					

				}
		});					
		
	}).trigger('change');
	
	//作业学生排名统计
	form.examrank.on("change", function(){

		var _this = $(this), 
			type = _this.val();
		
		var data = {
			cid : form.cid,
			eid : $('input[name=eid]').val(),
			type : type
		};
			
		if (!data.cid || !data.eid || !data.type) {
			return ;	
		}
		
		tool.ajax_post(base_url+'/examrank', data, function(res){
			
			if (res.ranking.length) {
								
				var html = '',
					$rank = $('.rank-type');
				
				$rank.empty();
				$.each(res.ranking, function(k,v){
					html += '<div class="rank-all">';
					
					switch (parseInt(type)) {
						case 1:
							html += '<div class="fl rank-score">'+v.score+'分</div>';
							break;
						case 2:
							html += '<div class="fl rank-score">'+v.answer_time+'秒</div>';
							break;
						case 3:
							html += '<div class="fl rank-score">'+v.right_num+'题</div>';
							break;
						default:
							html += '<div class="fl rank-score">'+v.score+'分</div>';
					}
					
					html += '<div class="fl rank-nickname">'+(k+1)+'.'+v.nickname+'</div>';
					html += '<div class="clear"></div>';
					html += '</div>';
				});
				$rank.append(html);
			}
		});
	}).trigger('change');
	
	/***********************生成二维码***********************/	
	//获取老师二维码
	$.get(base_url+'/teachercode', {cid:parseInt($("input[name=cid]").val())}, function(res){
			
			if (res.status) {

				var str = '';

				$.each(res.codes, function(k,v){
					
					str += '<div class="t-code"><img width="140" height="140" src="http://code.mstudy.me/tcode/'+v.code_name+'.png" />';
					
					switch (parseInt(v.status)) {
						case 0:
							str += '<p>(未绑定老师微信)</p></div>';
							break;
						case 1:
							str += '<p class="bind-status">(用户'+v.nickname+'&nbsp;绑定成功)</p>';
							str += '<a href="javascript:;" class="remove-bind" data-tid="'+v.tid+'" data-qid="'+v.qid+'" >解除绑定</a></div>';
							break;
					}
				});
				
				$(".teacher-qrcode-detail").append(str);
			}		
	});	
	
	//生成老师二维码	
	$(".make-qrcode").on("click", function(){
		
		//至多6个老师二维码
		var l = $('.teacher-qrcode').find('.qrcode-detail').size();
		if ( l >= 6 ) {
			alert('班级下至多6位老师');
			return ;
		} 
		
		var data = {
			cid : parseInt($("input[name=cid]").val())
		};
		
		var url = base_url+'/makecode';

		$.get( url , data, function(res){

			if (res.status) {
				
				var str = '<div  class="t-code"><img width="140" height="140" src="http://code.mstudy.me/tcode/'+res.tcodename+'.png" />';
					str += '<p>(未绑定老师微信)</p></div>';	
				
				$(".teacher-qrcode-detail").append(str);
				
			} else {
				alert(res.info);
			}
		
		});		
		
	});
	
	//解除绑定
	$(".teacher-qrcode").on('click', '.remove-bind',function(){
		
		var _this = $(this),
			url = base_url+"/removebind",
			$tCode = _this.parents('.t-code');
			data = {
				cid : parseInt($("input[name=cid]").val()),
				qid : _this.attr('data-qid'),
				tid : _this.attr('data-tid')
			};
			
		$.get(url, data, function(res){
			
			if ( res.status ){
				$tCode.remove();		
			} else {
				alert(res.info)
			}			
			
		});			
	});
	
	//设置备注名
	$(".btn_remark").on("click", function(){

		var url = base_url+'/setremark',
			_this = $(this);
			
		var data = {
			uid : parseInt($("input[name=uid]").val()),
			remark : $(".frm_input").val(),
			cid : parseInt($("input[name=cid]").val())
		};
				
		if (data.remark.length > 15 ) {
			//提示
			$(".frm_msg_content").show();	
			return ;
		}
			
		$.post(url, data, function(res){
			
			if (res.status) {
				//给出备注名
				alert('修改成功');
				var str = "("+data.remark+")";
				$("a[data-uid="+data.uid+"]").prev().find('b').text(str);
							
			} else {
				//提示失败
				alert('修改失败');
			}	
			$(".dialog_wrp").hide();
			$(".mask").hide();

		});
	});

		
})(jQuery, window);