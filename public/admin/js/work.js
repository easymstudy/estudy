(function($, w){
	
	w.form = {
		'total':$('input[name=total]').val()		
	};
	
	
	w.tool = {
		
		'ajax_post' : function(url, data, callback){
			$.post(url, data, callback);			
		}
			
	};

	/*******作业列表******/
	//勾选
	$(".paper-list-number img").on("click", function(){
		
		var _this = $(this);
		if ( _this.hasClass('checked') ){
			var img_url = base_url+'/admin/images/check.png';
			_this.removeClass('checked').attr('src', img_url);	
		} else {
			var img_url =  base_url+'/admin/images/checking.png';
			_this.addClass('checked').attr('src', img_url);	
		}
	});	
	
	//获取选中的paper id集合
	function getCheckIds(){
		//获取勾选的id
		var ids = [];
		var $img_checked = $("table.class-list-table").find('img.checked');
		var l = $img_checked.size();
		
		if (l == 0) {
			alert('请选择一项作业');
			return ;
		}
		
		$img_checked.each(function(){
			var id = $(this).parents('tr').attr('data-eid');
			ids.push(id);
		});
		return ids;
	}	

	//前一页
	$(".pre-page").on("click", function(){

		//获取分页的情况
		var pagination = $(".pagination-info").val(),
		pagination = pagination.split('/');
		if (pagination[0] == 1) 
			return ;
		
		var p = pagination[0] - 1;

		//跳转页面
		location.href = base_url+'/worklist/'+p;
	});
	
	//后一页
	$(".next-page").on("click", function(){
		
		//获取分页的情况
		var pagination = $(".pagination-info").val(),
			pagination = pagination.split('/');	

		if ( pagination[0] == form.total)
			return ;

		var p = parseInt(pagination[0]) + 1;
		
		//跳转页面
		location.href = base_url+'/worklist/'+p;
					
	});

	//全选和全不选
	$(".allcheck").on("click", function(){
		
		var _this = $(this);
		if ( _this.hasClass('checked') ){
			
			//已选中状态， 让所有全不选
			var img_url = base_url+'/admin/images/check.png';
			_this.removeClass('checked').attr('src', img_url);
			$(".paper-list-number img").removeClass('checked').attr('src', img_url);
			
			//取消全选后  可以完成复制，导入，输出页面
			
								
		} else {
			var img_url =  base_url+'/admin/images/checking.png';
			//未选中状态， 让所有全选
			_this.addClass('checked').attr('src', img_url);
			$(".paper-list-number img").addClass('checked').attr('src', img_url);
			
			//全选后 无法完成复制，导入，输出页面
		}
	});
	
	//删除试卷
	$(".delete-exam").on("click", function(){
			
			var ids = getCheckIds();
			
			if (ids.length > 1) {
				alert('无法一次删除多项作业');
				return ;
			}

		if (window.confirm("确定删除作业吗?")) {
					
			var data = {
				'eid':parseInt(ids[0]),
				'is_delete':1
			};
			
			var $status = $("table.class-list-table").find('img.checked').parents('tr');
			
			tool.ajax_post(base_url+'/workdelete', data, function(res){
					if (res.status){
						$status.remove();
					} else{
						alert(res.info);
					}				
			});				
		
		}
	});
	
	/******安排作业页面*******/
	//确认提交
	$("#setWork").on("click", function(){
		
		var paperid = $('.selected-paper-name').find('.paper-lists').attr('data-id');
		
		if ( !paperid ) {
			alert('至少选择一套试卷');
			return ;
		}
		
		//获取班级ids 和 学生ids
		var $class = $(".classes-lists-img.selected"),
			l = $class.size();
		
		if (!l){
			alert('至少选择一个班级');
			return ;
		}
		
		// uids字段 (不用考试的学生)
		var uids = {}, joins = {}, classes = {};
				
		$class.each(function(){
			var _this = $(this),
				$div = _this.parent(),
				cid = $div.attr('data-cid'),
				cname = $div.attr('title'),
				//剔除的学生
				$students = $div.find('.student-lists-img.removed'),
				//参加的学生
				$joins = $div.find('.student-lists-img.selected');
			
			classes[cid] = cname;
			
			var students = [],
				_joins = [];
						
			if ($students.size()) {
				//获取此班级下选中的学生id(剔除的用户id)
				$students.each(function(){
					var __this = $(this),
						$li = __this.parent();
					students.push($li.attr('data-uid'));				
				});
			}
			uids[cid] = students;
			
			//参加的学生
			if ($joins.size()) {
				$joins.each(function(){
					var __this = $(this),
						$li = __this.parent();
					_joins.push($li.attr('data-oid'));
				});	
			}
			
			//如果有人参加			
			if (_joins.length) {
				joins[cid] = _joins;
			}
		});
		
		var data = {
			paperid  : paperid,
			startime : $(".startime").val(),
			endtime  : $(".endtime").val(),
			is_see   : $('.checked').size(),
			uids : JSON.stringify(uids),
			joins : JSON.stringify(joins),
			classes : JSON.stringify(classes) 
		};
		
		if (!data.startime || !data.endtime) {
			alert('获取作业时间必须全选');
			return ;
		}
		
		var hours = $("input.hours").val(),
			minute = $("input.minute").val(),
			second = $("input.second").val();
		
		if (!hours && !minute && !second) {
			alert('必须设置作业时长');
			return ;
		}
		
		//作业时长 格式
		var reg = /^\d+$/;
		if (hours){
			if (!reg.test(hours)){
				alert('作业时长非法格式');
				return ;				
			}
		}
		if (minute){
			if (!reg.test(minute)){
				alert('作业时长非法格式');
				return ;				
			}
		}
		if (second){
			if (!reg.test(second)){
				alert('作业时长非法格式');
				return ;				
			}
		}		
		
		//做题时长
		data.duration = parseInt(hours == '' ? 0 : hours)*3600 +  parseInt(minute == '' ? 0 : minute)*60 + parseInt(second == '' ? 0 : second);

		if (data.duration <= 0) {
			alert('作业时长必须为正整数');
			return ;
		}
		
		//给预览试卷一个效果
		$("#redactor_modal_overlay").show();
		$("#redactor_modal_overlay .loading-info").html('loading');
				
		tool.ajax_post(base_url+'/setwork', data, function(res){
			
			$("#redactor_modal_overlay").hide();				
			
			if (res.status) {
				alert(res.info);
				
				//跳转到列表页面
				location.href = base_url+'/worklist';					
			} else {
				alert(res.info);
			}				
		});
			
	});
	
	//选择班级
	$(".select-paper-name img").on("click", function(){

			var _this = $(this),
				$selectPaper = $('.select-paper-name'),
				$selectedPaper = $('.selected-paper-name');
			
			$selectedPaper.empty();
			
			if (_this.hasClass('selected')) {
				//如果已经被选择
				_this.attr("src", base_url +'/admin/images/radio_select.png').removeClass('selected');								
			} else {

				var $div = _this.parent(),
					$_div = $div.clone();	
				$selectPaper.find('img').removeClass('selected').attr("src", base_url +'/admin/images/radio_select.png');	
				//没有被选择
				_this.attr("src", base_url +'/admin/images/radio_selected.png').addClass('selected');
				
				$_div.find('img').attr('src', base_url+'/admin/images/remove.png');
				$selectedPaper.append($_div);								
			}
	});
	
	//去除班级
	$(".selected-paper-name").on("click", "img", function(){
		var _this = $(this),
			$img = $('.select-paper-name').find('.selected');
		$img.attr("src", base_url +'/admin/images/radio_select.png').removeClass('selected');	
		_this.parent().remove();
			
	});
	
	//选择班级展开学生
	$(".classes-lists-img").on("click",function(){
			var _this = $(this),
				$parent = _this.parent();
				$studentlists = $parent.find('.student-lists');
			
			if (_this.hasClass('selected')){
				//取消
				$studentlists.hide();
				_this.removeClass('selected').attr('src',base_url +'/admin/images/radio_select.png');
				$studentlists.find('.student-lists-img').removeClass('selected removed').attr('src',base_url +'/admin/images/radio_select.png');
			} else {
				//选中,并展开
				$studentlists.show();
				_this.addClass('selected').attr('src',base_url +'/admin/images/radio_selected.png');
				$studentlists.find('.student-lists-img').addClass('selected').attr('src',base_url +'/admin/images/radio_selected.png');
				//$studentlists.find('.student-lists-img').addClass('selected').attr('src',base_url +'/admin/images/radio_selected.png');
			}
	});
	
	//单独选取某个学生
	$(".student-lists-img").on("click", function(){
			var _this = $(this);
			if (_this.hasClass('selected')) {
				_this.removeClass('selected').addClass('removed').attr('src',base_url +'/admin/images/remove.png');
				/*
				var l =  _this.parents('.student-lists').find('img.selected').size();
				if ( l == 0 ){
					_this.parents('.classes-lists').find('img.selected')
												   .removeClass('selected')
												   .attr('src',base_url +'/admin/images/radio_select.png');
					//隐藏
					_this.parents('.student-lists').hide();
				}
				*/
					
			} else {
				_this.addClass('selected').removeClass('removed').attr('src',base_url +'/admin/images/radio_selected.png');
			}	
	});

})(jQuery, window);