(function($, w){
	
	//系统设置
	w.form = {
		//修改密码
        'user' : $('input[name=user]'),
        'pass' : $('input[name=pass]'),
        'repass' : $('input[name=repass]'),
		//绑定微信
		'wx_account' : $('input[name=wx_account]'),
		'wx_pass' : $('input[name=wx_pass]'),
		'wx_code' : $('input[name=wx_code]'),
		'wx_secret' : $('input[name=wx_secret]'),
		//验证码
		'verifyImg' : $('#verifyImg')
	};
	
	w.tool = {
		
		'validate' : function(){
			
			if ( form.pass.val() !=  form.repass.val()) {
				alert('两次输入的密码不一致!');
				return ;
			}
			
			if ( form.pass.val().length < 6 ) {
				alert('密码不合法!');
				return ;				
			}
			
			return true;		
		},
		
		'ajax_post' : function(url, data, callback){
			$.post(url, data, callback);			
		}
	};
	
	//修改密码
	$('.changepass').on('click', function(){
		
		if (tool.validate() ){
			
			var data = {
				'user' : form.user.val(),
				'pass' : form.pass.val(),
				'repass' : form.repass.val()
			};
			
			tool.ajax_post('changePass', data, function(res){
				
					if (res.status){
						//提示
						alert(res.info);	
					} else {
						alert(res.info);
					}
			});
		}
	});
	
	//绑定微信
	$('.bindWx').on('click', function(){
			
			var data = {
				"wx_account" : form.wx_account.val(), 
				"wx_pass" : form.wx_pass.val(),
				'wx_secret' : form.wx_secret.val()
			};
			
			if (data.wx_secret == ''){
				alert('AppSecret必须填写');
				return ;			
			}
						
			if ( form.verifyImg.attr('src') != '' ) {
				data.wx_code = form.wx_code.val();	
			}

			var ajax = {
				'type' : 'POST',
				'url'  : base_url+'/bindWx',
				'data' : data,
				'dataType' : 'json',
				'error' : function(){
					alert('操作失败');
					return ;
				},
				'success' : function(res){
					if ( res.status ==1 ) {
						alert('绑定成功');
						var wx_name = res.data.nick_name;
						$('.wx-status').addClass('wx-status-bind').html(wx_name+'&nbsp;&nbsp; 绑定正常');						
					} else if (res.status == '-8'){
						//alert('需要填验证码')
						form.verifyImg.attr('src','wxCode/'+data.wx_account+'/'+Math.random(1));
						$('.need-code').show();
						
					} else {
						alert('绑定失败');
					}
				}
			};
			
			$.ajax(ajax);
	});
	
	//换验证码
	$('.changeCode').on('click', function(){
		form.verifyImg.attr('src','wxCode/'+form.wx_account.val()+'/'+Math.random(1));
	});
	
    
})(jQuery, window);