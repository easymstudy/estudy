/*
 * slide.module.js
 * author: joneshe
 * */

function XMANSlide(selector, params) {

	if (typeof selector === 'undefined') return;

	if (!(selector.nodeType)) {
		if ($(selector).length === 0) return;
	}

	var _this = this;

	var defaults = {
		selectorClassName : 'slide-layout',          //父元素classname，默认：'slide-layout'
		childClassName    : 'slide-layout__page',    //子元素classname，默认：'slide-layout__page'
		animationClassName: 'animation',             //动画元素classname，默认：'animation'
		direction         : 'x',                     //框架可移动方向，可选：x/y，默认：x
		navigation: false,
		navClassName: 'slide-nav',
		navChildClassName: 'nav-item',
		navChileCurrentClassName: 'nav-item--current',
		fullScreen: true,   //是否全屏slide
		cancelTouchMove   : true,                    //是否关闭浏览器默认的touchmove事件，默认：true
		allowEndBounce    : true,                    //是否允许到头、尾时还可被拖动，默认true
		touchRatio        : 1.2,                     //touchmove移动幅度，默认：1.2
		swipeRatio        : 0.3,                     //移动幅度达到页面宽度的多少比例即可切换，默认：0.3
		moveStartThreshold: 80,                      //移动阀值，达到该数值以上即可切换页面，默认：80
		speed             : 300,                     //切换页面的动画间隔，默认：300
		onTouchStartCB: null,                        //onTouchStart触发的callback
		onTouchEndCB: null,                          //onTouchEnd触发的callback
		onChangeStartCB: null,                       //切换页面前触发的callback
		onChangeEndCB: null,                         //切换页面后触发的callback,
		onTouchMove: null
	};

	params = params || {};
	for (var prop in defaults) {
		if (prop in params && typeof params[prop] === 'object') {
			for (var subProp in defaults[prop]) {
				if (!(subProp in params[prop])) {
					params[prop][subProp] = defaults[prop][subProp];
				}
			}
		}
		else if (!(prop in params)) {
			params[prop] = defaults[prop];
		}
	}

	_this.selector = selector;
	_this.params = params;

	/*
	 * support
	 * transform,translate3d,touch
	 * return boolean
	 * 检测浏览器是否支持某个特性
	 * */
	_this.support = {
		//transform
		transform  : (function () {
			'use strict';
			var div = document.createElement('div').style;
			return !!('transform' in div || 'webkitTransform' in div || 'msTransform' in div || 'MsTransform' in div);
		})(),
		//css3 translate3d
		translate3d: (function () {
			'use strict';
			var div = document.createElement('div').style;
			return !!('webkitPerspective' in div || 'MsPerspective' in div || 'perspective' in div);
		})(),
		//touch event
		touch      : (function () {
			'use strict';
			return !!(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
		})()
	};

	_this.browser = {
		ie10: window.navigator.msPointerEnabled,
		ie11: window.navigator.pointerEnabled
	};

	var desktopEvent = ['mousedown', 'mousemove', 'mouseup'];
	if (_this.browser.ie10) {
		desktopEvent = ['MSPointerDown', 'MSPointerMove', 'MSPointerUp'];
	}
	if (_this.browser.ie11) {
		desktopEvent = ['pointerdown', 'pointermove', 'pointerup'];
	}

	_this.posGrid = [];

	var bTouched = false,
		bMoving = false;

	var isTouchEvent = false,
		allowThresholdMove,
		isX = params.direction === 'x',
		isScrolling;

	var containerSize,          //container 宽/高
		slideSize;              //slide 总宽/高
	/*
	 * init()
	 * 对象初始化
	 * 1、设置html高度为框架高度
	 * 2、默认滚动到顶部
	 * 3、是否禁用浏览器默认touchmove事件
	 * 4、设置slide框架size
	 * */
	_this.help = {
		getWinWidth : function () {
			var _width;
			if (window.innerWidth) {
				_width = window.innerWidth;
			} else if (document.documentElement && document.documentElement.clientWidth) {
				_width = document.documentElement.clientWidth;
			}
			return _width;
		},
		getWinHeight: function () {
			var _height;
			if (window.innerHeight) {
				_height = window.innerHeight;
			} else if (document.documentElement && document.documentElement.clientHeight) {
				_height = document.documentElement.clientHeight
			}

			return _height;
		},
		getWrapperWidth: function (){
			return selector.parent().width();
		},
		getWrapperHeight: function (){
			return selector.parent().height();
		}
	};

	_this.id = _this.selector[0].id;
	_this.slide = _this.selector.children('div.' + params.childClassName);
	_this.slideLength = _this.slide.length;
	_this.activeSlide = 0;
	_this.animationElmAll = $('.' + params.animationClassName);
	_this.animationElm = {};

	/**
	 * 定义动画对象
	 * author:joneshe
	 */
	function getAnimationElm(){
		$.each(_this.slide,function (i,e){
			var _e = $(e).find('.' + params.animationClassName),
				_len = _e.length,
				_elm = [];
			for (var j = 0; j < _len; j++) {
				_elm.push(_e.eq(j));
				_this.animationElm[i] = _elm;
			}
		});
	}


	_this.init = function () {
		var _this = this,
			_parent = _this.selector.closest('.container'),
			_html = $('html'),
			event = _this.touchEvent,
			cssText = '';

		if (!!params.fullScreen) {
			_parent.height(_this.help.getWinHeight());
		}


		window.scrollTo(0, 1);

		if (!!params.cancelTouchMove) {
			_html.on(event.touchMove, function (e) {
				e.preventDefault();
			});
		}
		getAnimationElm();

		cssText += _this.setFWSize() + setAnimationStyle()

		_this.insertCss(cssText);

		initEvents();

		if (!!params.navigation) {
			_this.initNavigation();
		}

		selector[0].className = params.selectorClassName + ' ' + params.selectorClassName + '-' + (_this.activeSlide + 1);

		if (typeof params.onChangeEndCB === 'function') {
			params.onChangeEndCB(_this.activeSlide);
		}

	};

	/*
	 * setFWSize()
	 * 初始化时设置slide框架size
	 * */
	_this.setFWSize = function () {
		var _width,
			_height,
			cssText = '',
			sTranslate = _this.support.translate3d ? 'translate3d(0px,0px,0px)' : 'translate(0px,0px)';

		cssText += '.' + params.selectorClassName + '{ ' +
			_this.setCssText('transition-property', 'transform, left, top') +
			_this.setCssText('transition-duration', '0s') +
			_this.setCssText('transform', sTranslate) +
			_this.setCssText('transition-timing-function', 'ease') +
			'}';

		if (params.direction === 'y') {
			//竖向滚动
			cssText += '#' + _this.id + '{ ' +
				_this.setCssText('width', '100%') +
				_this.setCssText('height', (_this.slideLength * 100) + '%') +
				'}' +
				'#' + _this.id + ' > .' + params.childClassName + '{' +
				_this.setCssText('width', '100%') +
				_this.setCssText('height', (100 / _this.slideLength) + '%') +
				'}';

			_height = (params.fullScreen ? _this.help.getWinHeight() : _this.help.getWrapperHeight());
			slideSize =  _height* _this.slideLength;
			containerSize = _height;
		} else {
			//横向滚动
			cssText += '#' + _this.id + '{ ' +
				_this.setCssText('width', (_this.slideLength * 100) + '%') +
				_this.setCssText('height', '100%') +
				_this.setCssText('display', 'box') +
				'}' +
				'#' + _this.id + ' > .' + params.childClassName + '{' +
				_this.setCssText('width', '100%') +
				_this.setCssText('height', '100%') +
				_this.setCssText('box-flex', 1) +
				'}';

			_width = (params.fullScreen ? _this.help.getWinWidth() : _this.help.getWrapperWidth());
			slideSize = _width * _this.slideLength;
			containerSize = _width;
		}

		var _position = 0;
		for (i = 0; i < _this.slideLength; i++) {
			_position = i * containerSize;
			_this.posGrid.push(_position);
		}

		return cssText;

	};
	/*
	 * insertCss(rule)
	 * 向文档<head>底部插入css rule操作
	 * rule: 传入的css text
	 * */
	_this.insertCss = function (rule) {
		var head = document.head || document.getElementsByTagName('head')[0],
			style;

		if (!!head.getElementsByTagName('style').length) {
			style = head.getElementsByTagName('style')[0];
			if (style.styleSheet) {
				style.styleSheet.cssText = rule;
			} else {
				style.appendChild(document.createTextNode(rule));
			}
		} else {
			style = document.createElement('style');

			style.type = 'text/css';
			if (style.styleSheet) {
				style.styleSheet.cssText = rule;
			} else {
				style.appendChild(document.createTextNode(rule));
			}

			head.appendChild(style);
		}
	};

	/*
	 * setCssText(prop,value)
	 * 设置css text
	 * 参数说明：
	 * prop: 传入的属性名
	 * value: 传入的值
	 * */
	_this.setCssText = function (prop, value) {
		return prop + ': ' + value + '; ';
	};

	/*
	 * touchEvent
	 * touchStart,touchMove,touchEnd
	 * 定义touch事件类型
	 * */
	_this.touchEvent = {
		touchStart: _this.support.touch ? 'touchstart' : desktopEvent[0],
		touchMove : _this.support.touch ? 'touchmove' : desktopEvent[1],
		touchEnd  : _this.support.touch ? 'touchend' : desktopEvent[2]
	};

	/*
	 * initEvents()
	 * touch事件初始化
	 * */
	function initEvents() {
		_this.selector
			.on(_this.touchEvent.touchStart, onTouchStart)
			.on(_this.touchEvent.touchMove, onTouchMove)
			.on(_this.touchEvent.touchEnd, onTouchEnd)
			.on('webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd',debounce(function (){
					selector[0].className = params.selectorClassName + ' ' + params.selectorClassName + '-' + (_this.activeSlide + 1)},
				300)
			);

	}

	/**
	 * 定义动画元素cssStyle
	 * author:joneshe
	 */
	function setAnimationStyle(){
		var cssText = '';

		cssText += '.' +params.animationClassName + '{' +
			_this.setCssText('display','none') +
			'}' +
			'.touchstart .' +params.animationClassName + '{' +
			_this.setCssText('animation-duration','0 !important') +
			_this.setCssText('animation-delay','0 !important') +
			_this.setCssText('animation-iteration-count','1 !important') +
			'}';

		$.each(_this.animationElm,function (i,e){
			var _len = e.length,
				_index = Math.floor(i) + 1;
			for (var j = 0; j < _len; j++) {
				var obj = e[j],
					_className = obj.attr('data-item'),
					_animation = obj.attr('data-animation'),
					_duration  = ((obj.attr('data-duration') / 1000) || 1) + 's',
					_timing = obj.attr('data-timing-function') || 'ease',
					_delay = (obj.attr('data-delay') / 1000) + 's',
					_count = obj.attr('data-iteration-count') || 1;

				var _t = '.' + params.selectorClassName + '-' + _index +
					' .page-' + _index +
					' .' + _className;

				cssText += _t + '{' +
					_this.setCssText('display','block') +
					_this.setCssText('animation-name',_animation) +
					_this.setCssText('animation-duration',_duration) +
					_this.setCssText('animation-timing-function',_timing) +
					_this.setCssText('animation-delay',_delay) +
					_this.setCssText('animation-fill-mode','both') +
					_this.setCssText('animation-iteration-count',_count) +
					'}';
			}

		});

		return cssText;

	}


	_this.initNavigation = function (){
		var navHtml = '<div class="' + params.navClassName + '">';


		for (var i = 0; i < _this.slideLength; i++) {

			navHtml += (i ===0 ) ? '<a class="' + params.navChildClassName + ' ' + params.navChileCurrentClassName + '" href="javascript:;">' + (i + 1) + '</a>' : '<a class="' + params.navChildClassName + '" href="javascript:;">' + (i + 1) + '</a>'
		}

		navHtml += '</div>';

		_this.selector.after(navHtml);

		_this.navigation = _this.selector.siblings('.' + params.navClassName);


		_this.navItem = _this.navigation.find('.' + params.navChildClassName);
		_this.currentNavItem = _this.navigation.find('.' + params.navChileCurrentClassName);

	};

	_this.toggleNavigation = function (index){
		_this.currentNavItem.removeClass(params.navChileCurrentClassName);

		_this.currentNavItem = _this.navItem.eq(index);
		_this.currentNavItem.addClass(params.navChileCurrentClassName);

	};

	/*
	 * 定义event中所需变量 start
	 * */
	_this.touches = {
		start   : 0,
		startX  : 0,
		startY  : 0,
		current : 0,
		currentX: 0,
		currentY: 0,
		diff    : 0,
		abs     : 0
	};
	_this.positions = {
		start  : 0,
		abs    : 0,
		diff   : 0,
		current: 0
	};
	_this.times = {
		start: 0,
		end  : 0
	};

	function maxposition() {
		var a = slideSize - containerSize;
		if (a < 0) a = 0;
		return a;
	}

	_this.swipeNext = function () {
		var currentPosition = _this.getTranslate(),
			newPosition;

		newPosition = -(Math.floor(Math.abs(currentPosition) / Math.floor(containerSize)) * containerSize + containerSize);

		newPosition = newPosition < -maxposition() ? -maxposition() : newPosition;

		if (newPosition === currentPosition) {
			return false;
		}

		if (typeof params.onChangeStartCB === 'function') {
			params.onChangeStartCB(_this.activeSlide);
		}

		_this.activeSlide = _this.activeSlide >= _this.slideLength - 1 ? _this.slideLength - 1 : -newPosition / containerSize;

		swipeToPosition(newPosition, 'next');

		return true;
	};

	_this.swipePrev = function () {
		var currentPosition = _this.getTranslate(),
			newPosition;

		newPosition = -(Math.ceil(-currentPosition / containerSize) - 1) * containerSize;

		newPosition = newPosition > 0 ? 0 : newPosition;

		if (newPosition === currentPosition) {
			return false;
		}

		if (typeof params.onChangeStartCB === 'function') {
			params.onChangeStartCB(_this.activeSlide);
		}

		_this.activeSlide = _this.activeSlide <= 0 ? 0 : -newPosition / containerSize;

		swipeToPosition(newPosition, 'prev');

		return true;
	};

	_this.swipeReset = function () {
		var currentPosition = _this.getTranslate(),
			newPosition,
			maxPosition = -maxposition();

		newPosition = 0;

		for (var i = 0; i < _this.posGrid.length; i++) {
			if (-currentPosition === _this.posGrid[i]) {
				return
			}

			if (-currentPosition >= _this.posGrid[i] && -currentPosition < _this.posGrid[i + 1]) {
				newPosition = _this.positions.diff > 0 ? -_this.posGrid[i + 1] : -_this.posGrid[i];
				break;
			}
		}

		newPosition = -currentPosition >= _this.posGrid[_this.posGrid.length - 1] ? -_this.posGrid[_this.posGrid.length - 1] : newPosition;

		newPosition = currentPosition <= maxPosition ? maxPosition : newPosition;

		if (newPosition === currentPosition) {
			return false;
		}

		swipeToPosition(newPosition, 'reset');
		return true;
	};

	function swipeToPosition(newPosition, action) {
		var speed = params.speed;

		_this.setTranslate(newPosition);
		_this.setTransitionDuration(speed);

		if (!!params.navigation) {
			_this.toggleNavigation(_this.activeSlide);
		}

		if (typeof params.onChangeEndCB === 'function') {
			params.onChangeEndCB(_this.activeSlide);
		}

	}

	_this.swipeTo = function (index) {
		index = parseInt(index, 10);


		if (index > _this.slideLength - 1 || index < 0) {
			return;
		}

		var currentPosition = _this.getTranslate(),
			newPosition;

		newPosition = -index * containerSize;

		newPosition = (newPosition < -maxposition()) ? -maxposition() : newPosition;

		if (newPosition === currentPosition) {
			return false;
		}

		_this.activeSlide = index;

		swipeToPosition(newPosition,'swipeTo');

		return true;
	};

	/**
	 * 事件防抖
	 * author:joneshe
	 */
	function debounce(func,wait,immediate){
		var timeout, result;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) result = func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) result = func.apply(context, args);
			return result;
		};
	}

	/*
	 * onTouchStart
	 * touchstart事件定义
	 * */
	function onTouchStart(e) {
		if (bTouched) {
			return false;
		}

		bTouched = true;

		if (typeof params.onTouchStartCB === 'function') {
			params.onTouchStartCB(_this.activeSlide);
		}

		isTouchEvent = e.type === 'touchstart';

		var touches = e.targetTouches;

		var pageX = isTouchEvent ? touches[0].pageX : (e.pageX || e.clientX),
			pageY = isTouchEvent ? touches[0].pageY : (e.pageY || e.clientY);

		_this.touches.startX = _this.touches.currentX = pageX;
		_this.touches.startY = _this.touches.currentY = pageY;

		_this.touches.start = _this.touches.current = isX ? pageX : pageY;

		_this.positions.start = _this.positions.current = _this.getTranslate();

		_this.setTransitionDuration(0);

		//设置动画
		_this.setTranslate(_this.positions.start);

		//记录touchstart开始时间
		_this.times.start = (new Date()).getTime();

		//重设isScrolling状态
		isScrolling = undefined;

		if (params.moveStartThreshold > 0) {
			allowThresholdMove = false;
		}

	}

	/*
	 * onTouchMove
	 * touchmove事件定义
	 * */
	function onTouchMove(e) {
		if (!bTouched) {
			return;
		}

		if (isTouchEvent && e.type === 'mousemove') {
			return;
		}

		var touches = e.targetTouches;

		var pageX = isTouchEvent ? touches[0].pageX : (e.pageX || e.clientY),
			pageY = isTouchEvent ? touches[0].pageY : (e.pageY || e.clientY);

		if (typeof isScrolling === 'undefined' && isX) {
			isScrolling = !!(isScrolling || Math.abs(pageY - _this.touches.startY) > Math.abs(pageX - _this.touches.startX));
		}

		if (typeof isScrolling === 'undefined' && !isX) {
			isScrolling = !!(isScrolling || Math.abs((pageY - _this.touches.startX)) < Math.abs(pageX - _this.touches.startX));
		}

		if (!!isScrolling) {
			bTouched = false;
			return;
		}

		//fixed：嵌套模式下可以随意拖动
		if (!!e.nested) {
			bTouched = false;
			return;
		}

		e.nested = true;

		bMoving = true;

		e.preventDefault();

		_this.touches.current = isX ? pageX : pageY;

		_this.positions.current = (_this.touches.current - _this.touches.start) * params.touchRatio + _this.positions.start;

		if (Math.abs(_this.touches.current - _this.touches.start) > params.moveStartThreshold || allowThresholdMove) {
			if (!allowThresholdMove) {
				allowThresholdMove = true;
				_this.touches.start = _this.touches.current;
				return;
			}
			debounce(_this.setTranslate(_this.positions.current),300);
		} else {
			_this.positions.current = _this.positions.start;
		}

		if (typeof params.onTouchMove === 'function') {
			debounce(params.onTouchMove(_this.positions.current),300);
		}

	}

	/*
	 * onTouchEnd
	 * touchend事件定义
	 * */
	function onTouchEnd(e) {
		if (isScrolling) {
			_this.swipeReset();
		}

		//		if (!bTouched) {
		//			return false;
		//		}

		bTouched = false;

		if (!_this.positions.current && _this.positions.current !== 0) {
			_this.positions.current = _this.positions.start;
		}

		_this.times.end = (new Date()).getTime();

		_this.touches.diff = _this.touches.current - _this.touches.start;
		_this.touches.abs = Math.abs(_this.touches.diff);

		_this.positions.diff = _this.positions.current - _this.positions.start;
		_this.positions.abs = Math.abs(_this.positions.diff);

		var diff = _this.positions.diff,
			diffAbs = _this.positions.abs,
			timeDiff = _this.times.end - _this.times.start;

		var direction = diff < 0 ? 'next' : 'prev';

		//short touch
		if (timeDiff <= 300) {
			if (diffAbs < 5) {
				if (diffAbs === 0) {
					_this.swipeReset();
				}
			} else {
				if (direction === 'next') {
					if (diffAbs < 30) {
						_this.swipeReset();
					} else {
						_this.swipeNext();
					}
				}
				if (direction === 'prev') {
					if (diffAbs < 30) {
						_this.swipeReset();
					} else {
						_this.swipePrev();
					}
				}
			}

		}
		//long touch
		else {
			if (direction === 'next') {
				if (diffAbs >= containerSize * params.swipeRatio) {
					_this.swipeNext();
				} else {
					_this.swipeReset();
				}
			}
			if (direction === 'prev') {
				if (diffAbs >= containerSize * params.swipeRatio) {
					_this.swipePrev();
				} else {
					_this.swipeReset();
				}
			}
		}

		if (!!params.navigation) {
			_this.toggleNavigation(_this.activeSlide);
		}

		if (typeof params.onTouchEndCB === 'function') {
			params.onTouchEndCB(_this.activeSlide);
		}

	}


	_this.setTransitionDuration = function (duration) {
		var es = _this.selector[0].style;
		es.webkitTransitionDuration = es.MsTransitionDuration = es.msTransitionDuration = es.transitionDuration = duration / 1000 + 's';
	};

	//获取wrapper偏移量
	_this.getTranslate = function (axis) {
		var el = _this.selector[0],
			bHasCssMatrix = window.WebKitCSSMatrix,
			matrix, currTransform, currStyle, transformMatrix;

		if (typeof axis === 'undefined') {
			axis = _this.params.direction;
		}

		if (!!_this.support.transform) {
			currStyle = window.getComputedStyle(el, null);

			if (bHasCssMatrix) {
				transformMatrix = new WebKitCSSMatrix(currStyle.webkitTransform === 'none' ? '' : currStyle.webkitTransform);
			} else {
				transformMatrix = currStyle.MsTransform || currStyle.msTransform || currStyle.transform || currStyle.getPropertyValue('transform').replace('translate(', 'matrix(1, 0, 0, 1');
				matrix = transformMatrix.toString().split(',');
			}

			if (axis === 'x') {
				if (bHasCssMatrix) {
					currTransform = transformMatrix.m41;
				} else if (matrix.length === 16) {
					currTransform = parseFloat(matrix[12]);
				} else {
					currTransform = parseFloat(matrix[4]);
				}
			}

			if (axis === 'y') {
				if (bHasCssMatrix) {
					currTransform = transformMatrix.m42;
				} else if (matrix.length === 16) {
					currTransform = parseFloat(matrix[13]);
				} else {
					currTransform = parseFloat(matrix[5]);
				}
			}

		} else {
			if (axis === 'x') {
				currTransform = parseFloat(_this.selector.css('left'), 10) || 0;
			}
			if (axis === 'y') {
				currTransform = parseFloat(_this.selector.css('top'), 10) || 0;
			}
		}
		return currTransform || 0;

	};

	//设置wrapper偏移量
	_this.setTranslate = function (x, y) {
		var es = _this.selector[0].style,
			xyz = {
				x: 0,
				y: 0,
				z: 0
			},
			translate;

		if (arguments.length === 2) {

			xyz.x = x;
			xyz.y = y;
		} else {
			if (typeof y === 'undefined') {
				y = params.direction == 'x' ? 'x' : 'y'
			}
			if (!params.allowEndBounce) {
				x = (x >= 0) ? 0 : x;
				x = (x < -maxposition()) ? -maxposition() : x;
			}
			xyz[y] = x;
		}

		if (!!_this.support.transform) {
			translate = _this.support.translate3d ? 'translate3d(' + xyz.x + 'px, ' + xyz.y + 'px, ' + xyz.z + 'px)' : 'translate(' + xyz.x + 'px, ' + xyz.y + 'px)';
			es.webkitTransform = es.MsTransform = es.msTransform = es.transform = translate;
		} else {
			_this.selector.css({
				'left': xyz.x,
				'top' : xyz.y
			});
		}

	}
}

XMANSlide.prototype = {
	constructor          : XMANSlide
};

/**
 * define Zepto Plugin
 * author:joneshe
 */
if (window.Zepto) {
	(function($){
		'use strict';
		$.fn.XMANSlide = function (params){
			var instance;
			this.each(function (i){
				var _this = $(this);

				if (!_this.data('slide')) {
					var _slide = new XMANSlide(_this,params);
					if (!i) {
						instance = _slide;
					}
					_slide.init();
					_this.data('slide',_slide);
				}
			});
			return instance;
		}
	})(window.Zepto);
}/*  |xGv00|1d06d92bb51d48f186ba967433f8a491 */