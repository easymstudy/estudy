/*
* index.js
* author: joneshe
* */

var rc = {};

rc.container = $('#slideLayout');

rc.slide = new XMANSlide(rc.container,{
	navigation: true,
	allowEndBounce: false,
	onChangeEndCB: function (){
		rc.navigation = $('div.slide-nav') || rc.navigation;

		if (rc.slide.activeSlide === 5) {
			rc.navigation.hide();
		}else{
			rc.navigation.show();
		}
	}
});

rc.slide.init();
/*  |xGv00|513ddfba7ad8c3b3017c740a84377d28 */